FROM python:3.9-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /django

RUN apk update
RUN apk add postgresql-dev gcc python3-dev musl-dev jpeg-dev zlib-dev \
    ca-certificates linux-headers libffi-dev
RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
CMD python django/manage.py collectstatic

COPY . .