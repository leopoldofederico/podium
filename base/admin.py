from django.contrib import admin
from base.models import Team, Place

# Register your models here.
admin.site.register(Team)
admin.site.register(Place)