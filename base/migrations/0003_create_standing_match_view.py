from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20220308_1947'),
    ]

    query = """CREATE VIEW match_standings AS
                select m._id, date, stage_id, team1_id as squad_id, s1.name, t1.shield, m.role1 as role,
                	case
                        when team1_score > team2_score then 'Win'
                        when team1_score = team2_score then 'Draw'
                        when team1_score < team2_score then 'Lost'
                    end status,
                    team2_id as opponent_id, s2.name as opponent_name, t2.shield as opponent_shield,
                    m.role2 as opponent_role, team1_points as points, team1_bonus as bonus,
                    team1_score as scorefor,
                    team2_score as scoreagainst, (team1_score - team2_score) as difference,
                    m."updatedAt" 
                from matches as m inner join squads as s1 on m.team1_id = s1._id
                        inner join teams as t1 on s1.team_id = t1._id
                    inner join squads as s2 on m.team2_id = s2._id
                        inner join teams as t2 on s2.team_id = t2._id
                where team1_points is not null and m.deleted_at is null
                
                UNION
                
                select m._id, date, stage_id, team2_id as squad_id, s2.name, t2.shield, m.role2 as role,
                    case
                        when team2_score > team1_score then 'Win'
                        when team2_score = team1_score then 'Draw'
                        when team2_score < team1_score then 'Lost'
                    end status,
                    team1_id as opponent_id, s1.name as opponent_name, t1.shield as opponent_shield, m.role1 as opponent_role,
                    team2_points as points, team2_bonus as bonus, team2_score as scorefor,
                    team1_score as scoreagainst, (team2_score - team1_score) as difference,
                    m."updatedAt" 
                from matches as m inner join squads as s1 on m.team1_id = s1._id
                        inner join teams as t1 on s1.team_id = t1._id
                    inner join squads as s2 on m.team2_id = s2._id
                        inner join teams as t2 on s2.team_id = t2._id
                where team1_points is not null and m.deleted_at is null
                
                UNION

                select i._id, date, stage_id, squad_id, s.name, t.shield, '' as role,
                    'Incidence' as status,
                    -1 as oponent_id, '' as opponent_name, '' as opponent_shield,
                    '' as opponent_role, points, 0 as bonus, 0 as score_for, 0 as score_against,
                    0 as difference, i."updatedAt"
                from incidences as i inner join squads as s on s._id = i.squad_id
                    inner join teams as t on s.team_id = t._id
                where i.deleted_at is null"""
    
    operations = [
        migrations.RunSQL(
            
            query,
            "DROP VIEW match_standings"
        )
    ]