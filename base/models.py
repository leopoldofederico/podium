from django.db import models
from django.contrib.auth.models import User
from django_softdelete.models import SoftDeleteModel
from django.db.models import Count, Sum, Q, IntegerField, Value
import datetime

# Create your models here.
class Category(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, default=1)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'categories'

class Place(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    state = models.CharField(max_length=20)
    country = models.CharField(max_length=2)
    # location = models.PointField(geography=True, default=Point(0.0, 0.0), blank=True)

    def __str__(self):
        return f"{self.name}, {self.state} ({self.country})"
    
    class Meta:
        db_table = 'places'

class Tournament(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    isCurrent = models.BooleanField(default=True)
    season = models.CharField(max_length=100, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)
    win_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    draw_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    lost_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    abandon_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    goals_win_abandon = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    goals_lost_abandon = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    draw = models.BooleanField(default=True)
    bonus = models.BooleanField(default=False)
    logo = models.ImageField(null=True, blank=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    @property
    def user(self):
        return self.category.user

    def __str__(self):
        return self.name + ' ' + self.season + self.category.name
    
    class Meta:
        db_table = 'tournaments'

class Team(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    official_name = models.CharField(max_length=200, blank=False)
    shield = models.ImageField(null=False, blank=False, default="/shield.png")
    place = models.ForeignKey(Place, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'teams'

class Stage(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, null=False, default=1)
    win_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    draw_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    lost_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    abandon_points = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    goals_win_abandon = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    goals_lost_abandon = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'stages'

class Person(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    last_name = models.CharField(max_length=200, blank=False)
    first_name = models.CharField(max_length=200, blank=False)
    external_id = models.CharField(max_length=200, blank=False)
    mail = models.EmailField(max_length=200, blank=False)
    photo = models.ImageField(null=True, blank=True, default="/person.png")
    born_at = models.DateField(blank=True, null=True)
    referee = models.BooleanField(default=False)
    official = models.BooleanField(default=False)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.last_name + ", " + self.first_name

    class Meta:
        db_table = 'people'
    
class Squad(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=False)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, null=False)
    players = models.ManyToManyField(Person, db_table='squad_players')
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'squads'
        constraints = [models.UniqueConstraint(
            fields=['team', 'tournament', 'deleted_at'], 
            name='one_register_per_team_per_tournament')
        ]

class Group(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    name = models.CharField(max_length=200, blank=False)
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, null=False)
    competitors = models.ManyToManyField(Squad, db_table='squad_team')
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
    def standings(self):
        
        group = self
        standings = group.competitors.all()
        # print(standings)

        win = Count('standing', filter=Q(standing__status='Win', standing__stage_id=group.stage_id))
        draw = Count('standing', filter=Q(standing__status='Draw', standing__stage_id=group.stage_id))
        lost = Count('standing', filter=Q(standing__status='Lost', standing__stage_id=group.stage_id))
        played = Count('standing', filter=Q(~Q(standing__status='Incidence'), standing__stage_id=group.stage_id))
        points = Sum('standing__points', filter=Q(standing__stage_id=group.stage_id))
        scoreFor = Sum('standing__scorefor', filter=Q(standing__stage_id=group.stage_id))
        scoreAgainst = Sum('standing__scoreagainst', filter=Q(standing__stage_id=group.stage_id))
        difference = Sum('standing__difference', filter=Q(standing__stage_id=group.stage_id))
        bonus = Sum('standing__bonus', filter=Q(standing__stage_id=group.stage_id))

        # print(win)
        standings = standings.annotate(stage_id=Value(group.stage_id, output_field=IntegerField()),
            played=played, scoreFor=scoreFor, scoreAgainst=scoreAgainst, points=points, 
            difference=difference, win=win, draw=draw, lost=lost, bonus=bonus)

        return standings

    class Meta:
        db_table = 'groups'


class Match(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, null=False)
    role1 = models.CharField(max_length=10, blank=False, default="home")
    role2 = models.CharField(max_length=10, blank=False, default="away")
    team1 = models.ForeignKey(Squad, on_delete=models.CASCADE, null=False, related_name='team1')
    team2 = models.ForeignKey(Squad, on_delete=models.CASCADE, null=False, related_name='team2')
    team1_points = models.IntegerField(blank=True, null=True)
    team2_points = models.IntegerField(blank=True, null=True)
    team1_score = models.IntegerField(blank=True, null=True)
    team2_score = models.IntegerField(blank=True, null=True)
    team1_played = models.BooleanField(blank=True, null=True)
    team2_played = models.BooleanField(blank=True, null=True)
    team1_bonus = models.IntegerField(blank=True, null=True)
    team2_bonus = models.IntegerField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    fixture_note = models.TextField(blank=True, null=True)
    round = models.CharField(max_length=10, blank=True)
    series = models.IntegerField(null=True, blank=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        result = f"{self.team1.name} vs. {self.team2.name}"
        if self.date:
            result = f"{result} ({self.date.strftime('%y-%m-%d')})"
        return result

    class Meta:
        db_table = 'matches'

class Incidence(SoftDeleteModel):
    _id = models.AutoField(primary_key=True, editable=False)
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, null=False)
    squad = models.ForeignKey(Squad, on_delete=models.CASCADE, null=False)
    points = models.IntegerField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.squad.name + ": " + str(self.points)
    
    class Meta:
        db_table = 'incidences'

class Standing(models.Model):
    _id = models.BigIntegerField(primary_key=True)
    date = models.DateTimeField(blank=True, null=True)
    stage = models.ForeignKey(Stage, on_delete=models.DO_NOTHING, null=False)
    squad = models.ForeignKey(Squad, on_delete=models.DO_NOTHING, null=False)
    name = models.CharField(max_length=200, blank=False)
    shield = models.ImageField(null=False, blank=False)
    role = models.CharField(max_length=10, blank=False)
    opponent = models.ForeignKey(Squad, on_delete=models.DO_NOTHING, null=False, related_name='opponent')
    opponent_name = models.CharField(max_length=200, blank=False)
    opponent_shield = models.ImageField(null=False, blank=False)
    opponent_role = models.CharField(max_length=10, blank=False)
    points = models.IntegerField(blank=True, null=True)
    bonus = models.IntegerField(blank=True, null=True)
    scorefor = models.IntegerField(blank=True, null=True)
    scoreagainst = models.IntegerField(blank=True, null=True)
    difference = models.IntegerField(blank=True, null=True)
    updatedAt = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=200, blank=False)

    class Meta:
        managed = False
        db_table = 'match_standings'
    
    def __str__(self):
        return f"{self.name}: {self.scorefor}-{self.scoreagainst} ({self.points})"