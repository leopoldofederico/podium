from pickletools import read_floatnl, read_long1
from .models import Category, Place, Tournament, Team, Stage, Person, Squad, Match, Group, Incidence, Standing
from django.db.models import Q
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken



class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class TournamentSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(read_only=True)
    squads = serializers.SerializerMethodField(read_only=True)
    stages = serializers.SerializerMethodField(read_only=True)
    
    class Meta:
        model = Tournament
        fields = ['_id', 'name', 'season', 'category', 'isCurrent', 'squads', 'stages', 'win_points',
            'draw_points', 'lost_points', 'abandon_points', 'goals_win_abandon', 'goals_lost_abandon',
            'draw', 'bonus', 'logo']
    
    def get_category(self, obj):
        category = obj.category
        serializer = CategorySerializer(category, many=False)
        return serializer.data
    
    def get_squads(self, obj):
        squads = obj.squad_set
        serializer = SquadSerializer(squads, many=True)
        return serializer.data
    
    def get_stages(self, obj):
        stages = obj.stage_set
        serializer = StageSerializer(stages, many=True)
        return serializer.data

class TournamentMiniSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(read_only=True)
    
    class Meta:
        model = Tournament
        fields = ['_id', 'name', 'season', 'category', 'isCurrent', 'win_points',
            'draw_points', 'lost_points', 'abandon_points', 'goals_win_abandon', 'goals_lost_abandon',
            'draw', 'bonus', 'logo']
    
    def get_category(self, obj):
        category = obj.category
        serializer = CategorySerializer(category, many=False)
        return serializer.data
    
class TournamentsSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(read_only=True)
    squads = serializers.SerializerMethodField(read_only=True)
    
    class Meta:
        model = Tournament
        fields = ['_id', 'name', 'season', 'category', 'isCurrent', 'squads', 'win_points',
            'draw_points', 'lost_points', 'abandon_points', 'goals_win_abandon', 'goals_lost_abandon',
            'bonus', 'draw', 'logo']
    
    def get_category(self, obj):
        category = obj.category
        serializer = CategorySerializer(category, many=False)
        return serializer.data
    
    def get_squads(self, obj):
        squads = obj.squad_set
        serializer = SquadSerializer(squads, many=True)
        return serializer.data

class PlaceSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField('get_full_name')

    class Meta:
        model = Place
        fields = ['_id', 'name', 'state', 'country', 'full_name']

    def get_full_name(self, obj):
        return str(obj)

class TeamSerializer(serializers.ModelSerializer):
    place = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Team
        fields = ['_id', 'name', 'official_name', 'shield', 'place']
    
    def get_place(self, obj):
        place = obj.place
        serializer = PlaceSerializer(place, many=False)
        return serializer.data

class StageSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField(read_only=True)
    incidences = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Stage
        fields = ['_id', 'name', 'groups', 'win_points', 'draw_points', 'lost_points', 
            'abandon_points', 'goals_win_abandon', 'goals_lost_abandon', 'incidences']
    
    def get_groups(self, obj):
        groups = obj.group_set
        serializer = GroupSerializer(groups, many=True)
        return serializer.data
    
    def get_incidences(self, obj):
        incidences = obj.incidence_set
        serializer = IncidenceSerializer(incidences, many=True)
        return serializer.data

class StageMiniSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Stage
        fields = ['_id', 'name', 'win_points', 'draw_points', 'lost_points', 
            'abandon_points', 'goals_win_abandon', 'goals_lost_abandon', 'tournament_id']

class GroupSerializer(serializers.ModelSerializer):
    competitors = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Group
        fields = ['_id', 'name', 'competitors']

    def get_competitors(self, obj):
        competitors = obj.competitors
        
        serializer = SquadSerializer(competitors, many=True)
        return serializer.data

class PersonBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ['_id', 'last_name', 'first_name', 'photo', 'born_at', 'referee', 'official']

class PersonExtendedSerializer(PersonBaseSerializer):
    class Meta:
        model = Person
        fields = PersonBaseSerializer.Meta.fields + ['external_id','mail',]

class SquadSerializer(serializers.ModelSerializer):
    team = serializers.SerializerMethodField(read_only=True)
    tournament = serializers.SerializerMethodField(read_only=True)
    players = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Squad
        # fields = ['_id', 'name']
        fields = ['_id', 'name', 'tournament', 'team', 'players']
    
    def get_team(self, obj):
        team= obj.team 
        serializer = TeamSerializer(team, many=False)
        return serializer.data
        
    def get_tournament(self, obj):
        tournament= obj.tournament 
        serializer = TournamentMiniSerializer(tournament, many=False)
        return serializer.data
    
    def get_players(self, obj):
        players = obj.players.order_by('last_name', 'first_name')
        serializer = PersonBaseSerializer(players, many=True)
        return serializer.data
    
class SquadMiniSerializer(serializers.ModelSerializer):
    team = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Squad
        # fields = ['_id', 'name']
        fields = ['_id', 'name', 'team']
    
    def get_team(self, obj):
        team= obj.team 
        serializer = TeamSerializer(team, many=False)
        return serializer.data

class MatchSerializer(serializers.ModelSerializer):
    stage = serializers.SerializerMethodField(read_only=True)
    team1 = serializers.SerializerMethodField(read_only=True)
    team2 = serializers.SerializerMethodField(read_only=True)
    draw = serializers.SerializerMethodField(read_only=True)
    bonus = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Match
        fields = ['_id', 'stage', 'role1', 'role2', 'team1', 'team2', 'team1_points', 'team1_bonus',
            'team2_points', 'team2_bonus', 'team1_score', 'team2_score', 'team1_played', 'team2_played',
            'date', 'note','round', 'series', 'draw', 'bonus', 'fixture_note']
        read_only_fields = fields

    def get_stage(self, obj):
        stage = obj.stage 
        serializer = StageMiniSerializer(stage, many=False)
        return serializer.data
    
    def get_team1(self, obj):
        team1 = obj.team1 
        serializer = SquadMiniSerializer(team1, many=False)
        return serializer.data
    
    def get_team2(self, obj):
        team2 = obj.team2 
        serializer = SquadMiniSerializer(team2, many=False)
        return serializer.data

    def get_draw(self, obj):
        return obj.stage.tournament.draw
    
    def get_bonus(self, obj):
        return obj.stage.tournament.bonus
    
    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.select_related('team1', 'team2', 'stage')
        return queryset

class IncidenceSerializer(serializers.ModelSerializer):
    squad = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Incidence
        fields = ['_id', 'squad', 'points', 'date', 'note']
        read_only_fields = fields
    
    def get_squad(self, obj):
        squad = obj.squad 
        serializer = SquadMiniSerializer(squad, many=False)
        return serializer.data
        
    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.select_related('squad', 'stage')
        return queryset

class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(read_only=True)
    _id = serializers.SerializerMethodField(read_only=True)
    isAdmin = serializers.SerializerMethodField(read_only=True)
    
    class Meta:
        model = User
        fields = ['id', '_id', 'isAdmin', 'username', 'name', 'email', 'first_name', 'last_name']
    
    def get__id(self, obj):
        return obj.id

    def get_isAdmin(self, obj):
        return obj.is_staff

    def get_name(self, obj):
        name = obj.first_name
        if name == '':
            name = obj.email
        
        return name

class UserSerializerWithToken(UserSerializer):
    token = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ['id', '_id', 'isAdmin', 'username', 'name', 'email', 'token', 'first_name', 'last_name']
    
    def get_token(self, obj):
        token = RefreshToken.for_user(obj)
        return str(token.access_token)

class GroupWithStandingSerializer(serializers.ModelSerializer):
    stage = serializers.StringRelatedField(many=False)
    tournament = serializers.SerializerMethodField(read_only=True)
    standings = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Group
        fields = ['_id', 'name', 'stage', 'tournament', 'standings', 'stage_id']
    
    def get_tournament(self, obj):
        tournament = obj.stage.tournament
        serializer = TournamentsSerializer(tournament, many=False)
        return serializer.data
    
    def get_standings(self, obj):
        standings = obj.standings()
        serializer = StandingSerializer(standings, many=True)
        return serializer.data

class StandingSerializer(serializers.ModelSerializer):   
    team = serializers.SerializerMethodField(read_only=True)
    played = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    bonus = serializers.SerializerMethodField()
    scoreFor = serializers.SerializerMethodField()
    scoreAgainst = serializers.SerializerMethodField()
    difference = serializers.SerializerMethodField()
    win = serializers.SerializerMethodField()
    draw = serializers.SerializerMethodField()
    lost = serializers.SerializerMethodField()
    stage_id = serializers.SerializerMethodField()
    lastMatches = serializers.SerializerMethodField(read_only=True)
    incidences = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Squad
        fields = ['_id', 'name', 'lastMatches', 'incidences', 'team', 'played', 'points', 'bonus',
            'difference', 'scoreFor', 'scoreAgainst', 'win','draw', 'lost', 'stage_id']
 
    
    def get_team(self, obj):
        team= obj.team 
        serializer = TeamSerializer(team, many=False)
        return serializer.data

    def get_played(self, obj):
        return obj.played
    
    def get_points(self, obj):
        return obj.points
    
    def get_scoreFor(self, obj):
        return obj.scoreFor
    
    def get_scoreAgainst(self, obj):
        return obj.scoreAgainst

    def get_difference(self, obj):
        return obj.difference

    def get_win(self, obj):
        return obj.win

    def get_draw(self, obj):
        return obj.draw

    def get_lost(self, obj):
        return obj.lost
    
    def get_bonus(self, obj):
        return obj.bonus
    
    def get_stage_id(self, obj):
        return obj.stage_id

    def get_lastMatches(self, obj):
        # first I get all the matches of the group's stage
        stage = Stage.objects.get(pk=obj.stage_id)
        # print(stage)
        matches = stage.standing_set
        # for m in matches.all():
        #     print(m)
        # print(matches.all())
        # filter all matches where current squad is team1 or team2
        # filterTeams = Q(team1_id=obj._id) | Q(team2_id=obj._id)

        # also filter played matches, then order by date descedent (last played matches)
        # and then by updatedAt (in case date is empty), and get the final 5
        lastMatches = matches.filter(squad=obj._id).order_by(
            '-date', '-updatedAt')[:5]
        # lastMatches = matches.all()[:5]

        # print(lastMatches)
        serializer = MatchStandingSerializer(lastMatches, many=True)
        return serializer.data
    
    def get_incidences(self, obj):
        incidences = Incidence.objects.filter(stage_id=obj.stage_id, squad_id=obj._id)

        serializer = IncidenceSerializer(incidences, many=True)
        return serializer.data

    
class MatchMiniSerializer(serializers.ModelSerializer):
    team1 = serializers.SerializerMethodField(read_only=True)
    team2 = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Match
        fields = ['_id', 'role1', 'role2', 'team1', 'team2', 'team1_points', 'team2_points', 
            'team1_score', 'team2_score', 'team1_played', 'team2_played', 'date', 'note',
            'team1_bonus', 'team2_bonus', 'round', 'series', 'fixture_note']
        read_only_fields = fields

    def get_team1(self, obj):
        team1 = obj.team1 
        serializer = SquadMiniSerializer(team1, many=False)
        return serializer.data
    
    def get_team2(self, obj):
        team2 = obj.team2 
        serializer = SquadMiniSerializer(team2, many=False)
        return serializer.data
    
    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.select_related('team1', 'team2')
        return queryset

class MatchStandingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Standing
        fields = ['_id', 'date', 'name', 'shield', 'role', 
            'opponent_name', 'opponent_shield', 'opponent_role', 'points', 'bonus', 'scorefor', 
            'scoreagainst', 'difference', 'status']
        read_only_fields = fields
