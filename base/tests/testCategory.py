from django.test import TestCase, Client
from base.models import Category
from django.contrib.auth.models import User
from faker import Faker

class CategoryTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')

    def test_user_can_create_category(self):
        fake = Faker()
        client = Client()
        testCategoryName = fake.name()
        
        totalCategoriesBefore = Category.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/categories/create/', {'name': testCategoryName})
        
        totalCategoriesAfter = Category.objects.all().count()
        
        lastCreatedName = Category.objects.latest('createdAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalCategoriesBefore + 1, totalCategoriesAfter)
        self.assertEquals(testCategoryName, lastCreatedName)

    def test_user_cant_create_empty_name_category(self):
        fake = Faker()
        client = Client()
        testCategoryName = ''
        
        client.login(username='usuario', password='usuario')

        response = client.post('/api/categories/create/', {'name': testCategoryName})
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_category(self):
        fake = Faker()
        client = Client()
        testCategoryName = fake.name()
        
        totalCategoriesBefore = Category.objects.all().count()
        
        response = client.post('/api/categories/create/', {'name': testCategoryName})
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_category(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        newCategory = Category.objects.create(name=newCategoryName, user=self.user)

        client.login(username='usuario', password='usuario')
        
        updatedCategoryName = fake.name()

        response = client.put(f'/api/categories/{newCategory._id}/update/', {'name': updatedCategoryName}, content_type='application/json')
        
        lastUpdatedName = Category.objects.latest('updatedAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedCategoryName, lastUpdatedName)

    def test_user_cannot_edit_not_own_category(self):

        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        otherUser = User.objects.create_user('otheruser', 'other@other.com', 'otherpass')

        newCategory = Category.objects.create(name=newCategoryName, user=otherUser)

        client.login(username='usuario', password='usuario')
        
        updatedCategoryName = fake.name()

        response = client.put(f'/api/categories/{newCategory._id}/update/', {'name': updatedCategoryName}, content_type='application/json')
        
        self.assertEquals(response.status_code, 400)

    def test_user_cannot_edit_making_blank_category(self):

        fake = Faker()
        client = Client()
        newCategoryName = fake.name()

        newCategory = Category.objects.create(name=newCategoryName, user=self.user)

        client.login(username='usuario', password='usuario')
        
        updatedCategoryName = ''

        response = client.put(f'/api/categories/{newCategory._id}/update/', {'name': updatedCategoryName}, content_type='application/json')
        
        self.assertEquals(response.status_code, 400)

    def test_adminuser_can_edit_not_own_category(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        newCategory = Category.objects.create(name=newCategoryName, user=self.user)

        client.login(username='admin', password='admin')
        
        updatedCategoryName = fake.name()

        response = client.put(f'/api/categories/{newCategory._id}/update/', {'name': updatedCategoryName}, content_type='application/json')
        
        lastUpdatedName = Category.objects.latest('updatedAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedCategoryName, lastUpdatedName)

    def test_user_can_delete_own_category(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        

        newCategory = Category.objects.create(name=newCategoryName, user=self.user)

        totalCategoriesBefore = Category.objects.count()

        client.login(username='usuario', password='usuario')

        response = client.delete(f'/api/categories/{newCategory._id}/delete/')
        
        totalCategoriesAfter = Category.objects.all().count()
        totalDeleted = Category.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalCategoriesBefore, totalCategoriesAfter + 1)
        self.assertEquals(totalDeleted, 1)
    
    def test_user_cannot_delete_not_own_category(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        newCategory = Category.objects.create(name=newCategoryName, user=self.otherUser)

        totalCategoriesBefore = Category.objects.all().count()

        client.login(username='usuario', password='usuario')

        response = client.delete(f'/api/categories/{newCategory._id}/delete/')
        
        totalCategoriesAfter = Category.objects.all().count()
        
        self.assertEquals(response.status_code, 400)
        self.assertEquals(totalCategoriesBefore, totalCategoriesAfter)
    
    def test_adminuser_can_delete_not_own_category(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        newCategory = Category.objects.create(name=newCategoryName, user=self.otherUser)

        totalCategoriesBefore = Category.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/categories/{newCategory._id}/delete/')
        
        totalCategoriesAfter = Category.objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalCategoriesBefore, totalCategoriesAfter + 1)

    def test_get_category_by_id(self):
        fake = Faker()
        client = Client()
        newCategoryName = fake.name()
        
        newCategory = Category.objects.create(name=newCategoryName, user=self.user)

        response = client.get(f'/api/categories/{newCategory._id}/')

        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['name'], newCategoryName)
    
    def test_list_user_categories(self):
        fake = Faker()
        client = Client()
        newCategory1Name = fake.name()
        newCategory2Name = fake.name()
        newCategory3Name = fake.name()
        
        newCategory1 = Category.objects.create(name=newCategory1Name, user=self.user)
        newCategory2 = Category.objects.create(name=newCategory2Name, user=self.user)
        newCategory3 = Category.objects.create(name=newCategory3Name, user=self.otherUser)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/categories/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 2)

    
    def test_admin_list_all_categories(self):
        fake = Faker()
        client = Client()
        newCategory1Name = fake.name()
        newCategory2Name = fake.name()
        newCategory3Name = fake.name()
        
        newCategory1 = Category.objects.create(name=newCategory1Name, user=self.user)
        newCategory2 = Category.objects.create(name=newCategory2Name, user=self.admin)
        newCategory3 = Category.objects.create(name=newCategory3Name, user=self.otherUser)
        
        client.login(username='admin', password='admin')

        response = client.get('/api/categories/list/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)
    
    def test_notadminuser_cant_list_all_categories(self):
        fake = Faker()
        client = Client()
        newCategory1Name = fake.name()
        newCategory2Name = fake.name()
        newCategory3Name = fake.name()
        
        newCategory1 = Category.objects.create(name=newCategory1Name, user=self.user)
        newCategory2 = Category.objects.create(name=newCategory2Name, user=self.admin)
        newCategory3 = Category.objects.create(name=newCategory3Name, user=self.otherUser)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/categories/list/')

        self.assertEquals(response.status_code, 403)