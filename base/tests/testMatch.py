from django.test import TestCase, Client
from base.models import Match, Stage, Tournament, Category, Team
from django.contrib.auth.models import User
from faker import Faker

class MatchTestCase(TestCase):
    

    def setUp(self):
        self.faker = Faker()
        adminProfile = self.faker.profile()
        userProfile = self.faker.profile()
        otherProfile = self.faker.profile()
        self.admin = User.objects.create_superuser(adminProfile['username'], adminProfile['mail'], self.faker.password(length=10))
        self.user = User.objects.create_user(userProfile['username'], userProfile['mail'], self.faker.password(length=10))
        self.otherUser = User.objects.create_user(otherProfile['username'], otherProfile['mail'], self.faker.password(length=10))
        self.category = Category.objects.create(
            name = self.faker.word(),
            user = self.user
        )
        self.otherCategory = Category.objects.create(
            name = self.faker.word(),
            user = self.otherUser
        )
        self.tournament = Tournament.objects.create(
            name = self.faker.word(),
            category = self.category,
        )
        self.otherTournament = Tournament.objects.create(
            name = self.faker.word(),
            category = self.otherCategory,
        )
        self.teams = []
        for i in range(1, 5):
            self.teams.append(Team.objects.create(
                name = self.faker.name()
            ))
        self.otherTeams = []
        for i in range(1, 3):
            self.otherTeams.append(Team.objects.create(
                name = self.faker.name()
            ))
        
        self.stage = Stage.objects.create(
            name = self.faker.word(),
            tournament = self.tournament,
        )
        self.otherStage = Stage.objects.create(
            name = self.faker.word(),
            tournament = self.otherTournament,
        )

    def test_user_can_create_match(self):
        client = Client()
        testMatchName = self.faker.name()

        totalMatchesBefore = Match.objects.all().count()
        
        client.force_login(self.user)
        
        testMatchDate = self.faker.date_this_year(after_today=False)
        response = client.post('/api/matches/create/', {
            'stage_id': self.stage._id,
            'team1_id': self.teams[0]._id,
            'team2_id': self.teams[1]._id,
            'date': testMatchDate,
            'note': self.faker.paragraph(),
        })
        
        totalMatchesAfter = Match.objects.all().count()
        
        lastCreatedDate = Match.objects.latest('createdAt').date
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalMatchesBefore + 1, totalMatchesAfter)
        self.assertEquals(testMatchDate, lastCreatedDate)

        
    def test_notlogged_user_cannot_create_match(self):
        client = Client()
        testMatchName = self.faker.name()
        
        testMatchDate = self.faker.date_this_year(after_today=False)
        response = client.post('/api/matches/create/', {
            'stage_id': self.stage._id,
            'team1_id': self.teams[0]._id,
            'team2_id': self.teams[1]._id,
            'date': testMatchDate,
            'note': self.faker.paragraph(),
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_match(self):

        client = Client()
        newMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        client.force_login(self.user)
        
        updatedMatchDate = self.faker.date_this_year(after_today=False)

        response = client.put(f'/api/matches/{newMatch._id}/update/', {
            'team1_id': self.teams[2]._id,
            'team2_id': self.teams[3]._id,
            'date': updatedMatchDate,
            'note': self.faker.paragraph(),
            'team1_points': self.faker.random_int(min=0, max=2),
            'team2_points': self.faker.random_int(min=0, max=2),
            'team1_score': self.faker.random_int(min=0, max=120),
            'team2_score': self.faker.random_int(min=0, max=120),
            'team1_played': self.faker.boolean(),
            'team2_played': self.faker.boolean(),
            },
            content_type='application/json'
        )
        
        lastUpdated = Match.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedMatchDate, lastUpdated.date)
        self.assertEquals(self.teams[2]._id, lastUpdated.team1._id)

    def test_user_cannot_edit_not_own_match(self):

        client = Client()
        testMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        client.force_login(user=self.otherUser)
        
        updatedMatchDate = self.faker.date_this_year(after_today=False)

        response = client.put(f'/api/matches/{newMatch._id}/update/', {
            'team1_id': self.teams[2]._id,
            'team2_id': self.teams[3]._id,
            'date': updatedMatchDate,
            'note': self.faker.paragraph(),
            'team1_points': self.faker.random_int(min=0, max=2),
            'team2_points': self.faker.random_int(min=0, max=2),
            'team1_score': self.faker.random_int(min=0, max=120),
            'team2_score': self.faker.random_int(min=0, max=120),
            'team1_played': self.faker.boolean(),
            'team2_played': self.faker.boolean(),
            },
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)


    def test_adminuser_can_edit_not_own_match(self):
      
        client = Client()
        newMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        client.force_login(self.admin)
        
        updatedMatchDate = self.faker.date_this_year(after_today=False)

        response = client.put(f'/api/matches/{newMatch._id}/update/', {
            'team1_id': self.teams[2]._id,
            'team2_id': self.teams[3]._id,
            'date': updatedMatchDate,
            'note': self.faker.paragraph(),
            'team1_points': self.faker.random_int(min=0, max=2),
            'team2_points': self.faker.random_int(min=0, max=2),
            'team1_score': self.faker.random_int(min=0, max=120),
            'team2_score': self.faker.random_int(min=0, max=120),
            'team1_played': self.faker.boolean(),
            'team2_played': self.faker.boolean(),
            },
            content_type='application/json'
        )
        
        lastUpdated = Match.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedMatchDate, lastUpdated.date)
        self.assertEquals(self.teams[2]._id, lastUpdated.team1._id)

    def test_user_can_delete_own_match(self):
        
        client = Client()
        testMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        totalMatchesBefore = Match.objects.all().count()

        client.force_login(user=self.user)
        
        response = client.delete(f'/api/matches/{newMatch._id}/delete/')
        
        totalMatchesAfter = Match.objects.all().count()
        totalDeleted = Match.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalMatchesBefore, totalMatchesAfter + 1)
        self.assertEquals(totalDeleted, 1)
    
    def test_user_cannot_delete_not_own_match(self):

        client = Client()
        testMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        totalMatchesBefore = Match.objects.all().count()
        
        client.force_login(self.otherUser)

        response = client.delete(f'/api/matches/{newMatch._id}/delete/')
        
        totalMatchesAfter = Match.objects.all().count()
        
        self.assertEquals(response.status_code, 400)
        self.assertEquals(totalMatchesBefore, totalMatchesAfter)
    
    def test_adminuser_can_delete_not_own_match(self):
 
        client = Client()
        testMatchName = self.faker.name()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )

        totalMatchesBefore = Match.objects.all().count()

        client.force_login(user=self.admin)

        response = client.delete(f'/api/matches/{newMatch._id}/delete/')
        
        totalMatchesAfter = Match.objects.all().count()
        totalDeleted = Match.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalMatchesBefore, totalMatchesAfter + 1)
        self.assertEquals(totalDeleted, 1)

    def test_get_match_by_id(self):
        
        client = Client()
        testMatchNote = self.faker.paragraph()
        
        newMatch = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = testMatchNote,
        )

        response = client.get(f'/api/matches/{newMatch._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['note'], testMatchNote)
    
    def test_list_user_matches(self):
        
        client = Client()
        newMatch1 = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        newMatch2 = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[2],
            team2 = self.teams[3],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        newMatch3 = Match.objects.create(
            stage = self.otherStage,
            team1 = self.otherTeams[0],
            team2 = self.otherTeams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        
        client.force_login(user=self.user)

        response = client.get('/api/matches/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 2)

    
    def test_admin_list_all_categories(self):

        client = Client()
        newMatch1 = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[0],
            team2 = self.teams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        newMatch2 = Match.objects.create(
            stage = self.stage,
            team1 = self.teams[2],
            team2 = self.teams[3],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        newMatch3 = Match.objects.create(
            stage = self.otherStage,
            team1 = self.otherTeams[0],
            team2 = self.otherTeams[1],
            date = self.faker.date_this_year(after_today=False),
            note = self.faker.paragraph(),
        )
        
        client.force_login(user=self.admin)

        response = client.get('/api/matches/list/')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)
    
    # # def test_notadminuser_cant_list_all_categories(self):
    # #     fake = Faker()
    # #     client = Client()
    # #     newCategory1Name = fake.name()
    # #     newCategory2Name = fake.name()
    # #     newCategory3Name = fake.name()
        
    # #     newCategory1 = Category.objects.create(name=newCategory1Name, user=self.user)
    # #     newCategory2 = Category.objects.create(name=newCategory2Name, user=self.admin)
    # #     newCategory3 = Category.objects.create(name=newCategory3Name, user=self.otherUser)
        
    # #     client.login(username='usuario', password='usuario')

    # #     response = client.get('/api/categories/list/')

    # #     self.assertEquals(response.status_code, 403)