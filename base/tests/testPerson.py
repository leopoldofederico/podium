from django.test import TestCase, Client
from base.models import Person
from django.contrib.auth.models import User
from faker import Faker
from django.core.files.uploadedfile import SimpleUploadedFile
import tempfile
from PIL import Image


class PersonTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')
        self.faker = Faker()

    def test_user_can_create_person(self):
        client = Client()
        testPersonName = self.faker.first_name()

        totalPeopleBefore = Person.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/people/create/', {
            'last_name': self.faker.last_name(),
            'first_name': testPersonName,
            'external_id': self.faker.ean(length=8),
            'mail': self.faker.email(),
            'photo': self.faker.image_url(width = 640, height = 480),
            'born_at': self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            'referee': False,
            'official': False,
        })
        
        totalPeopleAfter = Person.objects.all().count()
        
        lastCreatedName = Person.objects.latest('createdAt').first_name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalPeopleBefore + 1, totalPeopleAfter)
        self.assertEquals(testPersonName, lastCreatedName)

    def test_user_cannot_create_blank_name(self):
        client = Client()
        testPersonName = ''

        client.login(username='usuario', password='usuario')

        response = client.post('/api/people/create/', {
            'last_name': self.faker.last_name(),
            'first_name': testPersonName,
            'external_id': self.faker.ean(length=8),
            'mail': self.faker.email(),
            'photo': self.faker.image_url(width = 640, height = 480),
            'born_at': self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            'referee': False,
            'official': False,
        })
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_person(self):
        client = Client()
        testPersonName = self.faker.first_name()
        
        response = client.post('/api/people/create/', {
            'last_name': self.faker.last_name(),
            'first_name': testPersonName,
            'external_id': self.faker.ean(length=8),
            'mail': self.faker.email(),
            'photo': self.faker.image_url(width = 640, height = 480),
            'born_at': self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            'referee': False,
            'official': False,
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_person(self):

        client = Client()
        newPersonName = self.faker.first_name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        client.login(username='usuario', password='usuario')
        
        updatedPersonName = self.faker.first_name()
        updatedMail = self.faker.email()

        response = client.put(f'/api/people/{newPerson._id}/update/', {
            'last_name': self.faker.last_name(),
            'first_name': updatedPersonName,
            'external_id': self.faker.ean(length=8),
            'mail': updatedMail,
            'photo': self.faker.image_url(width = 640, height = 480),
            'born_at': self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            'referee': False,
            'official': False,
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Person.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedPersonName, lastUpdated.first_name)
        self.assertEquals(updatedMail, lastUpdated.mail)

 
    def test_user_cannot_edit_making_blank_person(self):


        client = Client()
        newPersonName = self.faker.name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        client.login(username='usuario', password='usuario')
        
        updatedPersonName = ''

        response = client.put(f'/api/people/{newPerson._id}/update/', {
                'last_name': self.faker.last_name(),
                'first_name': updatedPersonName,
                'external_id': self.faker.ean(length=8),
                'mail': self.faker.email(),
                'photo': self.faker.image_url(width = 640, height = 480),
                'born_at': self.faker.date_of_birth(minimum_age=35, maximum_age=65),
                'referee': False,
                'official': False,
            }, 
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)

    
    def test_user_cannot_delete_person(self):

        client = Client()
        newPersonName = self.faker.name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        totalPeopleBefore = Person.objects.all().count()
        
        client.login(username='other', password='other')

        response = client.delete(f'/api/people/{newPerson._id}/delete/')
        
        totalPeopleAfter = Person.objects.all().count()
        
        self.assertEquals(response.status_code, 403)
        self.assertEquals(totalPeopleBefore, totalPeopleAfter)
    
    def test_adminuser_can_delete_not_own_person(self):
 
        client = Client()
        newPersonName = self.faker.name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        totalPeopleBefore = Person.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/people/{newPerson._id}/delete/')
        
        totalPeopleAfter = Person.objects.all().count()
        totalDeleted = Person.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalPeopleBefore, totalPeopleAfter + 1)
        self.assertEquals(totalDeleted, 1)

    def test_get_person_by_id(self):
        
        client = Client()
        newPersonName = self.faker.name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        response = client.get(f'/api/people/{newPerson._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['first_name'], newPersonName)
    
    def test_list_user_people(self):
        
        client = Client()
        newPerson1Name = self.faker.first_name()
        newPerson2Name = self.faker.first_name()
        newPerson3Name = self.faker.first_name()
        
        
        newPerson1 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPerson1Name,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPerson2 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPerson2Name,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPerson3 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPerson3Name,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            photo = self.faker.image_url(width = 640, height = 480),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/people/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)

    def test_upload_photo(self):

        client = Client()
        newPersonName = self.faker.name()
        
        newPerson = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = newPersonName,
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        image = Image.new('RGB', (100, 100))
        fileName = self.faker.file_name(category='image')
        imageFile = SimpleUploadedFile(fileName, 
            image.tobytes(), content_type="image/jpg")
        
        client.login(username='usuario', password='usuario')

        response = client.post('/api/people/upload/',{ 
                'person_id': newPerson._id,
                'image': imageFile
            },         
            format='multipart/form-data')

        updatedPerson = Person.objects.get(pk=newPerson._id)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedPerson.photo, fileName)