from django.test import TestCase, Client
from base.models import Squad, Category, Tournament, Team, Person
from django.contrib.auth.models import User
from faker import Faker

class SquadTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')
        self.faker = Faker()
        self.category = Category.objects.create(name=self.faker.name(),user=self.user)
        self.tournament = Tournament.objects.create(
            name=self.faker.name(),
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )
        self.team = Team.objects.create(name=self.faker.name())

    def test_user_can_create_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        totalSquadsBefore = Squad.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/squads/create/', {
            'name': testSquadName,
            'team_id': self.team._id,
            'tournament_id': self.tournament._id,
        })
        
        totalSquadsAfter = Squad.objects.all().count()
        
        lastCreatedName = Squad.objects.latest('createdAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalSquadsBefore + 1, totalSquadsAfter)
        self.assertEquals(testSquadName, lastCreatedName)

    def test_user_cannot_create_blank_name_or_season_or_category(self):
        client = Client()
        testSquadName = ''

        client.login(username='usuario', password='usuario')

        response = client.post('/api/squads/create/', {
            'name': testSquadName,
            'team': self.team,
            'tournament': self.tournament,
        })
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_squad(self):
        client = Client()
        testSquadName = self.faker.name()
        
        response = client.post('/api/squads/create/', {
            'name': testSquadName,
            'team': self.team,
            'tournament': self.tournament,
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_squad(self):

        client = Client()
        newSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = newSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        client.login(username='usuario', password='usuario')
        
        updatedSquadName = self.faker.name()
        otherTeam = Team.objects.create(name=self.faker.name())
        response = client.put(f'/api/squads/{newSquad._id}/update/', {
                'name': updatedSquadName,
                'team_id': otherTeam._id,
                'tournament_id': self.tournament._id
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Squad.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedSquadName, lastUpdated.name)
        self.assertEquals(otherTeam._id, lastUpdated.team_id)

    def test_user_cannot_edit_not_own_squad(self):

        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        client.login(username='other', password='other')
        
        updatedSquadName = self.faker.name()

        response = client.put(f'/api/squads/{newSquad._id}/update/', {
                'name': testSquadName,
                'team_id': self.team._id,
                'tournament_id': self.tournament._id,
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 400)

    def test_user_cannot_edit_making_blank_squad(self):


        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        client.login(username='usuario', password='usuario')
        
        updatedSquadName = ''

        response = client.put(f'/api/squads/{newSquad._id}/update/', {
                'name': updatedSquadName,
                'team_id': self.team._id,
                'tournament_id': self.tournament._id,
            }, 
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)

    def test_adminuser_can_edit_not_own_squad(self):
        
        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        client.login(username='admin', password='admin')
        
        updatedSquadName = self.faker.name()
        updatedTeam = Team.objects.create(name=self.faker.name())
        
        response = client.put(f'/api/squads/{newSquad._id}/update/', {
                'name': updatedSquadName,
                'team_id': updatedTeam._id,
                'tournament_id': self.tournament._id,
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Squad.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedSquadName, lastUpdated.name)
        self.assertEquals(updatedTeam._id, lastUpdated.team_id)

    def test_user_can_delete_own_squad(self):
        
        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        totalSquadsBefore = Squad.objects.all().count()

        client.login(username='usuario', password='usuario')

        response = client.delete(f'/api/squads/{newSquad._id}/delete/')
        
        totalSquadsAfter = Squad.objects.all().count()
        totalDeleted = Squad.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalSquadsBefore, totalSquadsAfter + 1)
        self.assertEquals(totalDeleted, 1)
    
    def test_user_cannot_delete_not_own_squad(self):

        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        totalSquadsBefore = Squad.objects.all().count()
        
        client.login(username='other', password='other')

        response = client.delete(f'/api/squads/{newSquad._id}/delete/')
        
        totalSquadsAfter = Squad.objects.all().count()
        
        self.assertEquals(response.status_code, 400)
        self.assertEquals(totalSquadsBefore, totalSquadsAfter)
    
    def test_adminuser_can_delete_not_own_squad(self):
 
        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        totalSquadsBefore = Squad.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/squads/{newSquad._id}/delete/')
        
        totalSquadsAfter = Squad.objects.all().count()
        totalDeleted = Squad.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalSquadsBefore, totalSquadsAfter + 1)
        self.assertEquals(totalDeleted, 1)

    def test_get_squad_by_id(self):
        
        client = Client()
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        response = client.get(f'/api/squads/{newSquad._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['name'], testSquadName)
    
    def test_list_user_squads(self):
        
        client = Client()
        newSquad1Name = self.faker.name()
        newSquad2Name = self.faker.name()
        newSquad3Name = self.faker.name()
        
        otherTeam = Team.objects.create(name=self.faker.name())
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)

        newSquad1 = Squad.objects.create(name=newSquad1Name, 
            team=self.team, tournament=self.tournament)
        newSquad2 = Squad.objects.create(name=newSquad2Name, 
            team=self.team, tournament=self.tournament)
        newSquad3 = Squad.objects.create(name=newSquad3Name, 
            team=otherTeam, tournament=otherTournament)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/squads/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 2)

    
    def test_admin_list_all_categories(self):

        client = Client()
        newSquad1Name = self.faker.name()
        newSquad2Name = self.faker.name()
        newSquad3Name = self.faker.name()
        
        otherTeam = Team.objects.create(name=self.faker.name())
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)

        newSquad1 = Squad.objects.create(name=newSquad1Name, 
            team=self.team, tournament=self.tournament)
        newSquad2 = Squad.objects.create(name=newSquad2Name, 
            team=self.team, tournament=self.tournament)
        newSquad3 = Squad.objects.create(name=newSquad3Name, 
            team=otherTeam, tournament=otherTournament)
        
        client.login(username='admin', password='admin')

        response = client.get('/api/squads/list/')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)
    
    def test_notadminuser_cant_list_all_categories(self):
        fake = Faker()
        client = Client()
        newSquad1Name = self.faker.name()
        newSquad2Name = self.faker.name()
        newSquad3Name = self.faker.name()
        
        otherTeam = Team.objects.create(name=self.faker.name())
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)

        newSquad1 = Squad.objects.create(name=newSquad1Name, 
            team=self.team, tournament=self.tournament)
        newSquad2 = Squad.objects.create(name=newSquad2Name, 
            team=self.team, tournament=self.tournament)
        newSquad3 = Squad.objects.create(name=newSquad3Name, 
            team=otherTeam, tournament=otherTournament)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/categories/list/')

        self.assertEquals(response.status_code, 403)

    def test_user_can_add_player_to_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='usuario', password='usuario')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        response = client.post(f'/api/squads/{newSquad._id}/add-player/', {
            'player_id': newPlayer._id
        })

        newSquad = Squad.objects.get(pk=newSquad._id)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(newSquad.players.count(), 1)

    def test_user_can_remove_player_from_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='usuario', password='usuario')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPlayer2 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newSquad.players.add(newPlayer)
        newSquad.players.add(newPlayer2)

        response = client.post(f'/api/squads/{newSquad._id}/remove-player/', {
            'player_id': newPlayer._id
        })

        newSquad = Squad.objects.get(pk=newSquad._id)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(newSquad.players.count(), 1)

    def test_user_cannot_add_player_to_notown_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='other', password='other')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )

        response = client.post(f'/api/squads/{newSquad._id}/add-player/', {
            'player_id': newPlayer._id
        })

        newSquad = Squad.objects.get(pk=newSquad._id)

        self.assertEquals(response.status_code, 400)
        self.assertEquals(newSquad.players.count(), 0)

    def test_user_cannot_remove_player_from_notown_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='other', password='other')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPlayer2 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newSquad.players.add(newPlayer)
        newSquad.players.add(newPlayer2)

        response = client.post(f'/api/squads/{newSquad._id}/remove-player/', {
            'player_id': newPlayer._id
        })

        newSquad = Squad.objects.get(pk=newSquad._id)

        self.assertEquals(response.status_code, 400)
        self.assertEquals(newSquad.players.count(), 2)

    def test_error_when_add_player_who_is_in_another_squad_same_tournament(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='usuario', password='usuario')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )
        otherTeam = Team.objects.create(
            name = self.faker.name()
        )
        otherTournanment = Tournament.objects.create(
            name = self.faker.name(),
            category = self.category
        )
        otherSquad = Squad.objects.create(
            name = self.faker.name(),
            team = otherTeam,
            tournament = self.tournament,
        )
        squadInOtherTourn = Squad.objects.create(
            name = self.faker.name(),
            team = otherTeam,
            tournament = otherTournanment
        )
        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPlayer2 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newSquad.players.add(newPlayer)
        newSquad.players.add(newPlayer2)

        response = client.post(f'/api/squads/{otherSquad._id}/add-player/', {
            'player_id': newPlayer._id
        })

        # error when trying to add a player that is in another squad in same tournament
        self.assertEquals(response.status_code, 400)

        response = client.post(f'/api/squads/{squadInOtherTourn._id}/add-player/', {
            'player_id': newPlayer._id
        })

        # ok if players is added in squad of other tournament
        self.assertEquals(response.status_code, 200)


    def test_error_when_remove_player_who_is_not_in_squad(self):
        client = Client()
        testSquadName = self.faker.name()

        client.login(username='usuario', password='usuario')
        
        testSquadName = self.faker.name()
        
        newSquad = Squad.objects.create(
            name = testSquadName,
            team = self.team,
            tournament = self.tournament,
        )

        newPlayer = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newPlayer2 = Person.objects.create(
            last_name = self.faker.last_name(),
            first_name = self.faker.first_name(),
            external_id = self.faker.ean(length=8),
            mail = self.faker.email(),
            born_at = self.faker.date_of_birth(minimum_age=35, maximum_age=65),
            referee = False,
            official = False,
        )
        newSquad.players.add(newPlayer)
        
        response = client.post(f'/api/squads/{newSquad._id}/remove-player/', {
            'player_id': newPlayer2._id
        })

        newSquad = Squad.objects.get(pk=newSquad._id)

        self.assertEquals(newSquad.players.count(), 1)

    def test_get_squad_with_players(self):
        pass

    def test_get_squads_with_players(self):
        pass

