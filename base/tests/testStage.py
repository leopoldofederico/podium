from django.test import TestCase, Client
from base.models import Stage, Category, Tournament, Team
from django.contrib.auth.models import User
from faker import Faker

class StageTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')
        self.faker = Faker()
        self.category = Category.objects.create(name=self.faker.name(),user=self.user)
        self.tournament = Tournament.objects.create(
            name=self.faker.name(), 
            category=self.category)

    def test_user_can_create_stage(self):
        client = Client()
        testStageName = self.faker.name()
        team1 = Team.objects.create(
            name=self.faker.name()
        )
        team2 = Team.objects.create(
            name=self.faker.name()
        )
        totalStagesBefore = Stage.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/stages/create/', {
            'name': testStageName,
            'tournament_id': self.tournament._id,
            'competitors': [team1._id, team2._id]
        })

        totalStagesAfter = Stage.objects.all().count()
        
        lastCreatedName = Stage.objects.latest('createdAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalStagesBefore + 1, totalStagesAfter)
        self.assertEquals(testStageName, lastCreatedName)

    def test_user_cannot_create_blank_name_or_season_or_category(self):
        client = Client()
        testStageName = ''

        client.login(username='usuario', password='usuario')

        response = client.post('/api/stages/create/', {
            'name': testStageName,
            'tournament_id': self.tournament._id
        })
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_stage(self):
        client = Client()
        testStageName = self.faker.name()
        
        response = client.post('/api/stages/create/', {
            'name': testStageName,
            'tournament_id': self.tournament._id
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_stage(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament_id = self.tournament._id
        )

        client.login(username='usuario', password='usuario')
        
        updatedStageName = self.faker.name()

        otherTournament = Tournament.objects.create(name=self.faker.name(),category=self.category)

        response = client.put(f'/api/stages/{newStage._id}/update/', {
                'name': updatedStageName,
                'tournament_id': otherTournament._id 
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Stage.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedStageName, lastUpdated.name)
        self.assertEquals(otherTournament._id, lastUpdated.tournament_id)

    def test_user_cannot_edit_not_own_stage(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        client.login(username='other', password='other')
        
        updatedStageName = self.faker.name()

        response = client.put(f'/api/stages/{newStage._id}/update/', {
                'name': updatedStageName,
                'tournament_id': self.tournament._id
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 400)

    def test_user_cannot_edit_making_blank_stage(self):


        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        client.login(username='usuario', password='usuario')
        
        updatedStageName = ''
        otherTournament = Tournament.objects.create(name=self.faker.name(),category=self.category)

        response = client.put(f'/api/stages/{newStage._id}/update/', {
                'name': updatedStageName,
                'tournament_id': otherTournament._id
            }, 
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)

    def test_adminuser_can_edit_not_own_stage(self):
        
        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        client.login(username='admin', password='admin')
        
        updatedStageName = self.faker.name()
        otherTournament = Tournament.objects.create(name=self.faker.name(),category=self.category)

        response = client.put(f'/api/stages/{newStage._id}/update/', {
                'name': updatedStageName,
                'tournament_id': otherTournament._id
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Stage.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedStageName, lastUpdated.name)
        self.assertEquals(otherTournament._id, lastUpdated.tournament_id)

    def test_user_can_delete_own_stage(self):
        
        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        totalStagesBefore = Stage.objects.all().count()

        client.login(username='usuario', password='usuario')

        response = client.delete(f'/api/stages/{newStage._id}/delete/')
        
        totalStagesAfter = Stage.objects.all().count()
        totalDeleted = Stage.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalStagesBefore, totalStagesAfter + 1)
        self.assertEquals(totalDeleted, 1)
    
    def test_user_cannot_delete_not_own_stage(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        totalStagesBefore = Stage.objects.all().count()
        
        client.login(username='other', password='other')

        response = client.delete(f'/api/stages/{newStage._id}/delete/')
        
        totalStagesAfter = Category.objects.all().count()
        
        self.assertEquals(response.status_code, 400)
        self.assertEquals(totalStagesBefore, totalStagesAfter)
    
    def test_adminuser_can_delete_not_own_stage(self):
 
        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        totalStagesBefore = Stage.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/stages/{newStage._id}/delete/')
        
        totalStagesAfter = Stage.objects.all().count()
        totalDeleted = Stage.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalStagesBefore, totalStagesAfter + 1)
        self.assertEquals(totalDeleted, 1)

    def test_get_stage_by_id(self):
        
        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        response = client.get(f'/api/stages/{newStage._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['name'], newStageName)
    
    def test_list_user_stages(self):
        
        client = Client()
        newStage1Name = self.faker.name()
        newStage2Name = self.faker.name()
        newStage3Name = self.faker.name()
        
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)
        newStage1 = Stage.objects.create(name=newStage1Name, 
            tournament=self.tournament)
        newStage2 = Stage.objects.create(name=newStage2Name, 
            tournament=self.tournament)
        newStage3 = Stage.objects.create(name=newStage3Name, 
            tournament=otherTournament)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/stages/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 2)

    
    def test_admin_list_all_categories(self):

        client = Client()
        newStage1Name = self.faker.name()
        newStage2Name = self.faker.name()
        newStage3Name = self.faker.name()
        
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)

        newStage1 = Stage.objects.create(name=newStage1Name, 
            tournament=self.tournament)
        newStage2 = Stage.objects.create(name=newStage2Name, 
            tournament=self.tournament)
        newStage3 = Stage.objects.create(name=newStage3Name, 
            tournament=otherTournament)
        
        client.login(username='admin', password='admin')

        response = client.get('/api/stages/list/')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)
    
    def test_notadminuser_cant_list_all_categories(self):
        fake = Faker()
        client = Client()
        newStage1Name = self.faker.name()
        newStage2Name = self.faker.name()
        newStage3Name = self.faker.name()
        
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        otherTournament = Tournament.objects.create(name=self.faker.name(), category=otherCategory)

        newStage1 = Stage.objects.create(name=newStage1Name, 
            tournament=self.tournament)
        newStage2 = Stage.objects.create(name=newStage2Name, 
            tournament=self.tournament)
        newStage3 = Stage.objects.create(name=newStage3Name, 
            tournament=otherTournament)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/stages/list/')

        self.assertEquals(response.status_code, 403)
    
    def test_user_add_team_to_own_stage(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        team1 = Team.objects.create(
            name = self.faker.name()
        )

        client.login(username='admin', password='admin')

        response = client.put(f'/api/stages/{newStage._id}/add-team/',  {
                'team_id': team1._id
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(newStage.competitors.count(), 1)

    def test_user_remove_team_to_own_stage(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )

        team1 = Team.objects.create(
            name = self.faker.name()
        )
        team2 = Team.objects.create(
            name = self.faker.name()
        )
        team3 = Team.objects.create(
            name = self.faker.name()
        )
        newStage.competitors.add(team1._id)
        newStage.competitors.add(team2._id)
        newStage.competitors.add(team3._id)

        newStage.save()
        client.login(username='admin', password='admin')

        response = client.put(f'/api/stages/{newStage._id}/remove-team/',  {
                'team_id': team1._id
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(newStage.competitors.count(), 2)

    def test_generate_round_robin_matches_home_and_away(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )
        
        for i in range(5):
            team = Team.objects.create(
                name = self.faker.name()
            )
            newStage.competitors.add(team)
        newStage.save()
        newStage = Stage.objects.get(pk=newStage._id)

        client.force_login(user=self.user)

        response = client.post(f'/api/stages/{newStage._id}/round-robin/',  {
            'mode': 'home-away'
        })
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(newStage.match_set.count(), 20)

    def test_generate_round_robin_matches_home_and_away(self):

        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )
        
        for i in range(5):
            team = Team.objects.create(
                name = self.faker.name()
            )
            newStage.competitors.add(team)
        newStage.save()
        newStage = Stage.objects.get(pk=newStage._id)

        client.force_login(user=self.user)

        response = client.post(f'/api/stages/{newStage._id}/round-robin/',  {
            'mode': 'home-away'
        })
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(newStage.match_set.count(), 20)

    def test_generate_round_robin_matches_one_match(self):
    
        client = Client()
        newStageName = self.faker.name()
        
        newStage = Stage.objects.create(
            name = newStageName,
            tournament = self.tournament
        )
        
        for i in range(5):
            team = Team.objects.create(
                name = self.faker.name()
            )
            newStage.competitors.add(team)
        newStage.save()
        newStage = Stage.objects.get(pk=newStage._id)

        client.force_login(user=self.user)

        response = client.post(f'/api/stages/{newStage._id}/round-robin/',  {
            'mode': 'one-match'
        })
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(newStage.match_set.count(), 10)
    
    def test_list_standings:
        