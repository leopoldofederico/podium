from django.test import TestCase, Client
from base.models import Team
from django.contrib.auth.models import User
from faker import Faker
from django.core.files.uploadedfile import SimpleUploadedFile
import tempfile
from PIL import Image

class TeamTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')
        self.faker = Faker()

    def test_user_can_create_team(self):
        client = Client()
        testTeamName = self.faker.name()

        totalTeamsBefore = Team.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/teams/create/', {
            'name': testTeamName
            # shield = self.faker.name()
        })
        print(response)
        totalTeamsAfter = Team.objects.all().count()
        
        lastCreatedName = Team.objects.latest('createdAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTeamsBefore + 1, totalTeamsAfter)
        self.assertEquals(testTeamName, lastCreatedName)

    def test_user_cannot_create_blank_name(self):
        client = Client()
        testTeamName = ''

        client.login(username='usuario', password='usuario')

        response = client.post('/api/teams/create/', {
            'name': testTeamName
            # shield = self.faker.name()
        })
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_team(self):
        client = Client()
        testTeamName = self.faker.name()
        
        response = client.post('/api/teams/create/', {
            'name': self.faker.name()
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_adminuser_can_edit_own_team(self):

        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        client.login(username='admin', password='admin')
        
        updatedTeamName = self.faker.name()

        response = client.put(f'/api/teams/{newTeam._id}/update/', {
                'name': updatedTeamName,
                'shield': self.faker.name()
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Team.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedTeamName, lastUpdated.name)
        

    def test_user_cannot_edit_team(self):

        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        client.login(username='other', password='other')
        
        updatedTeamName = self.faker.name()

        response = client.put(f'/api/teams/{newTeam._id}/update/', {
                'name': updatedTeamName,
                'shield': self.faker.name()
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 403)

    def test_adminuser_cannot_edit_making_blank_team(self):


        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        client.login(username='admin', password='admin')
        
        updatedTeamName = ''

        response = client.put(f'/api/teams/{newTeam._id}/update/', {
                'name': updatedTeamName,
                'shield': self.faker.name()
            }, 
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)

    def test_adminuser_can__team(self):
        
        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        totalTeamsBefore = Team.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/teams/{newTeam._id}/delete/')
        
        totalTeamsAfter = Team.objects.all().count()
        totalDeleted = Team.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTeamsBefore, totalTeamsAfter + 1)
        self.assertEquals(totalDeleted, 1)
    
    def test_user_cannot_delete_team(self):

        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        totalTeamsBefore = Team.objects.all().count()
        
        client.login(username='other', password='other')

        response = client.delete(f'/api/teams/{newTeam._id}/delete/')
        
        totalTeamsAfter = Team.objects.all().count()
        
        self.assertEquals(response.status_code, 403)
        self.assertEquals(totalTeamsBefore, totalTeamsAfter)
    

    def test_get_team_by_id(self):
        
        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
            shield = self.faker.name()
        )

        response = client.get(f'/api/teams/{newTeam._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['name'], newTeamName)
    
    def test_list_teams(self):
        
        client = Client()
        newTeam1Name = self.faker.name()
        newTeam2Name = self.faker.name()
        newTeam3Name = self.faker.name()
        
        newTeam1 = Team.objects.create(name=newTeam1Name)
        newTeam2 = Team.objects.create(name=newTeam2Name)
        newTeam3 = Team.objects.create(name=newTeam3Name)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/teams/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)

    def test_upload_shield(self):

        client = Client()
        newTeamName = self.faker.name()
        
        newTeam = Team.objects.create(
            name = newTeamName,
        )
        image = Image.new('RGB', (100, 100))
        fileName = self.faker.file_name(category='image')
        imageFile = SimpleUploadedFile(fileName, 
            image.tobytes(), content_type="image/jpg")
        client.login(username='usuario', password='usuario')

        response = client.post('/api/teams/upload/',{ 
                'team_id': newTeam._id,
                'image': imageFile
            },         
            format='multipart/form-data')

        updatedTeam = Team.objects.get(pk=newTeam._id)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedTeam.shield, fileName)

