from django.test import TestCase, Client
from base.models import Category, Tournament
from django.contrib.auth.models import User
from faker import Faker

class TournamentTestCase(TestCase):
    

    def setUp(self):
        self.admin = User.objects.create_superuser('admin', 'myemail@test.com', 'admin')
        self.user = User.objects.create_user('usuario', 'user@email.com', 'usuario')
        self.otherUser = User.objects.create_user('other', 'other@email.com', 'other')
        self.faker = Faker()
        self.category = Category.objects.create(name=self.faker.name(),user=self.user)

    def test_user_can_create_tournament(self):
        client = Client()
        testTournamentName = self.faker.name()

        totalTournamentsBefore = Tournament.objects.all().count()
        
        client.login(username='usuario', password='usuario')
        
        response = client.post('/api/tournaments/create/', {
            'name' : testTournamentName,
            'season' : self.faker.name(),
            'category_id' : self.category._id,
            'isCurrent' : True
        })
        
        totalTournamentsAfter = Tournament.objects.all().count()
        
        lastCreatedName = Tournament.objects.latest('createdAt').name
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTournamentsBefore + 1, totalTournamentsAfter)
        self.assertEquals(testTournamentName, lastCreatedName)

    def test_user_cannot_create_blank_name_or_season_or_category(self):
        client = Client()
        testTournamentName = ''

        client.login(username='usuario', password='usuario')

        response = client.post('/api/tournaments/create/', {
            'name' : '',
            'season' : self.faker.name(),
            'category_id' : self.category._id,
            'isCurrent' : True
        })
        
        self.assertEquals(response.status_code, 400)

        response = client.post('/api/tournaments/create/', {
            'name' : self.faker.name(),
            'season' : '',
            'category_id' : self.category._id,
            'isCurrent' : True
        })
        
        self.assertEquals(response.status_code, 400)

        response = client.post('/api/tournaments/create/', {
            'name' : self.faker.name(),
            'season' : self.faker.name(),
            'category_id' : 0,
            'isCurrent' : True
        })
        
        self.assertEquals(response.status_code, 400)
        
    def test_notlogged_user_cannot_create_tournament(self):
        client = Client()
        testTournamentName = self.faker.name()
        
        response = client.post('/api/tournaments/create/', {
            'name' : testTournamentName,
            'season' : self.faker.name(),
            'category_id' : self.category._id,
            'isCurrent' : True
        })
        
        self.assertEquals(response.status_code, 403)
        
    def test_user_can_edit_own_tournament(self):

        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        client.login(username='usuario', password='usuario')
        
        updatedTournamentName = self.faker.name()
        updatedSeason = self.faker.name()
        otherCategory = Category.objects.create(name=self.faker.name(),user=self.user)

        response = client.put(f'/api/tournaments/{newTournament._id}/update/', {
                'name': updatedTournamentName,
                'season': updatedSeason,
                'category_id': otherCategory._id,
                'isCurrent': "False"
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Tournament.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedTournamentName, lastUpdated.name)
        self.assertEquals(updatedSeason, lastUpdated.season)
        self.assertEquals(otherCategory._id, lastUpdated.category_id)

    def test_user_cannot_edit_not_own_tournament(self):

        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        client.login(username='other', password='other')
        
        updatedTournamentName = self.faker.name()

        response = client.put(f'/api/tournaments/{newTournament._id}/update/', {
                'name': updatedTournamentName,
                'season': self.faker.name(),
                'category_id': self.category._id,
                'isCurrent': "False"
            }, 
            content_type='application/json')
        
        self.assertEquals(response.status_code, 401)

    def test_user_cannot_edit_making_blank_tournament(self):


        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        client.login(username='usuario', password='usuario')
        
        updatedTournamentName = self.faker.name()
        updatedSeason = self.faker.name()
        otherCategory = Category.objects.create(name=self.faker.name(),user=self.user)

        response = client.put(f'/api/tournaments/{newTournament._id}/update/', {
                'name': '',
                'season': updatedSeason,
                'category_id': otherCategory._id,
                'isCurrent': "False"
            }, 
            content_type='application/json'
        )
        
        self.assertEquals(response.status_code, 400)

    def test_adminuser_can_edit_not_own_tournament(self):
        
        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        client.login(username='admin', password='admin')
        
        updatedTournamentName = self.faker.name()
        updatedSeason = self.faker.name()
        otherCategory = Category.objects.create(name=self.faker.name(),user=self.user)

        response = client.put(f'/api/tournaments/{newTournament._id}/update/', {
                'name': updatedTournamentName,
                'season': updatedSeason,
                'category_id': otherCategory._id,
                'isCurrent': "False"
            }, 
            content_type='application/json'
        )
        
        lastUpdated = Tournament.objects.latest('updatedAt')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(updatedTournamentName, lastUpdated.name)
        self.assertEquals(updatedSeason, lastUpdated.season)
        self.assertEquals(otherCategory._id, lastUpdated.category_id)

    def test_user_can_delete_own_tournament(self):
        
        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        totalTournamentsBefore = Tournament.objects.all().count()

        client.login(username='usuario', password='usuario')

        response = client.delete(f'/api/tournaments/{newTournament._id}/delete/')
        
        totalTournamentsAfter = Tournament.objects.all().count()
        totalDeleted = Tournament.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTournamentsBefore, totalTournamentsAfter + 1)
        self.assertEquals(totalDeleted, 1)

        response = client.delete(f'/api/tournaments/{newTournament._id}/undelete/')
        totalTournamentsAfter = Tournament.objects.all().count()
        totalDeleted = Tournament.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTournamentsBefore, totalTournamentsAfter)
        self.assertEquals(totalDeleted, 0)

    
    def test_user_cannot_delete_not_own_tournament(self):

        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        totalTournamentsBefore = Tournament.objects.all().count()
        
        client.login(username='other', password='other')

        response = client.delete(f'/api/tournaments/{newTournament._id}/delete/')
        
        totalTournamentsAfter = Category.objects.all().count()
        
        self.assertEquals(response.status_code, 400)
        self.assertEquals(totalTournamentsBefore, totalTournamentsAfter)
    
    def test_adminuser_can_delete_not_own_tournament(self):
 
        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        totalTournamentsBefore = Tournament.objects.all().count()

        client.login(username='admin', password='admin')

        response = client.delete(f'/api/tournaments/{newTournament._id}/delete/')
        
        totalTournamentsAfter = Tournament.objects.all().count()
        totalDeleted = Tournament.deleted_objects.all().count()
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(totalTournamentsBefore, totalTournamentsAfter + 1)
        self.assertEquals(totalDeleted, 1)

    def test_get_tournament_by_id(self):
        
        client = Client()
        newTournamentName = self.faker.name()
        
        newTournament = Tournament.objects.create(
            name=newTournamentName,
            season=self.faker.name(),
            category=self.category,
            isCurrent=True
        )

        response = client.get(f'/api/tournaments/{newTournament._id}/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['name'], newTournamentName)
    
    def test_list_user_tournaments(self):
        
        client = Client()
        newTournament1Name = self.faker.name()
        newTournament2Name = self.faker.name()
        newTournament3Name = self.faker.name()
        
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        newTournament1 = Tournament.objects.create(name=newTournament1Name, 
            season=self.faker.name(), category=self.category)
        newTournament2 = Tournament.objects.create(name=newTournament2Name, 
            season=self.faker.name(), category=self.category)
        newTournament3 = Tournament.objects.create(name=newTournament3Name, 
            season=self.faker.name(), category=otherCategory)
        
        client.login(username='usuario', password='usuario')

        response = client.get('/api/tournaments/')

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 2)

    
    def test_admin_list_all_categories(self):

        client = Client()
        newTournament1Name = self.faker.name()
        newTournament2Name = self.faker.name()
        newTournament3Name = self.faker.name()
        
        otherCategory = Category.objects.create(name=self.faker.name(), user=self.otherUser)
        newTournament1 = Tournament.objects.create(name=newTournament1Name, 
            season=self.faker.name(), category=self.category)
        newTournament2 = Tournament.objects.create(name=newTournament2Name, 
            season=self.faker.name(), category=self.category)
        newTournament3 = Tournament.objects.create(name=newTournament3Name, 
            season=self.faker.name(), category=otherCategory)
        
        client.login(username='admin', password='admin')

        response = client.get('/api/tournaments/list/')
        
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 3)
    
    # def test_notadminuser_cant_list_all_categories(self):
    #     fake = Faker()
    #     client = Client()
    #     newCategory1Name = fake.name()
    #     newCategory2Name = fake.name()
    #     newCategory3Name = fake.name()
        
    #     newCategory1 = Category.objects.create(name=newCategory1Name, user=self.user)
    #     newCategory2 = Category.objects.create(name=newCategory2Name, user=self.admin)
    #     newCategory3 = Category.objects.create(name=newCategory3Name, user=self.otherUser)
        
    #     client.login(username='usuario', password='usuario')

    #     response = client.get('/api/categories/list/')

    #     self.assertEquals(response.status_code, 403)