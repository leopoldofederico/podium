from django.urls import path
from django.contrib.auth.models import User
from base.views import category_views as views
# from django.contrib import admin

urlpatterns = [
    path('', views.listUserCategories, name='user-categories'),
    # path('', admin.site.urls),
    path('list/', views.listCategories, name='categories'),
    path('create/', views.createCategory, name='create-category'),

    path('<str:pk>/', views.getCategory, name='get-category'),
    path('<str:pk>/update/', views.updateCategory, name='udpate-category'),
    path('<str:pk>/delete/', views.deleteCategory, name='delete-category'),
    path('<str:pk>/undelete/', views.undeleteCategory, name='undelete-category'),
]