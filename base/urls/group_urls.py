from django.urls import path
from django.contrib.auth.models import User
from base.views import group_views as views


urlpatterns = [
    path('', views.listUserGroups, name='groups'),
    path('list/', views.listGroups, name='groups'),
    path('create/', views.createGroup, name='create-group'),

    path('<str:pk>/', views.getGroup, name='get-group'),
    path('<str:pk>/add-team/', views.addTeam, name='add-team'),
    path('<str:pk>/remove-team/', views.removeTeam, name='remove-team'),
    path('<str:pk>/update/', views.updateGroup, name='udpate-group'),
    path('<str:pk>/delete/', views.deleteGroup, name='delete-group'),
    path('<str:pk>/undelete/', views.undeleteGroup, name='undelete-group'),

    path('<str:pk>/round-robin/', views.generateRoundRobinMatches, name='round-robin'),
    path('<str:pk>/standings/', views.listStandings, name='standings'),
]
