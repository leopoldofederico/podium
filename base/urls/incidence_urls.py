from django.urls import path
from django.contrib.auth.models import User
from base.views import incidence_views as views


urlpatterns = [
    path('', views.listUserIncidences, name='user-incidencees'),
    path('list/', views.listIncidences, name='incidencees'),
    path('create/', views.createIncidence, name='create-incidence'),

    path('<str:pk>/', views.getIncidence, name='get-incidence'),
    path('<str:pk>/update/', views.updateIncidence, name='udpate-incidence'),
    path('<str:pk>/delete/', views.deleteIncidence, name='delete-incidence'),
    path('<str:pk>/undelete/', views.undeleteIncidence, name='undelete-incidence'),
]