from django.urls import path
from django.contrib.auth.models import User
from base.views import match_views as views


urlpatterns = [
    path('', views.listUserMatches, name='user-matches'),
    path('list/', views.listMatches, name='matches'),
    path('create/', views.createMatch, name='create-match'),

    path('<str:pk>/', views.getMatch, name='get-match'),
    path('<str:pk>/update/', views.updateMatch, name='udpate-match'),
    path('<str:pk>/delete/', views.deleteMatch, name='delete-match'),
    path('<str:pk>/undelete/', views.undeleteMatch, name='undelete-match'),
]