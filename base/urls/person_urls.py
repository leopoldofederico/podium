from django.urls import path
from django.contrib.auth.models import User
from base.views import person_views as views


urlpatterns = [
    path('', views.listPeople, name='people'),
    path('list/', views.listPeople, name='people'),
    path('create/', views.createPerson, name='create-person'),
    path('upload/', views.uploadPhoto, name='upload-photo'),

    path('<str:pk>/', views.getPerson, name='get-person'),
    path('<str:pk>/update/', views.updatePerson, name='udpate-person'),
    path('<str:pk>/delete/', views.deletePerson, name='delete-person'),
]