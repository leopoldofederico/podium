from django.urls import path
from base.models import Place
from base.views import place_views as views


urlpatterns = [
    path('', views.listPlaces, name='places'),
    path('<str:pk>/', views.getPlace, name='get-place'),
]