from django.urls import path
from django.contrib.auth.models import User
from base.views import squad_views as views


urlpatterns = [
    path('', views.listUserSquads, name='user-squads'),
    path('list/', views.listSquads, name='squads'),
    path('create/', views.createSquad, name='create-squad'),

    path('<str:pk>/', views.getSquad, name='get-squad'),
    path('<str:pk>/update/', views.updateSquad, name='udpate-squad'),
    path('<str:pk>/delete/', views.deleteSquad, name='delete-squad'),
    path('<str:pk>/undelete/', views.undeleteSquad, name='undelete-squad'),
    path('<str:pk>/add-player/', views.addPlayer, name='add-player'),
    path('<str:pk>/remove-player/', views.removePlayer, name='remove-player'),
    path('<str:pk>/matches/<str:stage>/', views.listMatches, name='list-matches'),
]