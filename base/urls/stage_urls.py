from django.urls import path
from django.contrib.auth.models import User
from base.views import stage_views as views


urlpatterns = [
    path('', views.listUserStages, name='stages'),
    path('list/', views.listStages, name='stages'),
    path('create/', views.createStage, name='create-stage'),

    path('<str:pk>/', views.getStage, name='get-stage'),
    path('<str:pk>/add-team/', views.addTeam, name='add-team'),
    path('<str:pk>/remove-team/', views.removeTeam, name='remove-team'),
    path('<str:pk>/update/', views.updateStage, name='udpate-stage'),
    path('<str:pk>/delete/', views.deleteStage, name='delete-stage'),
    path('<str:pk>/undelete/', views.undeleteStage, name='undelete-stage'),

    path('<str:pk>/round-robin/', views.generateRoundRobinMatches, name='round-robin'),
]
