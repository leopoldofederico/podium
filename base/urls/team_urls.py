from django.urls import path
from django.contrib.auth.models import User
from base.views import team_views as views


urlpatterns = [
    path('', views.listAvailableTeams, name='available-teams'),
    path('list/', views.listOwnTeams, name='own-teams'),
    path('create/', views.createTeamWithShield, name='create-team'),
    path('upload/', views.uploadShield, name='upload-shield'),

    path('<str:pk>/', views.getTeam, name='get-team'),
    path('<str:pk>/update/', views.updateTeam, name='udpate-team'),
    path('<str:pk>/delete/', views.deleteTeam, name='delete-team'),
    path('<str:pk>/undelete/', views.undeleteTeam, name='undelete-team'),
]