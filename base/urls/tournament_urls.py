from django.urls import path
from django.contrib.auth.models import User
from base.views import tournament_views as views


urlpatterns = [
    path('', views.listUserTournaments, name='user-tournaments'),
    path('list/', views.listTournaments, name='tournaments'),
    path('create/', views.createTournament, name='create-tournament'),

    path('<str:pk>/', views.getTournament, name='get-tournament'),
    path('<str:pk>/update/', views.updateTournament, name='udpate-tournament'),
    path('<str:pk>/logo/', views.updateTournamentLogo, name='udpate-tournament-logo'),
    path('<str:pk>/delete/', views.deleteTournament, name='delete-tournament'),
    path('<str:pk>/undelete/', views.undeleteTournament, name='undelete-tournament'),
    path('<str:pk>/available-teams/', views.listAvailableTeams, name='available-teams'),
    path('<str:pk>/matches/', views.listMatches, name='list-matches'),
    
]