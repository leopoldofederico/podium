
from base.models import Category
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import CategorySerializer
from rest_framework import status
from django.core.exceptions import ValidationError

@api_view(['GET'])
# @permission_classes([IsAdminUser])
def listCategories(request):
    #  return Response({'detail':'Error getting categories'}, 
    #         status=status.HTTP_400_BAD_REQUEST)

    try:
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting categories'}, 
            status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserCategories(request):
    user = request.user
    try:
        categories = Category.objects.filter(user=user).order_by('name')
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting categories'}, 
            status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getCategory(request, pk):
    try:
        category = Category.objects.get(_id=pk)
        serializer = CategorySerializer(category, many=False)
        return Response(serializer.data)

    except:
        return Response({'detail':'Error getting category'}, 
            status=status.HTTP_400_BAD_REQUEST)
        

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createCategory(request):
    user = request.user
    data = request.data
    
    try:
        category = Category(
            user=User.objects.get(id=user.id),
            name=data['name'],
        )

        category.full_clean()
        category.save()
    except ValidationError as e:
        return Response({'detail':e.message_dict}, 
            status=status.HTTP_400_BAD_REQUEST)

    serializer = CategorySerializer(category, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateCategory(request, pk):
    
    user = request.user
    data = request.data
    

    try:
        category = Category.objects.get(_id=pk)

        if user.is_staff or category.user==user:

            category.name = data['name']
            category.full_clean()
            category.save()
            serializer = CategorySerializer(category, many=False)
            return Response(serializer.data)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
    except ValidationError as e:
        return Response({'detail':e.message_dict}, 
            status=status.HTTP_400_BAD_REQUEST)
            
    except:
        return Response({'detail':'Error getting category'}, 
                status=status.HTTP_400_BAD_REQUEST)
        

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteCategory(request, pk):
    
    user = request.user
    
    try:
        category = Category.objects.get(_id=pk)

        if user.is_staff or category.user==user:

            category.delete()
            
            return Response({'detail':'Deleted category ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting category'}, 
                status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteCategory(request, pk):
    
    user = request.user
    
    try:
        
        category = Category.deleted_objects.get(_id=pk)
        
        if user.is_staff or category.user==user:

            category.restore()
            
            return Response({'detail':'Undeleted category ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting category'}, 
                status=status.HTTP_400_BAD_REQUEST)