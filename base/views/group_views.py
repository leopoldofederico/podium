from base.models import Group,  Match, Stage
from django.db.models import Count, Sum, Q, IntegerField, Value
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import GroupSerializer, GroupWithStandingSerializer
from rest_framework import status
from django.core.exceptions import ValidationError

@api_view(['GET'])
@permission_classes([IsAdminUser])
def listGroups(request):
	try:
		groups = Group.objects.all()
		serializer = GroupSerializer(groups, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting groups'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserGroups(request):
	user = request.user
	try:
		
		groups = Group.objects.filter(stage__tournament__category__user_id=user.id)
		
		serializer = GroupSerializer(groups, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting tournaments'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getGroup(request, pk):
	try:
		group = Group.objects.get(_id=pk)
		serializer = GroupSerializer(group, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting group details'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createGroup(request):
	# user = request.user
	data = request.data

	try:
		group = Group(
			name = data['name'],
			stage=Stage.objects.get(_id=data['stage_id'])
		)

		group.full_clean()
		group.save()

		group.full_clean()
		group.save()		
			
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = GroupSerializer(group, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def updateGroup(request, pk):
	
	user = request.user
	data = request.data

	try:

		group = Group.objects.get(_id=pk)
		print(group.stage.tournament.user)
		if user.is_staff or user == group.stage.tournament.user:
			group.name = data['name']
			group.stage=Stage.objects.get(_id=data['stage_id'])
			
			group.full_clean()
			group.save()
			
			serializer = GroupSerializer(group, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting group'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def addTeam(request, pk):
	user = request.user
	data = request.data

	try:

		group = Group.objects.get(_id=pk)

		if user.is_staff or user == group.stage.tournament.user:
			for team in data:
				group.competitors.add(team)
			
			group.full_clean()
			group.save()
			
			serializer = GroupSerializer(group, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting group added'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def removeTeam(request, pk):
	user = request.user
	data = request.data

	try:

		group = Group.objects.get(_id=pk)
		
		if user.is_staff or user == group.stage.tournament.user:
			for team in data:
				group.competitors.remove(team)
			
			group.full_clean()
			group.save()
			
			serializer = GroupSerializer(group, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except ValueError as e:
		return Response({'detail': 'Try to remove a team that was not a competitor in the group'},
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response({'detail':'Error getting group removed'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def generateRoundRobinMatches(request, pk):
	user = request.user
	data = request.data
	try:
		
		group = Group.objects.get(_id=pk)
		
		competitors = list(group.competitors.all())
		print(group)
		print(competitors)
		
		rounds = data['rounds']
		print(rounds)
		complete_rounds = rounds // 2 
		extra_round = rounds % 2
		if user.is_staff or user == group.stage.tournament.category.user:
			print(complete_rounds)
			i = 0
			for x in range(complete_rounds):
				
				for team1 in competitors:
					print("==== " + str(team1._id) + " ====")
					for team2 in competitors:
						if team1 != team2:
							print(f"{team1} ({team1._id}) vs. {team2} ({team2._id}) en stage {group.stage._id}")
							Match.objects.create(
								stage = group.stage,
								team1 = team1,
								team2 = team2,
								role1 = 'home',
								role2 = 'away',
								round = (x * 2 + 1) + i % 2
							)
							i = i + 1
			k = i
			i = 0

			if extra_round == 1:
				
				availableTeams = competitors.copy()
				print(availableTeams)
				for team1 in competitors:
					# print("==== " + str(availableTeams.count()) + " ====")
					print(team1)
					availableTeams.remove(team1)
					print(availableTeams)
					for team2 in availableTeams:
						print(f"{team1} ({team1._id}) vs. {team2} ({team2._id}) en stage {group.stage._id}")
						Match.objects.create(
							stage = group.stage,
							team1 = team1 if i % 2 == 0 else team2,
							team2 = team2 if i % 2 == 0 else team1,
							role1 = 'home',
							role2 = 'away',
							round = rounds,
						)
						i = i + 1
			
			response = f'{k + i} matches generated for group {pk}'
			return Response({'detail': response}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting group for round-robin'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteGroup(request, pk):
	
	user = request.user
	
	try:
		
		group = Group.objects.get(_id=pk)
		
		if user.is_staff or user == group.stage.tournament.user:

			group.delete()
			
			return Response({'detail':'Deleted group ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting group for delete'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteGroup(request, pk):
    
    user = request.user
    
    try:
        
        group = Group.deleted_objects.get(_id=pk)
        
        if user.is_staff or group.stage.tournament.category.user==user:
            group.restore()
            
            return Response({'detail':'Undeleted group ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting group for undelete'}, 
                status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def listStandings(request, pk):
	data = request.data
	try:
		group = Group.objects.get(_id=pk)
		serializer = GroupWithStandingSerializer(group, many=False)

		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting group'}, 
			status=status.HTTP_400_BAD_REQUEST)