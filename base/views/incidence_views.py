from base.models import Incidence, Stage, Squad
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import IncidenceSerializer
from rest_framework import status
from django.core.exceptions import ValidationError

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserIncidences(request):
	user = request.user
	try:
		incidences = Incidence.objects.filter(stage__tournament__category__user_id = user.id)
		serializer = IncidenceSerializer(incidences, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting incidences'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAdminUser])
def listIncidences(request):
	print("chuipameeeeeeeeee")
	try:
		incidences = Incidence.objects.all()
		serializer = IncidenceSerializer(incidences, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting incidences'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getIncidence(request, pk):
	try:
		incidence = Incidence.objects.get(_id=pk)
		serializer = IncidenceSerializer(incidence, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting incidence'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createIncidence(request):
	print("chupameeeeeee")
	user = request.user
	data = request.data
	
	try:
		incidence = Incidence(
			stage = Stage.objects.get(pk=data['stage_id']),
			squad = Squad.objects.get(pk=data['squad_id']),
			points = (data['points'] if 'points' in data else None),
			date = (data['date'] if 'date' in data else None),
			note = (data['note'] if 'note' in data else None),
		)
		
		incidence.full_clean()
		incidence.save()
		
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = IncidenceSerializer(incidence, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateIncidence(request, pk):
	
	user = request.user
	data = request.data

	try:
		incidence = Incidence.objects.get(_id=pk)
		# print(incidence)
		if user.is_staff or incidence.stage.tournament.user == user:
			incidence.stage = Stage.objects.get(pk=data['stage_id']) if 'stage_id' in data else incidence.stage
			incidence.squad = Squad.objects.get(pk=data['squad_id']) if 'squad_id' in data else incidence.squad
			incidence.points = data['points'] if 'points' in data else incidence.points
			incidence.date = data['date'] if 'date' in data else incidence.date
			incidence.note = data['note'] if 'note' in data else incidence.note
			# print(incidence)
			incidence.full_clean()
			incidence.save()
			
			serializer = IncidenceSerializer(incidence, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting incidence'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteIncidence(request, pk):
	
	user = request.user

	try:
		incidence = Incidence.objects.get(_id=pk)
		
		if user.is_staff or incidence.stage.tournament.user == user:
			incidence.delete()
			
			return Response({'detail':'Deleted incidence ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting incidence'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteIncidence(request, pk):
	
	user = request.user

	try:
		incidence = Incidence.deleted_objects.get(_id=pk)
		
		if user.is_staff or incidence.stage.tournament.user == user:
			incidence.restore()
			
			return Response({'detail':'Undeleted incidence ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting incidence'}, 
				status=status.HTTP_400_BAD_REQUEST)
