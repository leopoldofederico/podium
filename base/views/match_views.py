from base.models import Match, Stage, Team, Squad
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import MatchSerializer
from rest_framework import status
from django.core.exceptions import ValidationError
from django.core import serializers

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserMatches(request):
	user = request.user
	try:
		matches = Match.objects.filter(stage__tournament__category__user_id = user.id)
		serializer = MatchSerializer(matches, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting matches'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAdminUser])
def listMatches(request):
	try:
		matches = Match.objects.all()
		serializer = MatchSerializer(matches, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting matches'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getMatch(request, pk):
	try:
		match = Match.objects.get(_id=pk)
		serializer = MatchSerializer(match, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting match'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createMatch(request):
	user = request.user
	data = request.data

	try:
		match = Match(
			stage = Stage.objects.get(pk=data['stage_id']),
			role1 = (data['role1'] if 'role1' in data else 'home'),
			role2 = (data['role2'] if 'role2' in data else 'away'),
			team1 = Squad.objects.get(pk=data['team1_id']),
			team2 = Squad.objects.get(pk=data['team2_id']),
			team1_points = (data['team1_points'] if 'team1_points' in data else None),
			team2_points = (data['team2_points'] if 'team2_points' in data else None),
			team1_bonus = (data['team1_bonus'] if 'team1_bonus' in data else None),
			team2_bonus = (data['team2_bonus'] if 'team2_bonus' in data else None),
			team1_score = (data['team1_score'] if 'team1_score' in data else None),
			team2_score = (data['team2_score'] if 'team2_score' in data else None),
			team1_played = (data['team1_played'] if 'team1_played' in data else None),
			team2_played = (data['team2_played'] if 'team2_played' in data else None),
			date = (data['date'] if 'date' in data else None),
			note = (data['note'] if 'note' in data else None),
			fixture_note = (data['fixture_note'] if 'fixture_note' in data else None),
			round = (data['round'] if 'round' in data else None),
			series = (data['series'] if 'series' in data else None),
		)
		print(match)
		match.full_clean()
		match.save()
		
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = MatchSerializer(match, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateMatch(request, pk):
	
	user = request.user
	data = request.data

	try:
		match = Match.objects.get(_id=pk)
		team1_score = None if data['team1_score']=='' else data['team1_score']
		team2_score = None if data['team2_score']=='' else data['team2_score']
			
		if user.is_staff or match.stage.tournament.user == user:
			match.stage = Stage.objects.get(pk=data['stage_id']) if 'stage_id' in data else match.stage
			match.role1 = data['role1'] if 'role1' in data else match.role1
			match.role2 = data['role2'] if 'role2' in data else match.role2
			match.team1 = Squad.objects.get(pk=data['team1_id']) if 'team1_id' in data else match.team1
			match.team2 = Squad.objects.get(pk=data['team2_id']) if 'team2_id' in data else match.team2
			match.team1_points = data['team1_points'] if 'team1_points' in data else match.team1_points
			match.team2_points = data['team2_points'] if 'team2_points' in data else match.team2_points
			match.team1_bonus = data['team1_bonus'] if 'team1_bonus' in data else match.team1_bonus
			match.team2_bonus = data['team2_bonus'] if 'team2_bonus' in data else match.team2_bonus
			match.team1_score = team1_score
			match.team2_score = team2_score
			match.team1_played = data['team1_played'] if 'team1_played' in data else match.team1_played
			match.team2_played = data['team2_played'] if 'team2_played' in data else match.team2_played
			match.date = data['date'] if 'date' in data else match.date
			match.note = data['note'] if 'note' in data else match.note
			match.fixture_note = data['fixture_note'] if 'fixture_note' in data else match.fixture_note
			match.round = data['round'] if 'round' in data else match.round
			match.series = data['series'] if 'series' in data else match.series
			
			match.full_clean()
			
			match.save()

			serializer = MatchSerializer(match, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting match'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteMatch(request, pk):
	
	user = request.user

	try:
		match = Match.objects.get(_id=pk)
		
		if user.is_staff or match.stage.tournament.user == user:
			match.delete()
			
			return Response({'detail':'Deleted match ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting match'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteMatch(request, pk):

	user = request.user

	try:
		match = Match.deleted_objects.get(_id=pk)
		
		if user.is_staff or match.stage.tournament.user == user:
			match.restore()
			
			return Response({'detail':'Undeleted match ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting match'}, 
				status=status.HTTP_400_BAD_REQUEST)
