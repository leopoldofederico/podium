from base.models import Person
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import PersonBaseSerializer
from rest_framework import status
from django.core.exceptions import ValidationError

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listPeople(request):
	try:
		people = Person.objects.all()
		serializer = PersonBaseSerializer(people, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting people'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getPerson(request, pk):
	try:
		person = Person.objects.get(_id=pk)
		serializer = PersonSerializer(person, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting person'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createPerson(request):
	user = request.user
	data = request.data

	try:
		person = Person(
			last_name = data['last_name'],
			first_name = data['first_name'],
			external_id = data['external_id'],
			mail = data['mail'],
			photo = data['photo'],
			born_at = data['born_at'],
			referee = data['referee'],
			official = data['official'],
		)

		person.full_clean()
		person.save()
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = PersonSerializer(person, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updatePerson(request, pk):
	
	# user = request.user
	data = request.data

	try:
		person = Person.objects.get(_id=pk)

		person.last_name = data['last_name']
		person.first_name = data['first_name']
		person.external_id = data['external_id']
		person.mail = data['mail']
		person.photo = data['photo']
		person.born_at = data['born_at']
		person.referee = data['referee']
		person.official = data['official']
		
		person.full_clean()
		person.save()
		
		serializer = PersonSerializer(person, many=False)
		return Response(serializer.data)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting person'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['DELETE'])
@permission_classes([IsAdminUser])
def deletePerson(request, pk):
	
	user = request.user
	
	try:
		person = Person.objects.get(_id=pk)
		
		person.delete()
			
		return Response({'detail':'Deleted person ' + pk}, status=status.HTTP_200_OK)

	except:
		return Response({'detail':'Error getting person'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def uploadPhoto(request):

    data = request.data
    person_id = data['person_id']
    person = Person.objects.get(_id=person_id)

    person.photo = request.FILES.get('image')
    person.save()

    return Response('Image uploaded')