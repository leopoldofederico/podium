from base.models import Place
from rest_framework.decorators import api_view
from rest_framework.response import Response
from base.serializer import PlaceSerializer
from rest_framework import status

@api_view(['GET'])
def listPlaces(request):
    try:
        # print("chupame")
        places = Place.objects.all().order_by('name')
        print(places)
        serializer = PlaceSerializer(places, many=True)
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting places'}, 
            status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getPlace(request, pk):
	try:
		place = Place.objects.get(_id=pk)
		serializer = PlaceSerializer(place, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting place'}, 
			status=status.HTTP_400_BAD_REQUEST)
