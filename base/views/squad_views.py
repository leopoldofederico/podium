from base.models import Group, Squad, Tournament, Team, Person
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import MatchMiniSerializer, MatchSerializer, SquadSerializer
from rest_framework import status
from django.core.exceptions import ValidationError
from django.db.models import Q

@api_view(['GET'])
# @permission_classes([IsAdminUser])
def listSquads(request):
	try:
		squads = Squad.objects.all()
		serializer = SquadSerializer(squads, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting squads'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserSquads(request):
	user = request.user
	try:
		squads = Squad.objects.filter(tournament__category__user_id=user.id)
		serializer = SquadSerializer(squads, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting squads'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getSquad(request, pk):
	try:
		
		squad = Squad.objects.get(_id=pk)
		serializer = SquadSerializer(squad, many=False)
		
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting squad'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createSquad(request):
	user = request.user
	data = request.data

	try:
		squad = Squad(
			name = data['name'],
			team = Team.objects.get(pk=data['team_id']),
			tournament = Tournament.objects.get(pk=data['tournament_id']),
		)
		print(squad)
		squad.full_clean()
		squad.save()
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	# serializer = SquadSerializer(squad, many=False)
	# return Response(serializer.data)
	return Response({'detail':'Created squad'}, status=status.HTTP_200_OK)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateSquad(request, pk):
	
	user = request.user
	data = request.data

	try:
		squad = Squad.objects.get(_id=pk)

		if user.is_staff or squad.tournament.user == user:
			squad.name = data['name']
			squad.team = Team.objects.get(pk=data['team_id'])
			squad.tournament = Tournament.objects.get(pk=data['tournament_id'])
			
			squad.full_clean()
			squad.save()
			
			serializer = SquadSerializer(squad, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting squad'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteSquad(request, pk):
	
	user = request.user
	
	try:
		squad = Squad.objects.get(_id=pk)
		
		if user.is_staff or squad.tournament.user == user:			
			squad.delete()
				
			return Response({'detail':'Deleted squad ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response({'detail':'Error getting squad'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteSquad(request, pk):
	
	user = request.user
	
	try:
		squad = Squad.deleted_objects.get(_id=pk)
		
		if user.is_staff or squad.tournament.user == user:			
			squad.restore()
				
			return Response({'detail':'Undeleted squad ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response({'detail':'Error getting squad'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def addPlayer(request, pk):

	user = request.user

	try:
		
		squad = Squad.objects.get(_id=pk)
		
		player = Person.objects.get(_id=request.data['player_id'])
		if user.is_staff or squad.tournament.user == user:
			filteredSquads = Squad.objects.filter(tournament=squad.tournament, players__pk=player._id)
			# print('filtrado es ' + str(filteredSquads.count()))
			if filteredSquads.count() > 0:
				raise ValidationError(_('Player is already on another squad in the same tournament.'))

			squad.players.add(player)
					
			squad.full_clean()
			squad.save()
			
			serializer = SquadSerializer(squad, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting squad'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def removePlayer(request, pk):

	user = request.user

	try:
		squad = Squad.objects.get(_id=pk)
		player = Person.objects.get(_id=request.data['player_id'])
		if user.is_staff or squad.tournament.user == user:
			squad.players.remove(player._id)
			
			squad.full_clean()
			squad.save()
			
			serializer = SquadSerializer(squad, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except ValueError as e:
		return Response({'detail': 'Try to remove a player that was not a member of the squad'},
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response({'detail':'Error getting squad'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def listMatches(request, pk, stage):
	try:
		squad = Squad.objects.get(_id=pk)
		stage = Group.objects.get(_id=stage).stage
		matches = stage.match_set.filter(
				Q(team1_id=squad._id) | Q(team2_id=squad._id)
			).order_by('date', 'round', 'series')

		serializer = MatchMiniSerializer(matches, many=True)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting squad'}, 
			status=status.HTTP_400_BAD_REQUEST)