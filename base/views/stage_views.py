from base.models import Stage, Tournament, Match, Team, Group
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import StageSerializer
from rest_framework import status
from django.core.exceptions import ValidationError
from django.db.models import Q, Count, Sum

@api_view(['GET'])
@permission_classes([IsAdminUser])
def listStages(request):
	try:
		stages = Stage.objects.all()
		serializer = StageSerializer(stages, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting stages'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserStages(request):
	user = request.user
	try:
		
		stages = Stage.objects.filter(tournament__category__user_id=user.id)
		
		serializer = StageSerializer(stages, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting tournaments'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getStage(request, pk):
	try:
		stage = Stage.objects.get(_id=pk)
		serializer = StageSerializer(stage, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting stage'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createStage(request):
	# user = request.user
	data = request.data

	try:
		stage = Stage(
			name = data['name'],
			tournament=Tournament.objects.get(_id=data['tournament_id']),
			win_points = data['win_points'],
            draw_points = data['draw_points'],
            lost_points = data['lost_points'],
            abandon_points = data['abandon_points'],
            goals_win_abandon = data['goals_win_abandon'],
            goals_lost_abandon = data['goals_lost_abandon'],
		)

		stage.full_clean()
		stage.save()

		group = Group(
			name = data['name'],
			stage = stage
		)

		group.full_clean()
		group.save()		
			
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = StageSerializer(stage, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def updateStage(request, pk):
	
	user = request.user
	data = request.data

	try:

		stage = Stage.objects.get(_id=pk)
		
		if user.is_staff or user == stage.tournament.category.user:
			previousName = stage.name
			if 'name' in data: stage.name = data['name']
			if 'tournament_id' in data: stage.tournament=Tournament.objects.get(_id=data['tournament_id']) 
			if 'win_points' in data: stage.win_points = data['win_points']
			if 'draw_points' in data: stage.draw_points = data['draw_points']
			if 'lost_points' in data: stage.lost_points = data['lost_points']
			if 'abandon_points' in data: stage.abandon_points = data['abandon_points']
			if 'goals_win_abandon' in data: stage.goals_win_abandon = data['goals_win_abandon']
			if 'goals_lost_abandon' in data: stage.goals_lost_abandon = data['goals_lost_abandon']
			print(stage)
			stage.full_clean()
			stage.save()
			
			# if there's only one group, and name of that group is the same as stage
			# also change group name
			if stage.group_set.count() == 1:
				group = stage.group_set.first()
				if group.name == previousName:
					group.name = data['name']
					group.full_clean()
					group.save()

			serializer = StageSerializer(stage, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting stage'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def addTeam(request, pk):
	user = request.user
	data = request.data

	try:

		stage = Stage.objects.get(_id=pk)
		
		if user.is_staff or user == stage.tournament.user:
			stage.competitors.add(data['team_id'])
			
			stage.full_clean()
			stage.save()
			
			serializer = StageSerializer(stage, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting stage'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def removeTeam(request, pk):
	user = request.user
	data = request.data

	try:

		stage = Stage.objects.get(_id=pk)
		
		if user.is_staff or user == stage.tournament.user:
			stage.competitors.remove(data['team_id'])
			
			stage.full_clean()
			stage.save()
			
			serializer = StageSerializer(stage, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except ValueError as e:
		return Response({'detail': 'Try to remove a team that was not a competitor in the stage'},
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response({'detail':'Error getting stage'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteStage(request, pk):
	
	user = request.user
	
	try:
		
		stage = Stage.objects.get(_id=pk)
		
		if user.is_staff or user == stage.tournament.user:
			for group in stage.group_set.all():
				group.delete()

			stage.delete()
			
			return Response({'detail':'Deleted stage ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting stage'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteStage(request, pk):
    
    user = request.user
    
    try:
        
        stage = Stage.deleted_objects.get(_id=pk)
        
        if user.is_staff or stage.tournament.category.user==user:
            for group in Group.deleted_objects.filter(stage=stage):
            	group.restore()

            stage.restore()
            
            return Response({'detail':'Undeleted stage ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting stage'}, 
                status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def generateRoundRobinMatches(request, pk):
	user = request.user
	data = request.data
	try:
		stage = Stage.objects.get(_id=pk)

		competitors = []
		for group in stage.group_set.all():
			competitors = competitors + list(group.competitors.all())

		rounds = data['rounds']

		complete_rounds = rounds // 2 
		extra_round = rounds % 2
		if user.is_staff or user == stage.tournament.category.user:
			print(complete_rounds)
			i = 0
			for x in range(complete_rounds):
				
				for team1 in competitors:
					print("==== " + str(team1._id) + " ====")
					for team2 in competitors:
						if team1 != team2:
							print(f"{team1} ({team1._id}) vs. {team2} ({team2._id}) en stage {stage._id}")
							Match.objects.create(
								stage = stage,
								team1 = team1,
								team2 = team2,
								role1 = 'home',
								role2 = 'away',
								round = (x * 2 + 1) + i % 2
							)
							i = i + 1
			# k = total de partidos generados en este paso (idas y vueltas)
			k = i
			i = 0

			if extra_round == 1:
				
				availableTeams = competitors.copy()
				
				for team1 in competitors:
					# print("==== " + str(availableTeams.count()) + " ====")
					
					availableTeams.remove(team1)
					
					for team2 in availableTeams:
						print(f"{team1} ({team1._id}) vs. {team2} ({team2._id}) en stage {stage._id}")
						Match.objects.create(
							stage = stage,
							team1 = team1 if i % 2 == 0 else team2,
							team2 = team2 if i % 2 == 0 else team1,
							role1 = 'home',
							role2 = 'away',
							round = rounds,
						)
						i = i + 1
			
			#tanto k como i arrancan en 0, por eso que se termine sumando 1 hace que efectivamente
			#tengan la cantidad de partidos generada en cada paso
			response = f'{k + i} matches generated in stage {pk}'
			return Response({'detail': response}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
				status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting stage'}, 
				status=status.HTTP_400_BAD_REQUEST)
