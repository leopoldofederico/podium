from base.models import Team, Place
from django.db.models import Q
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import TeamSerializer
from rest_framework import status
from django.core.exceptions import ValidationError
from datetime import datetime

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listAvailableTeams(request):
	
	user = request.user
	print(user)
	try:
		teams = Team.objects.filter(Q(user=user) | Q(user=None)).order_by('official_name')
		# teams = Team.objects.all()
		print(len(teams))
		serializer = TeamSerializer(teams, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting teams'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listOwnTeams(request):
	user = request.user
	try:
		teams = Team.objects.filter(user=user)
		serializer = TeamSerializer(teams, many=True)
		return Response(serializer.data)
	except:
		return Response({'detail':'Error getting teams'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getTeam(request, pk):
	try:
		team = Team.objects.get(_id=pk)
		serializer = TeamSerializer(team, many=False)
		return Response(serializer.data)

	except:
		return Response({'detail':'Error getting team'}, 
			status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createTeam(request):
	user = request.user
	data = request.data

	try:
		team = Team(
			name = data['name'],
			official_name = data['official_name'],
			place = Place.objects.get(_id=data['place_id']),
			user = User.objects.get(id=user.id),
		)
		if 'shield' in data and data['shield'] != '':
			team.shield = data['shield']
		
		team.full_clean()
		team.save()
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = TeamSerializer(team, many=False)
	return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createTeamWithShield(request):
	user = request.user
	data = request.data

	try:
		team = Team(
			name = data['name'],
			official_name = data['official_name'],
			place = Place.objects.get(_id=data['place_id']),
			user = User.objects.get(id=user.id),
		)
		if 'shield' in data and data['shield']!="":
			team.shield = request.FILES.get('shield')
			now = datetime.now().strftime("%Y%m%d%H%M%S")
			team.shield.name = f"team-{now}-{team.shield.name}"
		team.full_clean()
		team.save()
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)
	except:
		return Response('error', 
			status=status.HTTP_400_BAD_REQUEST)

	serializer = TeamSerializer(team, many=False)
	return Response(serializer.data)

@api_view(['PUT'])
# @permission_classes([IsAdminUser])
def updateTeam(request, pk):
	
	user = request.user
	data = request.data

	try:
		team = Team.objects.get(_id=pk)

		if user.is_staff or team.user==user:
			team.name = data['name']
			team.official_name = data['official_name']
			if request.FILES.get('shield'):
				team.shield = data['shield']
			team.place = Place.objects.get(_id=data['place_id'])
			
			team.full_clean()
			team.save()
			
			serializer = TeamSerializer(team, many=False)
			return Response(serializer.data)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
		
	except ValidationError as e:
		return Response({'detail':e.message_dict}, 
			status=status.HTTP_400_BAD_REQUEST)

	except:
		return Response({'detail':'Error getting team'}, 
				status=status.HTTP_400_BAD_REQUEST)
		

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteTeam(request, pk):
	
	user = request.user
	
	try:
		team = Team.objects.get(_id=pk)
		
		if user.is_staff or team.user==user:
			team.delete()
			
			return Response({'detail':'Deleted team ' + pk}, status=status.HTTP_200_OK)
		else:
			return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
			

	except:
		return Response({'detail':'Error getting team'}, 
				status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def uploadShield(request):

	data = request.data
	team_id = data['team_id']
	team = Team.objects.get(pk=team_id)
	team.shield = request.FILES.get('image')
	team.save()

	return Response('Se subió la imagen')

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteTeam(request, pk):
    
    user = request.user
    
    try:
        
        team = Team.deleted_objects.get(_id=pk)
        
        if user.is_staff or team.user==user:

            team.restore()
            
            return Response({'detail':'Undeleted team ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting team'}, 
                status=status.HTTP_400_BAD_REQUEST)