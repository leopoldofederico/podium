from datetime import datetime
from base.models import Tournament, Category, Team, Match
from django.db.models import Q
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth.models import User
from base.serializer import TournamentSerializer, TournamentsSerializer, TeamSerializer, MatchSerializer
from rest_framework import status
from django.core.exceptions import ValidationError

@api_view(['GET'])
# @permission_classes([IsAdminUser])
def listTournaments(request):
    try:
        tournaments = Tournament.objects.all()
        serializer = TournamentsSerializer(tournaments, many=True)
        
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting tournaments'}, 
            status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listUserTournaments(request):
    user = request.user
    try:
        
        tournaments = Tournament.objects.filter(category__user_id=user.id)
        serializer = TournamentsSerializer(tournaments, many=True)
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting tournaments'}, 
            status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getTournament(request, pk):
    try:
        tournament = Tournament.objects.get(_id=pk)
        print(tournament)
        serializer = TournamentSerializer(tournament, many=False)
        return Response(serializer.data)

    except:
        return Response({'detail':'Error getting tournament'}, 
            status=status.HTTP_400_BAD_REQUEST)
        
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listAvailableTeams(request, pk):
    user = request.user
    try:
        tournament = Tournament.objects.get(pk=pk)
        registered = tournament.squad_set.values_list('team_id')
        teams = Team.objects.filter(Q(user=user) | Q(user=None)).exclude(_id__in=registered)
        serializer = TeamSerializer(teams, many=True)
        return Response(serializer.data)
    except:
        return Response({'detail':'Error getting teams'}, 
			status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createTournament(request):
    user = request.user
    data = request.data['tournament']
    # print(data["tournament"])
    try:
        tournament = Tournament(
            isCurrent=data['isCurrent'],
            season=data['season'],
            category=Category.objects.get(_id=data['category_id']),
            name=data['name'],
            win_points = data['win_points'],
            draw_points = data['draw_points'],
            lost_points = data['lost_points'],
            abandon_points = data['abandon_points'],
            goals_win_abandon = data['goals_win_abandon'],
            goals_lost_abandon = data['goals_lost_abandon'],
            draw = data['draw'],
            bonus = data['bonus'],
        )
        print(tournament)

        tournament.full_clean()
        tournament.save()
    except ValidationError as e:
        return Response({'detail':e.message_dict}, 
            status=status.HTTP_400_BAD_REQUEST)
    except:
        return Response('error', 
            status=status.HTTP_400_BAD_REQUEST)

    serializer = TournamentSerializer(tournament, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateTournament(request, pk):
    
    user = request.user
    data = request.data

    try:
        tournament = Tournament.objects.get(_id=pk)
        
        if user.is_staff or tournament.user==user:

            tournament.name = data['name']
            tournament.isCurrent= data['isCurrent']
            tournament.season=data['season']
            tournament.category=Category.objects.get(_id=data['category_id'])
            tournament.win_points = data['win_points']
            tournament.draw_points = data['draw_points']
            tournament.lost_points = data['lost_points']
            tournament.abandon_points = data['abandon_points']
            tournament.goals_win_abandon = data['goals_win_abandon']
            tournament.goals_lost_abandon = data['goals_lost_abandon']
            tournament.draw = data['draw']
            tournament.bonus = data['bonus']

            
            tournament.full_clean()
            
            tournament.save()
            
            serializer = TournamentSerializer(tournament, many=False)
            return Response(serializer.data)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
    except ValidationError as e:
        return Response({'detail':e.message_dict}, 
            status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting tournament'}, 
                status=status.HTTP_400_BAD_REQUEST)
        
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateTournamentLogo(request, pk):
    
    user = request.user
    
    try:
        tournament = Tournament.objects.get(_id=pk)
        
        if user.is_staff or tournament.user==user:
            tournament.logo = request.FILES.get('logo')
           
            now = datetime.now().strftime("%Y%m%d%H%M%S")

            tournament.logo.name = f"tournament-{now}-{tournament.logo.name}"
            
            
            tournament.full_clean()
            
            tournament.save()
            
            serializer = TournamentSerializer(tournament, many=False)
            return Response(serializer.data)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)
    except ValidationError as e:
        return Response({'detail':e.message_dict}, 
            status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting tournament'}, 
                status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteTournament(request, pk):
    
    user = request.user
    
    try:
        tournament = Tournament.objects.get(_id=pk)

        if user.is_staff or tournament.user==user:

            tournament.delete()
            
            return Response({'detail':'Deleted tournament ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting tournament'}, 
                status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def undeleteTournament(request, pk):
    
    user = request.user
    
    try:
        
        tournament = Tournament.deleted_objects.get(_id=pk)
        
        if user.is_staff or tournament.user==user:

            tournament.restore()
            
            return Response({'detail':'Undeleted tournament ' + pk}, status=status.HTTP_200_OK)
        else:
            return Response({'detail':'Without authorization for this request'}, 
                status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'detail':'Error getting tournament'}, 
                status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def listMatches(request, pk):
    user = request.user
    try:
        tournament = Tournament.objects.get(_id=pk)
        
        if user.is_staff or tournament.user==user:
        
            matches = Match.objects.filter(stage__tournament_id=pk)
            matches = MatchSerializer.setup_eager_loading(matches)

            serializer = MatchSerializer(matches.order_by("date"), many=True)
            return Response(serializer.data)
    except:
        return Response({'detail':'Error getting tournaments'}, 
            status=status.HTTP_400_BAD_REQUEST)
