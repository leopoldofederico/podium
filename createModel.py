import sys
import os.path
from datetime import datetime
import shutil

target = sys.argv[1]
if len(sys.argv) > 2:
	plural = sys.argv[2]
else:
	plural = target + "s"
startLine = 0
fields = []
with open('base\models.py', 'r') as reader:
	lines = reader.readlines()
	for line in lines:
		# print(line)
		if startLine == 0:
			if "class " + target in line:
				startLine = lines.index(line)
		else:
			if "createdAt" in line:
				break
			else:
				field = line.split()[0]
				typeField = line.split()[2].split('(')[0].split('.')[1]
				fields.append({'name': field, 'type': typeField})
for field in fields:
	print(field)

# PREGUNTAR SI EL ARCHIVO EXISTE, SI ES ASÍ
# CREAR UNA COPIA CON LA FECHA/HORA DE COPIA

viewFileName = os.path.join('base', 'views', target.lower() + "_views.py")

if os.path.exists(viewFileName):
	os.rename(viewFileName,viewFileName + datetime.now().strftime('%Y%m%d%H%M%S'))

shutil.copy('../template_views.py', os.path.join('base', 'views'))
os.rename(os.path.join('base', 'views', "template_views.py"), viewFileName)

with open(viewFileName, 'r') as reader:
	lines = reader.readlines()

with open(viewFileName,'w') as writer:
	for line in lines:
		if "*fields" in line:
			spaces = line[:-9]
			model = ''
			comma = ','
			if "withmodel" in line:
				spaces = line[:-18]
				model = target.lower() + "."
				comma = ''
			for field in fields:
				if field['type'] == 'ForeignKey':
					writer.write(spaces + f"{model}{field['name']} = {field['name'].capitalize()}.objects.get(pk=data['{field['name']}_id']){comma}\n")
				elif field['name'] != '_id':
					writer.write(spaces + model + field['name'] + " = data['" + field['name'] + f"']{comma}\n")
		else:
			if "*Model*" in line:
				line = line.replace('*Model*', target)
			if "*model*" in line:
				line = line.replace('*model*', target.lower())
			if "*Plural*" in line:
				line = line.replace("*Plural*", plural)
			if "*plural" in line:
				line = line.replace("*plural*", plural.lower())
			
			writer.write(line)

with open('base\serializer.py', 'a') as writer:
    writer.write(f'\nclass {target}Serializer(serializers.ModelSerializer):\n')
    listOfForeigns = ""
    for field in fields:
        if field['type'] == "ForeignKey":
            listOfForeigns += ' ' * 4 + field['name'] + " = serializers.SerializerMethodField(read_only=True)\n"
    writer.write(listOfForeigns + "\n")

    writer.write(' ' * 4 + 'class Meta:\n')
    writer.write(' ' * 8 + 'model = ' + target + '\n')
    listOfFields = ""
    for field in fields:
        listOfFields += "'" + field['name'] + "', "
    listOfFields = listOfFields[:-2]
    writer.write(' ' * 8 + f'fields = [{listOfFields}]\n' )

    for field in fields:
        if field['type'] == "ForeignKey":
            foreign = field['name']
            writer.write(' ' * 4 + f"\ndef get_{foreign}(self, obj):\n")
            writer.write(' ' * 8 + f"{foreign} = obj.{foreign} \n")
            writer.write(' ' * 8 + f"serializer = {foreign.capitalize()}Serializer({foreign}, many=False)\n")
            writer.write(' ' * 8 + "return serializer.data\n")


urlFileName = os.path.join('base', 'urls', target.lower() + "_urls.py")

if os.path.exists(urlFileName):
    os.rename(urlFileName,urlFileName + datetime.now().strftime('%Y%m%d%H%M%S'))

shutil.copy('../template_urls.py', os.path.join('base', 'urls'))
os.rename(os.path.join('base', 'urls', "template_urls.py"), urlFileName)
	
with open(urlFileName, 'r') as reader:
    lines = reader.readlines()

with open(urlFileName,'w') as writer:
    for line in lines:
        if "*Model*" in line:
            line = line.replace('*Model*', target)
        if "*model*" in line:
            line = line.replace('*model*', target.lower())
        if "*Plural*" in line:
            line = line.replace("*Plural*", plural)
        if "*plural" in line:
            line = line.replace("*plural*", plural.lower())
		
        writer.write(line)

with open(r"backend\urls.py", 'r') as reader:
    lines = reader.readlines()

with open(r"backend\urls.py", 'w') as writer:
	
    for line in lines:

        if line[0] == "]":
            lines.insert(lines.index(line), 
                f"    path('api/{plural.lower()}/', include('base.urls.{target.lower()}_urls')),\n")
            break
    writer.writelines(lines)

testFileName = os.path.join('base', 'tests', f"test{target}.py")

if os.path.exists(testFileName):
    os.rename(testFileName,testFileName + datetime.now().strftime('%Y%m%d%H%M%S'))

shutil.copy('../testModel.py', os.path.join('base', 'tests'))
os.rename(os.path.join('base', 'tests', "testModel.py"), testFileName)

with open(testFileName, 'r') as reader:
    lines = reader.readlines()

with open(testFileName,'w') as writer:
    for line in lines:
        if "*fields" in line:
            spaces = line[:-9]
            for field in fields:
                assignment = spaces
                if "object" in line:
                    spaces = line[:-15]
                    assignment += f"'{field['name']}': " 
                else:
                    assignment += field['name'] + " = "

                if field['name'] == 'name':
                    writer.write(assignment + f"test{target}Name,\n")
                elif field['name'] != '_id':
                    writer.write(assignment + "self.faker.name(),\n")
        else:
            if "*Model*" in line:
                line = line.replace('*Model*', target)
            if "*model*" in line:
                line = line.replace('*model*', target.lower())
            if "*Plural*" in line:
                line = line.replace("*Plural*", plural)
            if "*plural" in line:
                line = line.replace("*plural*", plural.lower())
		
            writer.write(line)