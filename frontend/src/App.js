import { Container } from 'react-bootstrap';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import TournamentsScreen from './screens/TournamentsScreen';
import TournamentEditScreen from './screens/TournamentEditScreen';
import TournamentCreateScreen from './screens/TournamentCreateScreen';
import CategoriesScreen from './screens/CategoriesScreen';
import LoginScreen from './screens/LoginScreen';
import ProfileScreen from './screens/ProfileScreen';
import RegisterScreen from './screens/RegisterScreen';
import TeamsScreen from './screens/TeamsScreen';
import TeamCreateScreen from './screens/TeamCreateScreen';
import TeamEditScreen from './screens/TeamEditScreen';
import Header from './components/Header';
import Footer from './components/Footer';
import MatchCreateScreen from './screens/MatchCreateScreen';
import MatchEditScreen from './screens/MatchEditScreen';
import StandingsScreen from './screens/StandingsScreen';
import Log from './components/RequireLogin';
import SquadFixtureScreen from './screens/SquadFixtureScreen';
import SquadScreen from './screens/SquadScreen';

function App() {
  

    return (
        <DndProvider backend={HTML5Backend}>
            <Header />
            <main>
                <Container className="py 3">
                    <Routes>
                        <Route path="/" element={<Log><TournamentsScreen /></Log>} exact />
                        <Route path="/tournaments" element={<Log><TournamentsScreen /></Log>} />
                        <Route path="/tournament/create" element={<Log><TournamentCreateScreen /></Log>} exact/>
                        <Route path="/tournament/:id" element={<TournamentEditScreen />} />
                        <Route path="/categories" element={<CategoriesScreen />} />
                        <Route path="/login" element={<LoginScreen />} />
                        <Route path="/home" element={<Log><TournamentsScreen /></Log>} />
                        <Route path="/profile" element={<ProfileScreen />} />
                        <Route path="/register" element={<RegisterScreen />} />
                        <Route path="/teams" element={<TeamsScreen />} />
                        <Route path="/team/create" element={<TeamCreateScreen />} exact/>
                        <Route path="/team/:id" element={<TeamEditScreen />} exact/>
                        <Route path="/match/create" element={<MatchCreateScreen />} exact />
                        <Route path="/match/:id/edit" element={<MatchEditScreen />} exact />
                        <Route path="/squad/:id" element={<SquadScreen />} exact />
                        <Route path="/squad/:id/matches/:stage" element={<SquadFixtureScreen />} exact />
                        <Route path="/standings/:id" element={<StandingsScreen />} exact />
                    </Routes>
                </Container>
            </main>
            <Footer />
        </DndProvider>
    );
    }

export default App;
