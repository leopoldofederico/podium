import axios from 'axios';
import { 
    GROUP_LIST_REQUEST, 
    GROUP_LIST_SUCCESS, 
    GROUP_LIST_FAIL,
    GROUP_DETAILS_REQUEST,
    GROUP_DETAILS_SUCCESS, 
    GROUP_DETAILS_FAIL,
    GROUP_DELETE_REQUEST,
    GROUP_DELETE_SUCCESS, 
    GROUP_DELETE_FAIL,
    GROUP_UNDELETE_REQUEST,
    GROUP_UNDELETE_SUCCESS, 
    GROUP_UNDELETE_FAIL,
    GROUP_CREATE_REQUEST,
    GROUP_CREATE_SUCCESS, 
    GROUP_CREATE_FAIL,
    GROUP_UPDATE_REQUEST,
    GROUP_UPDATE_SUCCESS, 
    GROUP_UPDATE_FAIL,
} from '../constants/groupConstants';

export const listGroups = () => async (dispatch, getState) => {
    try {
        dispatch({type: GROUP_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            'api/groups/',
            config
        );

        dispatch({
            type: GROUP_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: GROUP_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listGroupsDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: GROUP_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/groups/${id}`)

        dispatch({
            type: GROUP_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: GROUP_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createGroup = (group) => async (dispatch, getState) => {
    try {
        console.log(group);
        dispatch({
            type: GROUP_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/groups/create/`,
            {...group},
            config
       )

       dispatch({
            type: GROUP_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: GROUP_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const updateGroup = (group) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GROUP_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/groups/${group._id}/update/`,
            group,
            config
       )

       dispatch({
            type: GROUP_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:GROUP_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: GROUP_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: group._id,
       })
    }
}

export const assignTeams = (teams, id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GROUP_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        console.log(teams)
        const {data} = await axios.put(
            `/api/groups/${id}/add-team/`,
            teams.map(x => x._id),
            config
       )

       dispatch({
            type: GROUP_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:GROUP_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: GROUP_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: id,
       })
    }
}

export const removeTeams = (teams, id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GROUP_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/groups/${id}/remove-team/`,
            teams.map(x => x._id),
            config
       )

       dispatch({
            type: GROUP_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:GROUP_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: GROUP_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: id,
       })
    }
}

export const deleteGroup = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GROUP_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/groups/${id}/delete`,
            config
       )

       dispatch({
            type: GROUP_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: GROUP_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteGroup = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GROUP_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/groups/${id}/undelete`,
            config
       )

       dispatch({
            type: GROUP_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: GROUP_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}