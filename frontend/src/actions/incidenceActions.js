import axios from 'axios';
import { 
    INCIDENCE_LIST_REQUEST, 
    INCIDENCE_LIST_SUCCESS, 
    INCIDENCE_LIST_FAIL,
    INCIDENCE_DETAILS_REQUEST,
    INCIDENCE_DETAILS_SUCCESS, 
    INCIDENCE_DETAILS_FAIL,
    INCIDENCE_DELETE_REQUEST,
    INCIDENCE_DELETE_SUCCESS, 
    INCIDENCE_DELETE_FAIL,
    INCIDENCE_UNDELETE_REQUEST,
    INCIDENCE_UNDELETE_SUCCESS, 
    INCIDENCE_UNDELETE_FAIL,
    INCIDENCE_CREATE_REQUEST,
    INCIDENCE_CREATE_SUCCESS, 
    INCIDENCE_CREATE_FAIL,
    INCIDENCE_UPDATE_REQUEST,
    INCIDENCE_UPDATE_SUCCESS, 
    INCIDENCE_UPDATE_FAIL,
} from '../constants/incidenceConstants';

export const listIncidences = (tournament) => async (dispatch, getState) => {
    try {
        dispatch({type: INCIDENCE_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            `api/tournaments/${tournament}/incidences/`,
            config
        );

        dispatch({
            type: INCIDENCE_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: INCIDENCE_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listIncidencesDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: INCIDENCE_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/incidences/${id}`)

        dispatch({
            type: INCIDENCE_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: INCIDENCE_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? Object.entries(error.response.data.detail)
                : Object.entries(error.message),
        })
    }
}

export const createIncidence = (incidence) => async (dispatch, getState) => {
    try {
        
        dispatch({
            type: INCIDENCE_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/incidences/create/`,
            {...incidence},
            config
       )

       dispatch({
            type: INCIDENCE_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: INCIDENCE_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : Object.entries(error.message),
       })
    }
}

export const updateIncidence = (incidence) => async (dispatch, getState) => {
    try {
        console.log(incidence);
        dispatch({
            type: INCIDENCE_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/incidences/${incidence._id}/update/`,
            incidence,
            config
       )

       dispatch({
            type: INCIDENCE_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:INCIDENCE_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: INCIDENCE_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : Object.entries(error.message),
            idUpdated: incidence._id,
       })
    }
}

export const deleteIncidence = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INCIDENCE_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/incidences/${id}/delete`,
            config
       )

       dispatch({
            type: INCIDENCE_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: INCIDENCE_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteIncidence = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INCIDENCE_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/incidences/${id}/undelete`,
            config
       )

       dispatch({
            type: INCIDENCE_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: INCIDENCE_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}