import axios from 'axios';
import { 
    MATCH_LIST_REQUEST, 
    MATCH_LIST_SUCCESS, 
    MATCH_LIST_FAIL,
    MATCH_DETAILS_REQUEST,
    MATCH_DETAILS_SUCCESS, 
    MATCH_DETAILS_FAIL,
    MATCH_DELETE_REQUEST,
    MATCH_DELETE_SUCCESS, 
    MATCH_DELETE_FAIL,
    MATCH_UNDELETE_REQUEST,
    MATCH_UNDELETE_SUCCESS, 
    MATCH_UNDELETE_FAIL,
    MATCH_CREATE_REQUEST,
    MATCH_CREATE_SUCCESS, 
    MATCH_CREATE_FAIL,
    MATCH_UPDATE_REQUEST,
    MATCH_UPDATE_SUCCESS, 
    MATCH_UPDATE_FAIL,
} from '../constants/matchConstants';

export const listMatches = (tournament) => async (dispatch, getState) => {
    try {
        dispatch({type: MATCH_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            `/api/tournaments/${tournament}/matches/`,
            config
        );
        // console.log('ESTOY POR ENTRAR AL MATCH SUCCESS')
        dispatch({
            type: MATCH_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: MATCH_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listMatchesDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: MATCH_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/matches/${id}`)

        dispatch({
            type: MATCH_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: MATCH_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? Object.entries(error.response.data.detail)
                : Object.entries(error.message),
        })
    }
}

export const createMatch = (match) => async (dispatch, getState) => {
    try {
        
        dispatch({
            type: MATCH_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/matches/create/`,
            {...match},
            config
       )

       dispatch({
            type: MATCH_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: MATCH_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : Object.entries(error.message),
       })
    }
}

export const generateStageMatches = (stage, rounds) => async (dispatch, getState) => {
    try {
        
        dispatch({
            type: MATCH_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/stages/${stage}/round-robin/`,
            {rounds: rounds},
            config
       )

       dispatch({
            type: MATCH_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: MATCH_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : Object.entries(error.message),
       })
    }
}

export const generateGroupMatches = (group, rounds) => async (dispatch, getState) => {
    try {
        
        dispatch({
            type: MATCH_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/groups/${group}/round-robin/`,
            {rounds: rounds},
            config
       )

       dispatch({
            type: MATCH_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: MATCH_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : Object.entries(error.message),
       })
    }
}

export const updateMatch = (match) => async (dispatch, getState) => {
    try {
        // console.log(match);
        dispatch({
            type: MATCH_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/matches/${match._id}/update/`,
            match,
            config
       )

       dispatch({
            type: MATCH_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:MATCH_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: MATCH_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: match._id,
       })
    }
}

export const deleteMatch = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: MATCH_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/matches/${id}/delete`,
            config
       )

       dispatch({
            type: MATCH_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: MATCH_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteMatch = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: MATCH_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/matches/${id}/undelete`,
            config
       )

       dispatch({
            type: MATCH_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: MATCH_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}