import axios from 'axios';
import { 
    PLACE_LIST_REQUEST, 
    PLACE_LIST_SUCCESS, 
    PLACE_LIST_FAIL,
    PLACE_DETAILS_REQUEST,
    PLACE_DETAILS_SUCCESS, 
    PLACE_DETAILS_FAIL,
} from '../constants/placeConstants';

export const listPlaces = () => async (dispatch, getState) => {
    try {
        dispatch({type: PLACE_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            '/api/places/',
            config
        );
        // console.log('ESTOY POR ENTRAR AL PLACE SUCCESS')
        dispatch({
            type: PLACE_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: PLACE_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listPlacesDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: PLACE_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/places/${id}`)

        dispatch({
            type: PLACE_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: PLACE_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}
