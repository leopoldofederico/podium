import axios from 'axios';
import { 
    SQUAD_LIST_REQUEST, 
    SQUAD_LIST_SUCCESS, 
    SQUAD_LIST_FAIL,
    SQUAD_FIXTURE_REQUEST, 
    SQUAD_FIXTURE_SUCCESS, 
    SQUAD_FIXTURE_FAIL,
    SQUAD_DETAILS_REQUEST,
    SQUAD_DETAILS_SUCCESS, 
    SQUAD_DETAILS_FAIL,
    SQUAD_DELETE_REQUEST,
    SQUAD_DELETE_SUCCESS, 
    SQUAD_DELETE_FAIL,
    SQUAD_UNDELETE_REQUEST,
    SQUAD_UNDELETE_SUCCESS, 
    SQUAD_UNDELETE_FAIL,
    SQUAD_CREATE_REQUEST,
    SQUAD_CREATE_SUCCESS, 
    SQUAD_CREATE_FAIL,
    SQUAD_UPDATE_REQUEST,
    SQUAD_UPDATE_SUCCESS, 
    SQUAD_UPDATE_FAIL,
} from '../constants/squadConstants';

export const listSquads = () => async (dispatch, getState) => {
    try {
        dispatch({type: SQUAD_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            '/api/squads/',
            config
        );
        // console.log('ESTOY POR ENTRAR AL SQUAD SUCCESS')
        dispatch({
            type: SQUAD_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: SQUAD_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listSquadsDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: SQUAD_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/squads/${id}`)

        dispatch({
            type: SQUAD_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: SQUAD_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createSquad = (squad) => async (dispatch, getState) => {
    try {
        console.log(squad);
        dispatch({
            type: SQUAD_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/squads/create/`,
            {...squad},
            config
       )

       dispatch({
            type: SQUAD_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: SQUAD_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const updateSquad = (squad) => async (dispatch, getState) => {
    try {
        console.log(squad);
        dispatch({
            type: SQUAD_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/squads/${squad._id}/update/`,
            squad,
            config
       )

       dispatch({
            type: SQUAD_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:SQUAD_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: SQUAD_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: squad._id,
       })
    }
}

export const deleteSquad = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: SQUAD_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/squads/${id}/delete`,
            config
       )

       dispatch({
            type: SQUAD_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: SQUAD_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteSquad = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: SQUAD_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/squads/${id}/undelete`,
            config
       )

       dispatch({
            type: SQUAD_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: SQUAD_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const listMatches = (id, stage) => async (dispatch, getState) => {
    try {
        dispatch({type: SQUAD_FIXTURE_REQUEST});

        const {data} = await axios.get(
            `/api/squads/${id}/matches/${stage}/`
        );

        dispatch({
            type: SQUAD_FIXTURE_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: SQUAD_FIXTURE_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}
