import axios from 'axios';
import { 
    STAGE_LIST_REQUEST, 
    STAGE_LIST_SUCCESS, 
    STAGE_LIST_FAIL,
    STAGE_DETAILS_REQUEST,
    STAGE_DETAILS_SUCCESS, 
    STAGE_DETAILS_FAIL,
    STAGE_DELETE_REQUEST,
    STAGE_DELETE_SUCCESS, 
    STAGE_DELETE_FAIL,
    STAGE_UNDELETE_REQUEST,
    STAGE_UNDELETE_SUCCESS, 
    STAGE_UNDELETE_FAIL,
    STAGE_CREATE_REQUEST,
    STAGE_CREATE_SUCCESS, 
    STAGE_CREATE_FAIL,
    STAGE_UPDATE_REQUEST,
    STAGE_UPDATE_SUCCESS, 
    STAGE_UPDATE_FAIL,
} from '../constants/stageConstants';

export const listStages = () => async (dispatch, getState) => {
    try {
        dispatch({type: STAGE_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            'api/stages/',
            config
        );

        dispatch({
            type: STAGE_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: STAGE_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listStagesDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: STAGE_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/stages/${id}`)

        dispatch({
            type: STAGE_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: STAGE_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createStage = (stage) => async (dispatch, getState) => {
    try {
        console.log(stage);
        dispatch({
            type: STAGE_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/stages/create/`,
            {...stage},
            config
       )

       dispatch({
            type: STAGE_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: STAGE_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const updateStage = (stage) => async (dispatch, getState) => {
    try {
        console.log(stage);
        dispatch({
            type: STAGE_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/stages/${stage._id}/update/`,
            stage,
            config
       )

       dispatch({
            type: STAGE_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:STAGE_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: STAGE_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idUpdated: stage._id,
       })
    }
}

export const deleteStage = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STAGE_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/stages/${id}/delete`,
            config
       )

       dispatch({
            type: STAGE_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: STAGE_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteStage = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STAGE_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/stages/${id}/undelete`,
            config
       )

       dispatch({
            type: STAGE_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: STAGE_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}