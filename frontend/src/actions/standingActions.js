import axios from 'axios';
import { 
    STANDINGS_LIST_REQUEST, 
    STANDINGS_LIST_SUCCESS, 
    STANDINGS_LIST_FAIL,
} from '../constants/standingConstants';

export const listStandings = (group) => async (dispatch, getState) => {
    // console.log("entrando a listar con " + group)
    try {
        dispatch({type: STANDINGS_LIST_REQUEST});

        const {data} = await axios.get(`/api/groups/${group}/standings/`);
        // console.log('ESTOY POR ENTRAR AL STANDINGS SUCCESS')
        dispatch({
            type: STANDINGS_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: STANDINGS_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}