import axios from 'axios';
import { 
    TEAM_LIST_REQUEST, 
    TEAM_LIST_SUCCESS, 
    TEAM_LIST_FAIL,
    TEAM_LISTOWN_REQUEST, 
    TEAM_LISTOWN_SUCCESS, 
    TEAM_LISTOWN_FAIL,
    TEAM_DETAILS_REQUEST,
    TEAM_DETAILS_SUCCESS, 
    TEAM_DETAILS_FAIL,
    TEAM_DELETE_REQUEST,
    TEAM_DELETE_SUCCESS, 
    TEAM_DELETE_FAIL,
    TEAM_UNDELETE_REQUEST,
    TEAM_UNDELETE_SUCCESS, 
    TEAM_UNDELETE_FAIL,
    TEAM_CREATE_REQUEST,
    TEAM_CREATE_SUCCESS, 
    TEAM_CREATE_FAIL,
    TEAM_UPDATE_REQUEST,
    TEAM_UPDATE_SUCCESS, 
    TEAM_UPDATE_FAIL,
} from '../constants/teamConstants';

export const listTeams = () => async (dispatch, getState) => {
    try {
        dispatch({type: TEAM_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            'api/teams/',
            config
        );

        dispatch({
            type: TEAM_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: TEAM_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listOwnTeams = () => async (dispatch, getState) => {
    try {
        dispatch({type: TEAM_LISTOWN_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            'api/teams/list/',
            config
        );

        dispatch({
            type: TEAM_LISTOWN_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: TEAM_LISTOWN_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listTeamsDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: TEAM_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/teams/${id}`)

        dispatch({
            type: TEAM_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: TEAM_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createTeam = (team) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TEAM_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        let contentType= "application/json";
        let payload = team;

        if (typeof(team.shield) !== 'string') {
            // shield is NOT a string (it's a File object), so it was updated

            contentType= "multipart/form-data";
            
            payload = new FormData();

            payload.append('shield', team.shield);
            payload.append('name', team.name);
            payload.append('official_name', team.official_name)
            payload.append('place_id', team.place_id)
            
        }

        const config = {
            headers: {
               'Content-type': contentType,
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/teams/create/`,
            payload,
            config
       )

       dispatch({
            type: TEAM_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: TEAM_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const updateTeam = (team) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TEAM_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        let contentType= "application/json";
        let payload = team;

        if (typeof(team.shield) !== 'string') {
            // shield is NOT a string (it's a File object), so it was updated

            contentType= "multipart/form-data";
            payload = new FormData();

            payload.append('shield', team.shield);
            payload.append('_id', team._id);
            payload.append('name', team.name);
            payload.append('official_name', team.official_name)
            payload.append('place_id', team.place_id)
    
        }
        
        const config = {
            headers: {
               'Content-type': contentType,
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/teams/${team._id}/update/`,
            payload,
            config
       )

       dispatch({
            type: TEAM_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:TEAM_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: TEAM_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const deleteTeam = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TEAM_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/teams/${id}/delete`,
            config
       )

       dispatch({
            type: TEAM_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: TEAM_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
            idDeleted: id
       })
    }
}

export const undeleteTeam = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TEAM_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/teams/${id}/undelete`,
            config
       )

       dispatch({
            type: TEAM_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: TEAM_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}