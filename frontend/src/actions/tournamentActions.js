import axios from 'axios';
import { 
    TOURNAMENT_LIST_REQUEST, 
    TOURNAMENT_LIST_SUCCESS, 
    TOURNAMENT_LIST_FAIL,
    AVAILABLETEAMS_LIST_REQUEST, 
    AVAILABLETEAMS_LIST_SUCCESS, 
    AVAILABLETEAMS_LIST_FAIL,
    TOURNAMENT_DETAILS_REQUEST,
    TOURNAMENT_DETAILS_SUCCESS, 
    TOURNAMENT_DETAILS_FAIL,
    TOURNAMENT_DELETE_REQUEST,
    TOURNAMENT_DELETE_SUCCESS, 
    TOURNAMENT_DELETE_FAIL,
    TOURNAMENT_UNDELETE_REQUEST,
    TOURNAMENT_UNDELETE_SUCCESS, 
    TOURNAMENT_UNDELETE_FAIL,
    TOURNAMENT_CREATE_REQUEST,
    TOURNAMENT_CREATE_SUCCESS, 
    TOURNAMENT_CREATE_FAIL,
    TOURNAMENT_UPDATE_REQUEST,
    TOURNAMENT_UPDATE_SUCCESS, 
    TOURNAMENT_UPDATE_FAIL,
    TOURNAMENT_LOGO_REQUEST,
    TOURNAMENT_LOGO_SUCCESS, 
    TOURNAMENT_LOGO_FAIL,
} from '../constants/tournamentConstants';

export const listTournaments = () => async (dispatch, getState) => {
    try {
        dispatch({type: TOURNAMENT_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            'api/tournaments/',
            config
        );

        dispatch({
            type: TOURNAMENT_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: TOURNAMENT_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listAvailableTeams = (id) => async (dispatch, getState) => {
    try {
        dispatch({type: AVAILABLETEAMS_LIST_REQUEST});

        const {
            userLogin: { userInfo }
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.get(
            `/api/tournaments/${id}/available-teams`,
            config
        );

        dispatch({
            type: AVAILABLETEAMS_LIST_SUCCESS,
            payload: data
        });

    } catch(error) {
        dispatch({
            type: AVAILABLETEAMS_LIST_FAIL,
            payload: error.response && error.response.data.detail 
                ? error.response.data.detail
                : error.message,
        });
    }

}

export const listTournamentsDetails = (id) => async (dispatch) => {

    try {
        
        dispatch({type: TOURNAMENT_DETAILS_REQUEST})

        const {data} = await axios.get(`/api/tournaments/${id}`)

        dispatch({
            type: TOURNAMENT_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: TOURNAMENT_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createTournament = (tournament) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TOURNAMENT_CREATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.post(
            `/api/tournaments/create/`,
            {tournament},
            config
       )

       dispatch({
            type: TOURNAMENT_CREATE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: TOURNAMENT_CREATE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const updateTournament = (tournament) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TOURNAMENT_UPDATE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/tournaments/${tournament._id}/update/`,
            tournament,
            config
       )

       dispatch({
            type: TOURNAMENT_UPDATE_SUCCESS,
            payload: data
        });

        dispatch({
            type:TOURNAMENT_DETAILS_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: TOURNAMENT_UPDATE_FAIL,
           payload: error.response && error.response.data.detail
               ? Object.entries(error.response.data.detail)
               : error.message,
       })
    }
}

export const updateLogo = (tournament) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TOURNAMENT_LOGO_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        let contentType= "multipart/form-datan";
           
        let payload = new FormData();
        payload.append('logo', tournament.logo);
        
        const config = {
            headers: {
               'Content-type': contentType,
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.put(
            `/api/tournaments/${tournament._id}/logo/`,
            payload,
            config
       )

       dispatch({
            type: TOURNAMENT_LOGO_SUCCESS,
            payload: data
        });

    } catch(error) {
       dispatch({
           type: TOURNAMENT_LOGO_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const deleteTournament = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TOURNAMENT_DELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/tournaments/${id}/delete`,
            config
       )

       dispatch({
            type: TOURNAMENT_DELETE_SUCCESS,
            payload: data,
            idDeleted: id
        })

    } catch(error) {
       dispatch({
           type: TOURNAMENT_DELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const undeleteTournament = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TOURNAMENT_UNDELETE_REQUEST
        })

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }

        const {data} = await axios.delete(
            `/api/tournaments/${id}/undelete`,
            config
       )

       dispatch({
            type: TOURNAMENT_UNDELETE_SUCCESS,
            payload: data
        })

    } catch(error) {
       dispatch({
           type: TOURNAMENT_UNDELETE_FAIL,
           payload: error.response && error.response.data.detail
               ? error.response.data.detail
               : error.message,
       })
    }
}

export const listMatches = (id) => async (dispatch, getState) => {

    try {
        
        dispatch({type: TOURNAMENT_DETAILS_REQUEST})

        const {
            userLogin: {userInfo}
        } = getState();

        const config = {
            headers: {
               'Content-type': 'application/json',
               Authorization: `Bearer ${userInfo.token}`
            }
        }
        console.log(id)
        const {data} = await axios.get(`/api/tournaments/${id}/matches`, config)

        dispatch({
            type: TOURNAMENT_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: TOURNAMENT_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? Object.entries(error.response.data.detail)
                : Object.entries(error.message),
        })
    }
}

