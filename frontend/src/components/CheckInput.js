import React from 'react';
import { useField } from 'formik';
import { Form } from 'react-bootstrap';

function CheckInput( { children, ...props } ) {
    const [field, meta] = useField({...props, type: "checkbox"});


    return (
        <Form.Group>
            <Form.Check {...field} {...props}
                type="checkbox"
                label={props.label}
            />
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default CheckInput