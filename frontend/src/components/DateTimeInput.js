import React from 'react'
import DatePicker from 'react-datepicker'
import { Form } from 'react-bootstrap';
import { useField } from 'formik';

import "react-datepicker/dist/react-datepicker.css";


function DateTimeInput( { label, ...props } ) {
    const [field, meta, helpers] = useField(props);

    const { setValue } = helpers;
    return (
        <Form.Group>
            <Form.Label>
                {label}
            </Form.Label>
            <DatePicker {...field} {...props}
                isInvalid={meta.touched && meta.error}
                selected={(field.value && new Date(field.value)) || null}
                className="form-control"
                dateFormat="dd/MM/yyyy H:mm"
                // locale="pt-BR"
                // timeFormat="p"
                // dateFormat="Pp"
                // name="startDate"
                onChange={date => setValue(date)}
                showTimeInput
            />
            <Form.Control
                style={{ display: "none" }}
                isInvalid={meta.touched && meta.error}
            />
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default DateTimeInput
