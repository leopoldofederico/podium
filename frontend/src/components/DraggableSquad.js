import React, {useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Squad from './SquadMiniCard'
import { SQUAD } from '../constants/dragConstants'
import { useDrag } from 'react-dnd'
import { assignTeams } from '../actions/groupActions';
import { listTournamentsDetails } from '../actions/tournamentActions';

function DraggableSquad(props) {
    
    const dispatch = useDispatch();

    const teamsAdded = useSelector(state => state.groupUpdate);
    const { error: errorTeamsAdded, loading: loadingTeamsAdded, success: successTeamAdded} = teamsAdded;

    const [{ isDragging }, drag] = useDrag(() => ({
        type: SQUAD,
        collect: (monitor) => ({
          isDragging: !!monitor.isDragging()
        }),
        end (item, monitor) {
            if (monitor.getDropResult()) {
                dispatch(assignTeams([props.squad], monitor.getDropResult()._id))
            }
            
        }
    }))

    useEffect(() => {
        if (successTeamAdded) {
            dispatch(listTournamentsDetails);
        }
    }, [successTeamAdded])
    return (
        <div ref={drag}
            style={{
                opacity: isDragging ? 0.5 : 1,
            }}
        >
            <Squad {...props}/>
        </div>
    )
}

export default DraggableSquad
