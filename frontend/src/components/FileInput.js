import React, { useState, useRef, useEffect } from 'react';
import { useField } from 'formik';
import { Form, Image, Row, Col, Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function FileInput( { label, ...props } ) {
    const [field, meta, helpers] = useField(props);

    const { setValue, setTouched } = helpers;

    const { t } = useTranslation();

    const [uploadedFileName, setUploadedFileName] = useState(null)
    const [newImage, setNewImage] = useState(null)
    const inputRef = useRef(null);

    const handleUpload = () => {
        inputRef.current?.click();
    };
    
    const handleDisplayFileDetails = (e) => {
        inputRef.current?.files && setUploadedFileName(inputRef.current.files[0].name);
        setValue(inputRef.current.files[0])
        setNewImage(URL.createObjectURL(e.target.files[0]))
    };

    return (
        <Form.Group>
            <Form.Label>
                { label }
            </Form.Label>
            <Container>
                <Row className="align-items-center" >
                    <Col md="auto">
                        <Image 
                            src={ newImage ? newImage : "/staticdjango" + (meta.value ? meta.value : "/images/shield.png") } 
                            width="100" 
                            rounded 
                        />
                    </Col>
                    <Col >
                        <Form.File {...field} {...props} custom> 
                            <Form.File.Input
                                onBlur = { () => setTouched(true) }
                                ref={inputRef}
                                onChange={handleDisplayFileDetails}
                            />
                            <Form.File.Label data-browse={t('main.select_file')}>
                                { uploadedFileName || props.placeholder }
                            </Form.File.Label>
                        </Form.File>
                    </Col>
                </Row>
            </Container>
            
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default FileInput
