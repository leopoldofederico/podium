import React from 'react';
import { Trans } from 'react-i18next';
import { Container, Row, Col } from 'react-bootstrap'

function Footer() {
    return (
        <footer>
            <Container>
                <Row>
                    <Col className="text-center py-3">
                        <Trans i18nKey="home.footer">A project by Federico Pértile</Trans>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default Footer

