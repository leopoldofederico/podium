import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Card, Button, Form, Col, Row } from 'react-bootstrap'
import Squad from './SquadMiniCard'
import { BsPencil, BsFillTrashFill, BsX } from 'react-icons/bs';
import { deleteGroup, updateGroup, undeleteGroup, removeTeams } from '../actions/groupActions';
import { listTournamentsDetails } from '../actions/tournamentActions';
import Message from './Message';
import { Trans, useTranslation } from 'react-i18next';
import { SQUAD } from '../constants/dragConstants';
import { useDrop } from 'react-dnd'

function GroupCard( { group, stage, ...props }) {
    const [editMode, setEditMode] = useState(false);
    
    const dispatch = useDispatch();
    const { t } = useTranslation();

    const [name, setName] = useState(group.name)

    const groupDelete = useSelector(state => state.groupDelete);
    const groupUndelete = useSelector(state => state.groupUndelete);
    const { error, loading, success, idDeleted } = groupDelete;
    const { errorUndelete, loadingUndelete, successUndelete } = groupUndelete;
    const [deleted, setDeleted] = useState(false);
    const [errorDeleted, setErrorDeleted] = useState(false);

    const teamsRemoved = useSelector(state => state.groupUpdate);
    const { error: errorTeamsRemoved, loading: loadingTeamsRemoved, success: successTeamRemoved} = teamsRemoved;


    const inputRef = useRef(null);

    const handleDelete = () => {
        dispatch(deleteGroup(group._id));
    }

    const handleUndelete = () => {
        dispatch(undeleteGroup(group._id));
        setDeleted(false);
    }

    const handleChange = e => {
        setName(e.target.value)
    }

    const handleRemoveTeam = e => {
        dispatch(removeTeams([e], group._id))
    }

    const handleBlur = () => {
        if (name != group.name){
            dispatch(updateGroup({
                _id: group._id,
                name: name,
                stage_id: stage,
            }))
        }
        setEditMode(false);
    }

    const toggleEdit = () => {
        setEditMode(!editMode);
    }

    useEffect(() => {
        if (success) {
            setDeleted(true);
        }
        if (error) {
            setErrorDeleted(true);
        }
    }, [success, error])

    useEffect(() => {
        if (editMode) {
            inputRef.current.focus();
        }
    }, [editMode])

    useEffect(() => {
        if (successTeamRemoved) {
            dispatch(listTournamentsDetails)
        }
    }, [successTeamRemoved])

    const handleDropTeam = () => {

    }

    const [ { isOver } , drop] = useDrop(
        () => ({
            accept: SQUAD,
            drop: () => group ? group : {},
            collect: (monitor) => ({
                isOver: !!monitor.isOver()
            })
        })
    )

    return (
        <Card className={props.className} ref={drop} bg={isOver && 'light'}>
            { deleted && idDeleted == group._id ? (
                <Card.Body>
                    <Message variant="success">
                        { t("group.deleted", { group }) }
                        <Button variant="Link" onClick={ handleUndelete }>
                            <Trans i18nKey="main.undo_question" />
                        </Button>
                    </Message>
                </Card.Body>
            ) : ( 
                <div>
                    <Card.Title>
                        <Row>
                            <Col>
                                { editMode ?
                                    (<Form>
                                        <Form.Control
                                            ref={inputRef}
                                            type="text"
                                            value={ name }
                                            onChange={ handleChange }
                                            onBlur={ handleBlur }
                                        />
                                    </Form>)
                    
                                : <strong>{ name }</strong>
                    
                                }
                                <Button variant="link" onClick={ toggleEdit }>
                                    <BsPencil />
                                </Button>
                            </Col>
                            <Col className="text-right">
                                <Button variant="link" onClick={ handleDelete }>
                                    <BsFillTrashFill />
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>
                    <Card.Body>
                        { group.competitors.length ? group.competitors.map(competitor => (
                                <Squad key={competitor._id} squad={competitor} remove={ handleRemoveTeam } />
                            ))
                        : "No teams"}
                    </Card.Body>
                    
                </div>
            )}
        </Card>
    )
}

export default GroupCard
