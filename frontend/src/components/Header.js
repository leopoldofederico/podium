import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Trans } from 'react-i18next';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { logout } from '../actions/userActions'

function Header() {
    
    const userLogin = useSelector( state => state.userLogin)

    const {userInfo} = userLogin;

    const dispatch = useDispatch();

    const logoutHandler = () => {
        dispatch(logout());
    }

    return (
        <header>
            <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
                <Container>
                    <LinkContainer to="/">
                        <Navbar.Brand href="#home">Podium</Navbar.Brand>
                    </LinkContainer>
                    
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <LinkContainer to="/home">
                                <Nav.Link>
                                    <Trans i18nKey="home.home">
                                        Home
                                    </Trans>
                                </Nav.Link>                            
                            </LinkContainer>
                            {userInfo ? (
                                <NavDropdown title={userInfo.first_name} id='username'>
                                    <LinkContainer to='/profile'>
                                        <NavDropdown.Item>
                                            <Trans i18nKey="home.profile">Profile</Trans>
                                        </NavDropdown.Item>
                                    </LinkContainer>
                                    <NavDropdown.Item
                                        onClick={logoutHandler}>
                                            <Trans i18nKey="home.logout">Log out</Trans>
                                    </NavDropdown.Item>
                                </NavDropdown>
                                ) : <LinkContainer to="/login">
                                        <Nav.Link>
                                            <i className="fas fa-user mr-1"> </i>
                                            <Trans i18nKey="home.login">Login</Trans>
                                        </Nav.Link>                            
                                    </LinkContainer>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header>
    )
}

export default Header;