import React, { useEffect, useState } from 'react'
import { useTable, useFilters, useGlobalFilter, useAsyncDebounce, useSortBy } from 'react-table'
import { Table, Button, Form, Container, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import { deleteIncidence, listIncidences } from '../actions/incidenceActions'
import { BsFillTrashFill, BsPencil } from 'react-icons/bs'
import { Trans, useTranslation } from 'react-i18next'
import {matchSorter} from 'match-sorter'
import { LinkContainer } from 'react-router-bootstrap'
import { listTournamentsDetails } from '../actions/tournamentActions'
import IncidenceEditScreen from '../screens/IncidenceEditScreen'

function IncidencesTable( {tournament} ) {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    // const incidencesList = useSelector(state => state.matchList);
    // const { error, incidences } = incidencesList;

    const incidenceDelete = useSelector(state => state.incidenceDelete);
    const { loading, error: errorDelete, success: successDelete, idDeleted } = incidenceDelete;

    const [data, setData] = useState([])
    const [deleted, setDeleted] = useState([])

    const [showEdit, setShowEdit] = useState(false);
    const [incidence, setIncidence] = useState(null);

    const handleDelete = x => {
        dispatch(deleteIncidence(x._id))
    }

    const handleEdit = x => {
        x['tournament'] = tournament._id
        setIncidence(x)
        setShowEdit(true)
    }
    // Define a default UI for filtering
    function GlobalFilter({
        preGlobalFilteredRows,
        globalFilter,
        setGlobalFilter,
    }) {
        const count = preGlobalFilteredRows.length
        const [value, setValue] = React.useState(globalFilter)
        const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
        }, 200)
  
        return (
            <Form.Control
                value={value || ""}
                onChange={e => {
                    setValue(e.target.value);
                    onChange(e.target.value);
                }}
            />
        )
    }

    // Define a default UI for filtering
    function DefaultColumnFilter({ column: { filterValue, preFilteredRows, setFilter },}) {
        
        const count = preFilteredRows.length
  
        return (
            <input
                value={filterValue || ''}
                onChange={e => {
                setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
                }}
                placeholder={`Search ${count} records...`}
            />
        )
    }

    const defaultColumn = React.useMemo(
        () => ({
            // Let's set up our default Filter UI
            Filter: DefaultColumnFilter,
            disableFilters: true,
        }),
        []
    )

    const columns = React.useMemo(
        () => [
            {
                Header: ' ',
                accesor: 'action',
                Cell: (props) => {
                    const row = props.row.values;
                    return (
                        <Container>
                            <Button variant="link" size="sm"
                                className="py-0"
                                onClick={ () => handleDelete(row) }>
                                <BsFillTrashFill />
                            </Button>
                            
                            <Button variant="link" size="sm"
                                className="py-0"
                                onClick={ () => handleEdit(row) }>
                                <BsPencil />
                            </Button>
                            
                        </Container>
                    );
                  },
            },
        
            {
                Header: 'ID',
                accessor: '_id',
                disableFilters: true,
            },
            {
                Header: 'Team',
                accessor: 'squad.name',
                disableFilters: true,
            },
            {
                Header: 'Points',
                accessor: 'points',
                disableFilters: true,

            },
            {
                Header: 'Stage',
                accessor: 'stage',
                // Cell: EditableCell,
            },
            {
                Header: 'Date',
                accessor: d => d.date ? new Date(d.date).toLocaleDateString() : '',
                // Cell: EditableCell,
            },
            {
                Header: 'date',
                accessor: 'date',
                // Cell: EditableCell,
            },
            {
                Header: 'Note',
                accessor: 'note',
                disableFilters: true,
            },

        ],
        []
    )
    
    function fuzzyTextFilterFn(rows, id, filterValue) {
        return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
    }

    const filterTypes = React.useMemo( () => ({
        // Add a new fuzzyTextFilterFn filter type.
        fuzzyText: fuzzyTextFilterFn,
        // Or, override the default text filter to use
        // "startWith"
        text: (rows, id, filterValue) => {
            return rows.filter(row => {
                const rowValue = row.values[id]
                return rowValue !== undefined
                ? String(rowValue)
                    .toLowerCase()
                    .startsWith(String(filterValue).toLowerCase())
                : true
            })
        },
    
    }), [] )

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, state,
        visibleColumns, preGlobalFilteredRows, setGlobalFilter, } = useTable({
            columns,
            data,
            defaultColumn, // Be sure to pass the defaultColumn option
            filterTypes,
            initialState: {hiddenColumns: ['date']}
            },
        useFilters, // useFilters!
        useGlobalFilter, // useGlobalFilter!
        useSortBy)

    useEffect(() => {
        if (idDeleted) {
            let newDeleted = deleted;
            newDeleted.push(idDeleted)
            setDeleted(newDeleted)    
            let newData = data.filter(x => x._id != idDeleted)
            setData(newData)
        }
    }, [idDeleted])

    useEffect(() => {
        if (tournament && tournament.stages) { 
            let newIncidences = [];
            tournament.stages.forEach( stage => {
                stage.incidences.forEach(incidence => {
                    incidence['stage'] = stage.name
                    incidence['tournament_id'] = stage.tournament_id
                    newIncidences.push(incidence)
                })
                
            })
            setData(newIncidences)
        }
    }, [tournament])
    
    useEffect(() => {
        if (tournament && tournament._id) {
            dispatch(listTournamentsDetails(tournament._id))
        }
    }, [dispatch])

    return (
        <Container>
            { successDelete && <Message variant="success">
                { t('incidence.deleted', {id: idDeleted}) }
            </Message> }
            {/* { error && <Message variant="danger">{error}</Message>} */}
            <Row>
                {showEdit && 
                    <IncidenceEditScreen 
                        incidence={incidence} 
                        handleClose={() => setShowEdit(false)}/>}
            </Row>
            <Row>
                <Form.Group>
                    <Form.Label><Trans i18nKey="main.filter" /></Form.Label>
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={state.globalFilter}
                        setGlobalFilter={setGlobalFilter}
                    />
                </Form.Group>
            </Row>
            
            <Row>
                <Table striped bordered hover {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps()}>
                                        {column.render('Header')}
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                
                    <tbody>
                        {rows.map((row, i) => {
                            prepareRow(row)
                
                            return (
                                <tr {...row.getRowProps()}>
                                    {row.cells.map(cell => {
                                        return (
                                            <td className="m-0 p-0" {...cell.getCellProps()}>
                                                {cell.render('Cell')}
                                            </td>
                                        )
                                    })}
                                </tr>
                            )
                
                
                        })}
                    </tbody>
                </Table>
            </Row>
        </Container>
    )
}

export default IncidencesTable
