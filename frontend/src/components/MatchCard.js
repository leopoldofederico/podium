import React, { useState } from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import SquadMiniCard from './SquadMiniCard';

function MatchCard({match, ...props}) {
    
    const { t } = useTranslation();

    return (
        <Col xs={12} md={8} xl={6}>
            <Card className="mt-1">
                <Card.Header>
                    { match.date && ((new Date(match.date)).toLocaleString([],
                            {year: 'numeric', month:'2-digit', day:'2-digit'})
                    )}
                    { match.round && (` ${t('match.round')}: ${match.round}`) }
                    { match.series && (` ${t('match.series')}: ${match.series}`) }
                </Card.Header>
                <Card.Body>
                    <Card.Text as="div">
                        <Card.Title>
                            <Row className="align-items-center">
                                <Col xs={5}>
                                    <Row className="flex-row-reverse">
                                        <SquadMiniCard squad={ match.team1 } border="none" />
                                    </Row>
                                </Col>
                                <Col xs={2}>
                                    <Row className="justify-content-center">
                                        vs.
                                    </Row>
                                </Col>
                                <Col xs={5}>
                                    <Row>
                                        <SquadMiniCard squad={ match.team2 } border="none" />
                                    </Row>
                                </Col>
            
                            </Row>
                        </Card.Title>
                        <Card.Subtitle>
                            <Row>
                                <Col xs={5}>
                                    <Row className="flex-row-reverse">
                                        { match.team1_score }
                                    </Row>
                                </Col>
                                <Col xs={2}>
                                    <Row className="justify-content-center">-</Row>
                                </Col>
                                <Col xs={5}>
                                    <Row>{ match.team2_score }</Row>
                                </Col>
                            </Row>
                        </Card.Subtitle>
                    </Card.Text>
                </Card.Body>
                { match.fixture_note && 
                    <Card.Footer>
                        { match.fixture_note }
                    </Card.Footer>
                }
            </Card>
        </Col>
  )
}

export default MatchCard