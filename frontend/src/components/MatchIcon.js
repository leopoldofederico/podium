import { matchSorter } from 'match-sorter'
import React from 'react'
import { OverlayTrigger, Row, Tooltip } from 'react-bootstrap';
import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import { IconContext } from 'react-icons/lib'

function MatchIcon( {match} ) {
    const renderTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            { match.role=="home" ?
                `${match.name} ${match.scorefor} - ${match.scoreagainst} ${match.opponent_name}`
                : `${match.opponent_name} ${match.scoreagainst} - ${match.scorefor} ${match.name}`
            }
        </Tooltip>
      );

      return (
        <IconContext.Provider value={{ color: match.status=="Win" ? "green" : "red" }}>
            <OverlayTrigger 
                // overlay={<Tooltip>puto</Tooltip>}
                overlay = {renderTooltip}
            >
                {match.status=="Win" ? 
                    <FaCheckCircle className="mr-1" /> : 
                    <FaTimesCircle className="mr-1" />}
                
            </OverlayTrigger>
        </IconContext.Provider>

        
    )
} 

export default MatchIcon
