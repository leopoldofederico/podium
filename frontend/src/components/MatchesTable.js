import React, { useEffect, useState } from 'react'
import { useTable, useFilters, useGlobalFilter, useAsyncDebounce, useSortBy } from 'react-table'
import { Table, Button, Form, Container, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import { deleteMatch, listMatches } from '../actions/matchActions'
import { BsFillTrashFill, BsPencil } from 'react-icons/bs'
import { Trans, useTranslation } from 'react-i18next'
import {matchSorter} from 'match-sorter'
import { LinkContainer } from 'react-router-bootstrap'

function MatchesTable( {tournament} ) {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const matchesList = useSelector(state => state.matchList);
    const { error, matches } = matchesList;

    const matchDelete = useSelector(state => state.matchDelete);
    const { loading, error: errorDelete, success: successDelete, idDeleted } = matchDelete;

    const [data, setData] = useState([])
    const [deleted, setDeleted] = useState([])

    /* TODO: una URL con todos los partidos del torneo y hacer un SetData
        armar primer filtro
        armar otros filtros
        botón edición
    */

    const handleDelete = x => {
        dispatch(deleteMatch(x._id))
    }

    // Define a default UI for filtering
    function GlobalFilter({
        preGlobalFilteredRows,
        globalFilter,
        setGlobalFilter,
    }) {
        const count = preGlobalFilteredRows.length
        const [value, setValue] = React.useState(globalFilter)
        const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
        }, 200)
  
        return (
            <Form.Control
                value={value || ""}
                onChange={e => {
                    setValue(e.target.value);
                    onChange(e.target.value);
                }}
            />
        )
    }

    // Define a default UI for filtering
    function DefaultColumnFilter({ column: { filterValue, preFilteredRows, setFilter },}) {
        
        const count = preFilteredRows.length
  
        return (
            <input
                value={filterValue || ''}
                onChange={e => {
                setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
                }}
                placeholder={ t('main.filter') }
            />
        )
    }

    const defaultColumn = React.useMemo(
        () => ({
            // Let's set up our default Filter UI
            Filter: DefaultColumnFilter,
            disableFilters: false,
        }),
        []
    )

    const columns = React.useMemo(
        () => [
            {
                Header: ' ',
                accesor: 'action',
                Cell: (props) => {
                    const row = props.row.values;
                    return (
                        <Container>
                            <Button variant="link" size="sm"
                                className="py-0"
                                onClick={ () => handleDelete(row) }>
                                <BsFillTrashFill />
                            </Button>
                            <LinkContainer to={`/match/${row._id}/edit`}>
                                <Button variant="link" size="sm"
                                    className="py-0">
                                    <BsPencil />
                                </Button>
                            </LinkContainer>
                        </Container>
                    );
                  },
            },
        
            {
                Header: 'ID',
                accessor: '_id',
                
            },
            {
                Header: t('match.home'),
                accessor: 'team1.name',
                filter: 'fuzzyText',
            },
            {
                Header: t('match.score'),
                accessor: d => `${d.team1_score ? d.team1_score : ''}-${d.team2_score ? d.team2_score : ''}`,
                Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>

            },
            {
                Header: t('match.away'),
                accessor: 'team2.name',
                filter: 'fuzzyText',
            },
            {
                Header: t('stage.title'),
                accessor: 'stage.name',
            },
            {
                Header: t('match.date'),
                accessor: d => d.date ? new Date(d.date).toLocaleDateString() : '',
            },
            {
                Header: t('match.round'),
                accessor: 'round',
                Cell: row => <div style={{ textAlign: "right" }}>{row.value}</div>,
            },
            {
                Header: t('match.series'),
                accessor: 'series',
                Cell: row => <div style={{ textAlign: "right" }}>{row.value}</div>,
            },
        ],
        []
    )
    
    function fuzzyTextFilterFn(rows, id, filterValue) {
        return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
    }

    const filterTypes = React.useMemo( () => ({
        // Add a new fuzzyTextFilterFn filter type.
        fuzzyText: fuzzyTextFilterFn,
        // Or, override the default text filter to use
        // "startWith"
        text: (rows, id, filterValue) => {
            return rows.filter(row => {
                const rowValue = row.values[id]
                return rowValue !== undefined
                ? String(rowValue)
                    .toLowerCase()
                    .startsWith(String(filterValue).toLowerCase())
                : true
            })
        },
    
    }), [] )

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, state,
        visibleColumns, preGlobalFilteredRows, setGlobalFilter, } = useTable({
            columns,
            data,
            defaultColumn, // Be sure to pass the defaultColumn option
            filterTypes,
            },
        useFilters, // useFilters!
        useGlobalFilter, // useGlobalFilter!
        useSortBy)

    useEffect(() => {
        if (idDeleted) {
            let newDeleted = deleted;
            newDeleted.push(idDeleted)
            setDeleted(newDeleted)    
            let newData = data.filter(x => x._id != idDeleted)
            setData(newData)
        }
    }, [idDeleted])

    useEffect(() => {
        if (matches) { 
            setData(matches) 
        }
    }, [matches])
    
    useEffect(() => {
        if (tournament && tournament._id) {
            dispatch(listMatches(tournament._id))
        }
    }, [dispatch, tournament])

    return (
        <Container>
            { successDelete && <Message variant="success">
                { t('match.deleted', {id: idDeleted}) }
            </Message> }
            { error && <Message variant="danger">{error}</Message>}
            
            <Row>
                <Form.Group>
                    <Form.Label><Trans i18nKey="main.filter" /></Form.Label>
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={state.globalFilter}
                        setGlobalFilter={setGlobalFilter}
                    />
                </Form.Group>
            </Row>
            
            <Row>
                <Table striped bordered hover {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps()}>
                                        <Row className="px-1">{column.render('Header')}</Row>
                                        <Row className="px-1">{column.filter ? column.render('Filter') : null }</Row>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                
                    <tbody>
                        {rows.map((row, i) => {
                            prepareRow(row)
                
                            return (
                                <tr {...row.getRowProps()}>
                                    {row.cells.map(cell => {
                                        return (
                                            <td className="m-0 p-0" {...cell.getCellProps()}>
                                                {cell.render('Cell')}
                                            </td>
                                        )
                                    })}
                                </tr>
                            )
                
                
                        })}
                    </tbody>
                </Table>
            </Row>
        </Container>
    )
}

export default MatchesTable
