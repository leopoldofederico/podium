import React from 'react';
import { useField } from 'formik';
import { Form } from 'react-bootstrap';

function NameInput( { label, ...props } ) {
    const [field, meta] = useField(props);


    return (
        <Form.Group className={props.className}>
            <Form.Label className={props.className}>
                {label}
            </Form.Label>
            <Form.Control {...field} {...props}
                type="text"
                placeholder={props.placeholder}
                isInvalid={meta.touched && meta.error}
            ></Form.Control>
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default NameInput
