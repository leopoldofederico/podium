import React from 'react'
import { Card, Col } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'

function PersonCard( {person, ...props}) {
    const { t }= useTranslation();

  return (
    <Col xs={12} md={8} xl={3}>
        <Card className="mt-1">
            <Card.Img variant="top" src={ "/staticdjango/" + person.photo } height="200" style={{ objectFit: 'contain' }}/>
            <Card.Body>
                <Card.Title>{ person.last_name}, { person.first_name }</Card.Title>
                <Card.Text>
                    { t('person.born_at') }: { (new Date(person.born_at)).toLocaleString([],
                            {year: 'numeric', month:'2-digit', day:'2-digit'})
                    }
                </Card.Text>
            </Card.Body>
        </Card>
    </Col>
  )
}

export default PersonCard