import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, useLocation } from 'react-router';

function RequireLogin({ children }: { children: JSX.Element }) {
    
    const userLogin = localStorage.getItem('userInfo');
    let location = useLocation();
    if (!userLogin) {
        return <Navigate to="/login" state={{ from: location }} replace />;
    }
  
    return children;
  ;
}

export default RequireLogin;
