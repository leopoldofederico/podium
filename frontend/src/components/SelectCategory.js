import React, { useState, useEffect } from 'react'
import CreatableSelect from 'react-select/creatable'
import { useDispatch, useSelector } from 'react-redux';
import { listCategories, createCategory } from '../actions/categoryActions';
import { Button, Row, Col } from 'react-bootstrap'
import { BsFillGearFill } from "react-icons/bs";
import { LinkContainer } from 'react-router-bootstrap';

function SelectCategory( props ) {

	const [loaded, setLoaded] = useState(false);
	const [options, setOptions] = useState([]);

	const dispatch = useDispatch();
    
	const categoryList = useSelector(state => state.categoryList);
    const { error, loading, categories } = categoryList;

	const categoryCreate = useSelector(state => state.categoryCreate);
    const { loading: loadingCreate, category: categoryCreated, success: successCreated } = categoryCreate;
        
    const handleCreate = input => {
		let category = { name: input }
		dispatch(createCategory(category));
    }

	useEffect(() => {
		dispatch(listCategories());
	},[dispatch])

	useEffect(() => {
		categoryCreated && props.onChange(categoryCreated);
	}, [categoryCreated])

    return (
		<Row>
			<Col lg={true}>
				<CreatableSelect
					getOptionLabel={e => e.name}
					getOptionValue={e => e._id}
					getNewOptionData={(inputValue, optionLabel) => ({
						name: optionLabel,
						_id: inputValue,
						__isNew__: true
					})}
					isSearchable={true}
					options = { categories }
					onCreateOption={ handleCreate }
					value = { props.initialOption }
					onChange = { value => {
					  props.onChange(value);
					} }
					onBlur = { () => props.onBlur(true) }
					placeholder = { props.placeholder }
					isLoading = {loading}
					noOptionsMessage = { error }
				/>
			</Col>
			<LinkContainer to="/categories">
				<Row>
					<Button className="mx-2">
						<Row className="justify-content-md-center">
							<BsFillGearFill />
						</Row>
					</Button>
				</Row>
			</LinkContainer>
		</Row>
    ) 
    
}

export default SelectCategory