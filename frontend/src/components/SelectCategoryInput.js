import React from 'react'
import SelectCategory from './SelectCategory'
import { useField } from 'formik'
import { Form } from 'react-bootstrap'
import { Trans } from 'react-i18next'

function SelectCategoryInput({label, ...props}) {
    const [field, meta, helpers] = useField(props);

    const { setValue, setTouched } = helpers;
    return (
        <Form.Group>
            <Form.Label>
                <Trans i18nKey="category.title">Category</Trans>
            </Form.Label>
            <Form.Control {...field} {...props}
                type="text"
                placeholder={props.placeholder}
                style={{display: 'none'}}
                isInvalid={meta.touched && meta.error}
            ></Form.Control>
            <SelectCategory 
                onBlur = { () => setTouched(true) }
                onChange = { value => setValue(value._id)}
                error = { meta.error }
                initialOption = { props.initial }
                
            />
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{ meta.error }</Form.Control.Feedback>
            ) : null}
        </Form.Group>

    )
}

export default SelectCategoryInput
