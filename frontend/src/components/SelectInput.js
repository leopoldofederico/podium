import React from 'react';
import ReactDOM from 'react-dom';
import { useField } from 'formik';
import * as Yup from 'yup';
import { Form } from 'react-bootstrap';
import Select from 'react-select'

function SelectInput( { label, ...props } ) {
    const [field, meta, helpers] = useField(props);

    const { setValue, setTouched } = helpers;
    
    return (
        <Form.Group>
            <Form.Label>
                {label}
            </Form.Label>
            <Select 
                {...field}
                {...props}
                value = { field.value && props.options ?  props.options.find(option => option.value === field.value)  : ''}
                onChange={option => {
                    props.onSelect && props.onSelect(option);
                    let value = props.getOptionValue(option);
                    setValue(value);
                } }
            />
            <Form.Control
                style={{ display: "none" }}
                isInvalid={meta.touched && meta.error}
            />
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default SelectInput
