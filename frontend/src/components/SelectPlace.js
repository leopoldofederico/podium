import React, { useState, useEffect } from 'react'
import Select from 'react-select'
import { useDispatch, useSelector } from 'react-redux';
import { listPlaces } from '../actions/placeActions';

function SelectPlace( props ) {

    const dispatch = useDispatch();
    
	const placeList = useSelector(state => state.placeList);
    const { error, loading, places } = placeList;

	useEffect(() => {
		dispatch(listPlaces());
	},[dispatch])

    return (
        <Select
            getOptionLabel={e => e.full_name}
            getOptionValue={e => e._id}
            isSearchable={true}
            options = { places }
            value = { props.initialOption }
            onChange = { value => {
                props.onChange(value);
            } }
            onBlur = { () => props.onBlur(true) }
            placeholder = { props.placeholder }
            isLoading = {loading}
            // noOptionsMessage = { error }
        />
    )
}

export default SelectPlace
