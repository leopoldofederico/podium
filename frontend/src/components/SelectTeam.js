import React, { useState, useEffect } from 'react'
import Select from 'react-select'
import { useDispatch, useSelector } from 'react-redux';
import { listTeams } from '../actions/teamActions';


function SelectTeam( props ) {


    const dispatch = useDispatch();
    
	const teamList = useSelector(state => state.teamList);
    const { error, loading, teams } = teamList;

	useEffect(() => {
		dispatch(listTeams());
	},[dispatch])

    return (
        <Select
            getOptionLabel={e => e.official_name}
            getOptionValue={e => e._id}
            isSearchable={true}
            options = { teams }
            value = { props.initialOption }
            onChange = { value => {
                props.onChange(value);
            } }
            onBlur = { () => props.onBlur(true) }
            placeholder = { props.placeholder }
            isLoading = {loading}
        />
    )
}

export default SelectTeam
