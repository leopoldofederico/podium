import React, { useEffect, useState,  useRef } from 'react';
import { Card, Col, Row, Image, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { BsX, BsPencil } from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux'
import { deleteSquad, undeleteSquad } from '../actions/squadActions'
import Message from '../components/Message';
import { useTranslation, Trans } from 'react-i18next';
import { Formik, Form as FormFormik } from 'formik';
import { updateSquad } from '../actions/squadActions';

function SquadCard( {squad, tournament, ...props} ) {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const squadDelete = useSelector(state => state.squadDelete);
    const squadUndelete = useSelector(state => state.squadUndelete);
    const squadUpdate = useSelector(state => state.squadUpdate);
    const inputRef = useRef(null);

    const { error, loading, success, idDeleted } = squadDelete;
    const { errorUndelete, loadingUndelete, successUndelete } = squadUndelete;
    const { error: errorUpdate, loading: loadingUpdate, successUpdate, idUpdated } = squadUpdate;

    const [deleted, setDeleted] = useState(false);
    const [editMode, setEditMode] = useState(false);

    const [name, setName] = useState(squad.name);

    const handleDelete = () => {
        dispatch(deleteSquad(squad._id));
    }

    const handleUndelete = () => {
        dispatch(undeleteSquad(squad._id));
        setDeleted(false);
    }

    const toggleEdit = () => setEditMode(!editMode);

    useEffect(() => {
        if (editMode) {
            inputRef.current.focus();
        }
    }, [editMode])

    const handleChange = e => setName(e.target.value);

    const handleBlur = () => {
        if (name != squad.name){
            dispatch(updateSquad({
                _id: squad._id,
                name: name,
                team_id: squad.team._id,
                tournament_id: tournament,
            }))
        }
        setEditMode(false);
    }

    useEffect(() => {
        if (success) {
            setDeleted(true);
        }
    }, [success])

    return (
        <Card className="my-1 p-1 rounded">
            { deleted && idDeleted == squad._id ? (
                <Card.Body>
                    <Message variant="success">
                        { t("squad.deleted", { squad }) }
                        <Button variant="Link" onClick={ handleUndelete }>
                            <Trans i18nKey="main.undo_question" />
                        </Button>
                    </Message>
                </Card.Body>
            ) : ( 
                <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col xs={1}>
                                <Image src={ "/staticdjango" + squad.team.shield } width="30" rounded />
                            </Col>
                            <Col xs={6}>
                                { editMode ?
                                    (<Form>
                                        <Form.Control 
                                            ref={inputRef} 
                                            type="text" 
                                            value={ name } 
                                            onChange={ handleChange }
                                            onBlur={ handleBlur }
                                        />
                                    </Form>)
                                : (<Link to={`/squad/${squad.team._id}`}>
                                    <strong>{ name }</strong>
                                </Link>)}
                                <Button variant="link" onClick={ toggleEdit }>
                                    <BsPencil />
                                </Button>
                            </Col>
                            <Col className="text-right">
                                <Button variant="link" onClick={ handleDelete }>
                                    <BsX />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            { errorUpdate && <Form.Text className="text-muted">{errorUpdate.name}</Form.Text>}
                        </Row>
                    </Card.Title>
                        <Card.Subtitle as="my-3">
                            { squad.team.official_name + " - " + squad.team.place.full_name }
                        </Card.Subtitle>
                </Card.Body>
            )}
        </Card>
    )
}

export default SquadCard
