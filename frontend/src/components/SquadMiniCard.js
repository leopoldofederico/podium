import React from 'react';
import { Image, Button } from 'react-bootstrap';
import { BsX } from 'react-icons/bs'

function SquadMiniCard( {squad, ...props} ) {

    return (
        <div className="flex-direction-row-reverse mt-1 p-1" style={{ border: props.border ? props.border : '1px solid #C0C0C0' }}>
            <Image className="mr-1" src={ "/staticdjango/" + squad.team.shield } width="20" rounded />
            { squad.name }
            {props.remove && 
                <Button onClick={()=>props.remove(squad)} variant="link">
                    <BsX />
                </Button>
            }
        </div>
    )
}

export default SquadMiniCard
