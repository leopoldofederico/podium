import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Card, Row, Col, Button, Form, Modal } from 'react-bootstrap'
import { Formik, Form as FormFormik } from 'formik';
import { Trans, useTranslation } from 'react-i18next'
import { BsFillTrashFill, BsPencil, BsFillCaretDownFill, BsFillCaretUpFill } from 'react-icons/bs';
import Message from './Message';
import { 
    updateStage, 
    deleteStage, 
    undeleteStage
} from '../actions/stageActions';
import Group from './GroupCard';
import NameInput from './NameInput';
import * as Yup from 'yup'
import { createGroup } from '../actions/groupActions';
import { listTournamentsDetails } from '../actions/tournamentActions'
import StageEditModal from '../screens/StageEditModal';

function StageCard( {stage, tournament, props}) {

    const dispatch = useDispatch();
    const { t } = useTranslation();

    const [showCreate, setShowCreate] = useState(false)
    
    const stageDelete = useSelector(state => state.stageDelete);
    const stageUndelete = useSelector(state => state.stageUndelete);
    const { error, loading, success, idDeleted } = stageDelete;
    const { errorUndelete, loadingUndelete, successUndelete } = stageUndelete;
    const [deleted, setDeleted] = useState(false);
    const [errorDeleted, setErrorDeleted] = useState(false);
    
    const groupCreate = useSelector(state => state.groupCreate);
    const { error: errorCreate, group: groupCreated, success: successCreate } = groupCreate;

    
    const [editMode, setEditMode] = useState(false);
    const [detailsMode, setDetailsMode] = useState(false);
    const [showEditStage, setShowEditStage] = useState(false);
    const [name, setName] = useState(stage.name)
    // setName(stage.name)
    const inputRef = useRef(null);

    const handleDelete = () => {
        dispatch(deleteStage(stage._id));
    }

    const handleUndelete = () => {
        dispatch(undeleteStage(stage._id));
        setDeleted(false);
    }

    const handleChange = e => {
        setName(e.target.value)
    }

    const handleBlur = () => {
        if (name != stage.name){
            dispatch(updateStage({
                _id: stage._id,
                name: name,
                tournament_id: tournament,
            }))
        }
        setEditMode(false);
    }

    const handleCreate = () => {
        setShowCreate(true);
    }

    const handleClose = () => {
        setShowCreate(false);
    }

    const handleNewGroup = x => {
        dispatch(createGroup({ 
            name: x.name,
            stage_id: stage._id
        }));
        handleClose();
    }

    const toggleEdit = () => setEditMode(!editMode);

    const toggleDetails = () => setDetailsMode(!detailsMode);

    const handleEditClose = () => setShowEditStage(false);

    useEffect(() => {
        if (success) {
            setDeleted(true);
        }
        if (error) {
            setErrorDeleted(true);
        }
    }, [success, error])

    useEffect(() => {
        if (editMode) {
            inputRef.current.focus();
        }
    }, [editMode])

    useEffect(() => {
        if (groupCreated) {
            dispatch(listTournamentsDetails(tournament))
        }
    }, [groupCreated])

    return (
        <Card className="my-3 p-1 rounded">
            { deleted && idDeleted == stage._id ? (
                <Card.Body>
                    <Message variant="success">
                        { t("stage.deleted", { stage }) }
                        <Button variant="Link" onClick={ handleUndelete }>
                            <Trans i18nKey="main.undo_question" />
                        </Button>
                    </Message>
                </Card.Body>
            ) : ( 
                <Card.Body>
                    { errorDeleted && idDeleted == stage._id && <Message variant="danger">{error}</Message> }
                    <Card.Title>
                        <Row>
                            <Col>
                                { editMode ?
                                    (<Form>
                                        <Form.Control 
                                            ref={inputRef} 
                                            type="text" 
                                            value={ name } 
                                            onChange={ handleChange }
                                            onBlur={ handleBlur }
                                        />
                                    </Form>)
                                
                                : <strong>{ name }</strong>
                                
                                }
                                <Button variant="link" onClick={ toggleEdit }>
                                    <BsPencil />
                                </Button>
                            </Col>
                            <Col className="text-right">
                                <Button variant="link" onClick={ handleDelete }>
                                    <BsFillTrashFill />
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>
                    <Card.Subtitle as="my-3">
                        <Row>
                            { t('group.amount', {count: stage.groups.length}) }
                            <Button className="ml-1" variant="link" size="sm" onClick={ toggleDetails }>
                                { detailsMode ? <BsFillCaretUpFill /> : <BsFillCaretDownFill /> }
                            </Button>
                        </Row>
                        { detailsMode && (
                        <div>
                            <Row>
                                <strong><Trans i18nKey="tournament.points"/> </strong>
                                &nbsp;<i><Trans i18nKey="tournament.win"/></i>&nbsp;{stage.win_points}
                                &nbsp;-&nbsp;<i><Trans i18nKey="tournament.draw"/></i>&nbsp;{stage.draw_points}
                                &nbsp;-&nbsp;<i><Trans i18nKey="tournament.lost"/></i>&nbsp;{stage.lost_points}
                                &nbsp;-&nbsp;<i><Trans i18nKey="tournament.abandonment"/></i>&nbsp;{stage.abandon_points}
                                &nbsp;({stage.goals_win_abandon}-{stage.goals_lost_abandon})
                                <Button variant="link" onClick={ setShowEditStage }>
                                    <BsPencil />
                                </Button>
                            </Row>
                            <Row>
                                <Button className="m-1" onClick={handleCreate}>
                                    <Trans i18nKey="group.new"></Trans>
                                </Button>
                            </Row>
                        </div>)}
                        
                        { showEditStage && 
                            <StageEditModal 
                                stage={stage} 
                                tournament={tournament._id} 
                                handleClose={ handleEditClose }
                            />
                        }
                        
                        <Modal show={showCreate} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    <Trans i18nKey="group.new">New group</Trans>
                                </Modal.Title>
                            </Modal.Header>
                            <Formik
                                enableReinitialize={true}
                                initialValues={{
                                    name: name,
                                }}
                                validationSchema={Yup.object({
                                    name: Yup.string().required(t('validate.required')),
                                })}
                                onSubmit={(values, { setSubmitting }) => {
                                    handleNewGroup(values);
                                }}
                            >
                                <FormFormik>
                                    <Modal.Body>                            
                                        <NameInput
                                            label={t('group.name')}
                                            name="name"
                                            placeholder={t('stage.name_placeholder')}
                                        />
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            <Trans i18nKey="home.close">Close</Trans>
                                        </Button>
                                        <Button variant="primary" type="submit">
                                            <Trans i18nKey="home.save">Save</Trans>
                                        </Button>
                                    </Modal.Footer>
                                </FormFormik>
                            </Formik>
                        </Modal>
                        <Row>
                            { detailsMode && stage.groups.map(group => 
                                <Group className="m-1" key={stage._id} group={group} stage={stage._id} />
                            )}
                        </Row>
                    </Card.Subtitle>    
                </Card.Body>
            )}
        </Card>
    )
}

export default StageCard
