/*
TODO:
¿al tener update exitoso actualizar valores?}
no hacer update si no se cambió el dato
realimentación positiva o negativa por update
el <input> solo en nombre, no en id
agregar un botón de delete
agregar una opción de crear
traducciones en columna name
cambiar nombre TableContainer por TableCategory
*/

import React from 'react'
import { useTable, useSortBy } from "react-table"
import { Table, Container } from 'react-bootstrap'
import { updateCategory } from '../actions/categoryActions'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'


function TableContainer( {columns, data}) {

    const dispatch = useDispatch();
    const categoryUpdate = useSelector(state => state.categoryUpdate);
    const { error, category: categoryUpdated, success } = categoryUpdate;

    const updateMyData = (index, id, value) => {
        dispatch(updateCategory({ 
            _id: data[index]._id, 
            name: value, 
         }))
        return index;
    }

    const EditableCell = ({
        value: initialValue,
        row: { index },
        column: { id },
        updateMyData, // This is a custom function that we supplied to our table instance
      }) => {
        // We need to keep and update the state of the cell normally
        const [value, setValue] = React.useState(initialValue)
    
        const onChange = e => {
        setValue(e.target.value)
        }
    
        // We'll only update the external data when the input is blurred
        const onBlur = () => {
            //console.log("index " + index + " id " + id + " value " + value);
            updateMyData(index, id, value)
        }
    
        // If the initialValue is changed external, sync it up with our state
        React.useEffect(() => {
            setValue(initialValue)
        }, [initialValue])
    
        return <input className="m-1 p-0 border border-0" style={{ backgroundColor: "transparent"}} value={value} onChange={onChange} onBlur={onBlur} />
    }

    const defaultColumn = {
        Cell: EditableCell,
    }
    
    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
        columns,
        data,
        defaultColumn,
        updateMyData
    }, useSortBy)

    return (
        <div>
            { success && <Message variant="success">Se modificó la categoría {categoryUpdated._id}</Message> }
            { error && <Message variant="danger">{error}</Message>}
            <Table striped bordered hover {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
            
                <tbody>
                    {rows.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td className="m-0 p-0" {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </td>
                                    )
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>
    )
}

export default TableContainer
