import React, { useState, useEffect } from 'react';
import { Card, Col, Row, Image, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { BsFillTrashFill } from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux'
import { deleteTeam, undeleteTeam } from '../actions/teamActions'
import Message from '../components/Message';
import { useTranslation, Trans } from 'react-i18next';

function TeamCard( {team, ...props}  ) {

    const dispatch = useDispatch();
    const { t } = useTranslation();
    const teamDelete = useSelector(state => state.teamDelete);
    const teamUndelete = useSelector(state => state.teamUndelete);
    const { error, loading, success, idDeleted } = teamDelete;
    const { errorUndelete, loadingUndelete, successUndelete } = teamUndelete;
    const [deleted, setDeleted] = useState(false);
    const [errorDeleted, setErrorDeleted] = useState(false);

    const handleDelete = () => {
        dispatch(deleteTeam(team._id));
    }

    const handleUndelete = () => {
        dispatch(undeleteTeam(team._id));
        setDeleted(false);
    }

    useEffect(() => {
        if (success) {
            setDeleted(true);
        }
        if (error) {
            setErrorDeleted(true);
        }
    }, [success, error])

    return (
        <Card className="my-3 p-1 rounded" style={{ width: '50%' }}>
            { deleted && idDeleted == team._id ? (
                <Card.Body>
                    <Message variant="success">
                        { t("team.deleted", { team }) }
                        <Button variant="Link" onClick={ handleUndelete }>
                            <Trans i18nKey="main.undo_question" />
                        </Button>
                    </Message>
                </Card.Body>
            ) : ( 
                <Card.Body>
                    { errorDeleted && idDeleted == team._id && <Message variant="danger">{error}</Message> }
                    <Card.Title>
                        <Row>
                            <Col xs={1}>
                                <Image src={ "staticdjango" + team.shield } width="30" rounded />
                            </Col>
                            <Col xs={6}>
                                <Link to={`/team/${team._id}`}>
                                    <strong>{team.name}</strong>
                                </Link>
                            </Col>
                            <Col className="text-right">
                                <Button variant="link" onClick={ handleDelete }>
                                    <BsFillTrashFill />
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>
                    <Card.Subtitle as="my-3">
                            {team.official_name} - {team.place.full_name}
                    </Card.Subtitle>
                </Card.Body>
            )}
        </Card>
    )
}

export default TeamCard
