import React from 'react';
import { useField } from 'formik';
import { Form } from 'react-bootstrap';

function TextAreaInput( { label, ...props } ) {
    const [field, meta] = useField(props);


    return (
        <Form.Group className={props.className}>
            <Form.Label>
                {label}
            </Form.Label>
            <Form.Control {...field} {...props}
                as="textarea"
                placeholder={props.placeholder}
                isInvalid={meta.touched && meta.error}
            ></Form.Control>
            {meta.touched && meta.error ? (
                <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
            ) : null}
        </Form.Group>
    )
}

export default TextAreaInput
