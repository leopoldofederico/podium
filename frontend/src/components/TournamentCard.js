import React, { useEffect, useState } from 'react';
import { Card, Col, Row, Form, Button } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { BsFillTrashFill } from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux'
import { deleteTournament, undeleteTournament } from '../actions/tournamentActions'
import Message from '../components/Message';
import { useTranslation, Trans } from 'react-i18next';

function TournamentCard( {tournament, ...props} ) {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const tournamentDelete = useSelector(state => state.tournamentDelete);
    const tournamentUndelete = useSelector(state => state.tournamentUndelete);
    const { error, loading, success, idDeleted } = tournamentDelete;
    const { errorUndelete, loadingUndelete, successUndelete } = tournamentUndelete;
    const [deleted, setDeleted] = useState(false);

    const handleDelete = () => {
        dispatch(deleteTournament(tournament._id));
    }

    const handleUndelete = () => {
        dispatch(undeleteTournament(tournament._id));
        setDeleted(false);
    }

    useEffect(() => {
        if (success) {
            setDeleted(true);
        }
    }, [success])
    return (
        
        <Card className="my-3 p-1 rounded" style={{ width: '50%' }}>
            { deleted && idDeleted == tournament._id ? (
                <Card.Body>
                    <Message variant="success">
                        { t("tournament.deleted", { tournament }) }
                        <Button variant="Link" onClick={ handleUndelete }>
                            <Trans i18nKey="main.undo_question" />
                        </Button>
                    </Message>
                </Card.Body>
            ) : ( 
                <Card.Body>
                    <Card.Title>
                            <Row>
                                <Col>
                                    <Link to={`/tournament/${tournament._id}`}>
                                        <strong>{tournament.name}</strong>
                                    </Link>
                                </Col>
                                <Col className="text-right">
                                        <Button variant="link" onClick={ handleDelete }>
                                            <BsFillTrashFill />
                                        </Button>
                                </Col>
                            </Row>
                        </Card.Title>
                        <Card.Subtitle as="my-3">
                                {tournament.category.name} - {tournament.season}
                        </Card.Subtitle>
                </Card.Body>
            )}
        </Card>
    )
}

export default TournamentCard
