export const MATCH_LIST_REQUEST = 'MATCH_LIST_REQUEST'
export const MATCH_LIST_SUCCESS = 'MATCH_LIST_SUCCESS'
export const MATCH_LIST_FAIL = 'MATCH_LIST_FAIL'

export const MATCH_DETAILS_REQUEST = 'MATCH_DETAILS_REQUEST'
export const MATCH_DETAILS_SUCCESS = 'MATCH_DETAILS_SUCCESS'
export const MATCH_DETAILS_FAIL = 'MATCH_DETAILS_FAIL'

export const MATCH_CREATE_REQUEST = 'MATCH_CREATE_REQUEST'
export const MATCH_CREATE_SUCCESS = 'MATCH_CREATE_SUCCESS'
export const MATCH_CREATE_FAIL = 'MATCH_CREATE_FAIL'
export const MATCH_CREATE_RESET = 'MATCH_CREATE_RESET'

export const MATCH_UPDATE_REQUEST = 'MATCH_UPDATE_REQUEST'
export const MATCH_UPDATE_SUCCESS = 'MATCH_UPDATE_SUCCESS'
export const MATCH_UPDATE_FAIL = 'MATCH_UPDATE_FAIL'
export const MATCH_UPDATE_RESET = 'MATCH_UPDATE_RESET'

export const MATCH_DELETE_REQUEST = 'MATCH_DELETE_REQUEST'
export const MATCH_DELETE_SUCCESS = 'MATCH_DELETE_SUCCESS'
export const MATCH_DELETE_FAIL = 'MATCH_DELETE_FAIL'
export const MATCH_DELETE_RESET = 'MATCH_DELETE_RESET'

export const MATCH_UNDELETE_REQUEST = 'MATCH_UNDELETE_REQUEST'
export const MATCH_UNDELETE_SUCCESS = 'MATCH_UNDELETE_SUCCESS'
export const MATCH_UNDELETE_FAIL = 'MATCH_UNDELETE_FAIL'
export const MATCH_UNDELETE_RESET = 'MATCH_UNDELETE_RESET'