export const PLACE_LIST_REQUEST = 'PLACE_LIST_REQUEST'
export const PLACE_LIST_SUCCESS = 'PLACE_LIST_SUCCESS'
export const PLACE_LIST_FAIL = 'PLACE_LIST_FAIL'

export const PLACE_DETAILS_REQUEST = 'PLACE_DETAILS_REQUEST'
export const PLACE_DETAILS_SUCCESS = 'PLACE_DETAILS_SUCCESS'
export const PLACE_DETAILS_FAIL = 'PLACE_DETAILS_FAIL'
export const PLACE_DETAILS_RESET = 'PLACE_DETAILS_RESET'