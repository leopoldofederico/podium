import { 
    INCIDENCE_LIST_REQUEST, 
    INCIDENCE_LIST_SUCCESS, 
    INCIDENCE_LIST_FAIL,
    INCIDENCE_DETAILS_REQUEST,
    INCIDENCE_DETAILS_SUCCESS, 
    INCIDENCE_DETAILS_FAIL,
    INCIDENCE_DELETE_REQUEST,
    INCIDENCE_DELETE_SUCCESS, 
    INCIDENCE_DELETE_FAIL,
    INCIDENCE_DELETE_RESET,
    INCIDENCE_UNDELETE_REQUEST,
    INCIDENCE_UNDELETE_SUCCESS, 
    INCIDENCE_UNDELETE_FAIL,
    INCIDENCE_UNDELETE_RESET,
    INCIDENCE_CREATE_REQUEST,
    INCIDENCE_CREATE_SUCCESS, 
    INCIDENCE_CREATE_FAIL,
    INCIDENCE_CREATE_RESET,
    INCIDENCE_UPDATE_REQUEST,
    INCIDENCE_UPDATE_SUCCESS, 
    INCIDENCE_UPDATE_FAIL,
    INCIDENCE_UPDATE_RESET,
} from '../constants/incidenceConstants';

export const incidenceListReducer  = (state = {incidences: []}, action) => {
    switch(action.type) {
        case INCIDENCE_LIST_REQUEST:
            return {loading: true, incidences: []};

        case INCIDENCE_LIST_SUCCESS:
            return {loading: false, incidences: action.payload};

        case INCIDENCE_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}

export const incidenceDetailsReducer = (state= {incidence:{ }}, action) => {
    switch(action.type) {
        case INCIDENCE_DETAILS_REQUEST:
            return {loading: true, ...state}
        
            case INCIDENCE_DETAILS_SUCCESS:
                return {loading: false, incidence: action.payload}

            case INCIDENCE_DETAILS_FAIL:
                return {loading: false, error: action.payload}
            
            default:
                return state
        
    }
}

export const incidenceDeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case INCIDENCE_DELETE_REQUEST:
            return {loading: true}
        
        case INCIDENCE_DELETE_SUCCESS:
            return {loading: false, success:true, idDeleted: action.idDeleted}

        case INCIDENCE_DELETE_FAIL:
            return {loading: false, error: action.payload, idDeleted: action.idDeleted}
        
        case INCIDENCE_DELETE_RESET:
            return {}
        
        default:
            return state
        
    }
}

export const incidenceUndeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case INCIDENCE_UNDELETE_REQUEST:
            return {loading: true}
        
            case INCIDENCE_UNDELETE_SUCCESS:
                return {loading: false, success:true}

            case INCIDENCE_UNDELETE_FAIL:
                return {loading: false, error: action.payload}
            
            case INCIDENCE_UNDELETE_RESET:
                return {}
            
            default:
                return state
        
    }
}

export const incidenceCreateReducer = (state= {}, action) => {
    switch(action.type) {
        case INCIDENCE_CREATE_REQUEST:
            return {loading: true}
        
            case INCIDENCE_CREATE_SUCCESS:
                return {loading: false, incidence:action.payload, success:true}

            case INCIDENCE_CREATE_FAIL:
                return {loading: false, error: action.payload}
            
            case INCIDENCE_CREATE_RESET:
                return {}

            default:
                return state
        
    }
}

export const incidenceUpdateReducer = (state= {}, action) => {
    switch(action.type) {
        case INCIDENCE_UPDATE_REQUEST:
            return {loading: true}
        
            case INCIDENCE_UPDATE_SUCCESS:
                return {loading: false, incidence:action.payload, success:true}

            case INCIDENCE_UPDATE_FAIL:
                return {loading: false, error: action.payload}
            
            case INCIDENCE_UPDATE_RESET:
                return {}

            default:
                return state
        
    }
}
