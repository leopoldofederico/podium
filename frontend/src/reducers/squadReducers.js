import { 
    SQUAD_LIST_REQUEST, 
    SQUAD_LIST_SUCCESS, 
    SQUAD_LIST_FAIL,
    SQUAD_FIXTURE_REQUEST, 
    SQUAD_FIXTURE_SUCCESS, 
    SQUAD_FIXTURE_FAIL,
    SQUAD_DETAILS_REQUEST,
    SQUAD_DETAILS_SUCCESS, 
    SQUAD_DETAILS_FAIL,
    SQUAD_DELETE_REQUEST,
    SQUAD_DELETE_SUCCESS, 
    SQUAD_DELETE_FAIL,
    SQUAD_DELETE_RESET,
    SQUAD_UNDELETE_REQUEST,
    SQUAD_UNDELETE_SUCCESS, 
    SQUAD_UNDELETE_FAIL,
    SQUAD_UNDELETE_RESET,
    SQUAD_CREATE_REQUEST,
    SQUAD_CREATE_SUCCESS, 
    SQUAD_CREATE_FAIL,
    SQUAD_CREATE_RESET,
    SQUAD_UPDATE_REQUEST,
    SQUAD_UPDATE_SUCCESS, 
    SQUAD_UPDATE_FAIL,
    SQUAD_UPDATE_RESET,
} from '../constants/squadConstants';

export const squadListReducer  = (state = {squads: []}, action) => {
    switch(action.type) {
        case SQUAD_LIST_REQUEST:
            return {loading: true, squads: []};

        case SQUAD_LIST_SUCCESS:
            return {loading: false, squads: action.payload};

        case SQUAD_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}

export const squadFixtureReducer  = (state = {squads: []}, action) => {
    switch(action.type) {
        case SQUAD_FIXTURE_REQUEST:
            return {loading: true, squads: []};

        case SQUAD_FIXTURE_SUCCESS:
            return {loading: false, matches: action.payload};

        case SQUAD_FIXTURE_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}
export const squadDetailsReducer = (state= {squad:{ }}, action) => {
    switch(action.type) {
        case SQUAD_DETAILS_REQUEST:
            return {loading: true, ...state}
        
            case SQUAD_DETAILS_SUCCESS:
                return {loading: false, squad: action.payload}

            case SQUAD_DETAILS_FAIL:
                return {loading: false, error: action.payload}
            
            default:
                return state
        
    }
}

export const squadDeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case SQUAD_DELETE_REQUEST:
            return {loading: true}
        
        case SQUAD_DELETE_SUCCESS:
            return {loading: false, success:true, chupame: true, idDeleted: action.idDeleted}

        case SQUAD_DELETE_FAIL:
            return {loading: false, error: action.payload, idDeleted: action.idDeleted}
        
        case SQUAD_DELETE_RESET:
            return {}
        
        default:
            return state
        
    }
}

export const squadUndeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case SQUAD_UNDELETE_REQUEST:
            return {loading: true}
        
            case SQUAD_UNDELETE_SUCCESS:
                return {loading: false, success:true}

            case SQUAD_UNDELETE_FAIL:
                return {loading: false, error: action.payload}
            
            case SQUAD_UNDELETE_RESET:
                return {}
            
            default:
                return state
        
    }
}

export const squadCreateReducer = (state= {}, action) => {
    switch(action.type) {
        case SQUAD_CREATE_REQUEST:
            return {loading: true}
        
            case SQUAD_CREATE_SUCCESS:
                return {loading: false, squad:action.payload, success:true}

            case SQUAD_CREATE_FAIL:
                return {loading: false, error: action.payload}
            
            case SQUAD_CREATE_RESET:
                return {}

            default:
                return state
        
    }
}

export const squadUpdateReducer = (state= {}, action) => {
    switch(action.type) {
        case SQUAD_UPDATE_REQUEST:
            return {loading: true}
        
            case SQUAD_UPDATE_SUCCESS:
                return {loading: false, squad:action.payload, success:true}

            case SQUAD_UPDATE_FAIL:
                return {loading: false, error: action.payload}
            
            case SQUAD_UPDATE_RESET:
                return {}

            default:
                return state
        
    }
}
