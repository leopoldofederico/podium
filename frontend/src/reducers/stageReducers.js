import { 
    STAGE_LIST_REQUEST, 
    STAGE_LIST_SUCCESS, 
    STAGE_LIST_FAIL,
    STAGE_DETAILS_REQUEST,
    STAGE_DETAILS_SUCCESS, 
    STAGE_DETAILS_FAIL,
    STAGE_DELETE_REQUEST,
    STAGE_DELETE_SUCCESS, 
    STAGE_DELETE_FAIL,
    STAGE_DELETE_RESET,
    STAGE_UNDELETE_REQUEST,
    STAGE_UNDELETE_SUCCESS, 
    STAGE_UNDELETE_FAIL,
    STAGE_UNDELETE_RESET,
    STAGE_CREATE_REQUEST,
    STAGE_CREATE_SUCCESS, 
    STAGE_CREATE_FAIL,
    STAGE_CREATE_RESET,
    STAGE_UPDATE_REQUEST,
    STAGE_UPDATE_SUCCESS, 
    STAGE_UPDATE_FAIL,
    STAGE_UPDATE_RESET,
} from '../constants/stageConstants';

export const stageListReducer  = (state = {stages: []}, action) => {
    switch(action.type) {
        case STAGE_LIST_REQUEST:
            return {loading: true, stages: []};

        case STAGE_LIST_SUCCESS:
            return {loading: false, stages: action.payload};

        case STAGE_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}

export const stageDetailsReducer = (state= {stage:{ }}, action) => {
    switch(action.type) {
        case STAGE_DETAILS_REQUEST:
            return {loading: true, ...state}
        
            case STAGE_DETAILS_SUCCESS:
                return {loading: false, stage: action.payload}

            case STAGE_DETAILS_FAIL:
                return {loading: false, error: action.payload}
            
            default:
                return state
        
    }
}

export const stageDeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case STAGE_DELETE_REQUEST:
            return {loading: true}
        
        case STAGE_DELETE_SUCCESS:
            return {loading: false, success:true, chupame: true, idDeleted: action.idDeleted}

        case STAGE_DELETE_FAIL:
            return {loading: false, error: action.payload, idDeleted: action.idDeleted}
        
        case STAGE_DELETE_RESET:
            return {}
        
        default:
            return state
        
    }
}

export const stageUndeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case STAGE_UNDELETE_REQUEST:
            return {loading: true}
        
            case STAGE_UNDELETE_SUCCESS:
                return {loading: false, success:true}

            case STAGE_UNDELETE_FAIL:
                return {loading: false, error: action.payload}
            
            case STAGE_UNDELETE_RESET:
                return {}
            
            default:
                return state
        
    }
}

export const stageCreateReducer = (state= {}, action) => {
    switch(action.type) {
        case STAGE_CREATE_REQUEST:
            return {loading: true}
        
            case STAGE_CREATE_SUCCESS:
                return {loading: false, stage:action.payload, success:true}

            case STAGE_CREATE_FAIL:
                return {loading: false, error: action.payload}
            
            case STAGE_CREATE_RESET:
                return {}

            default:
                return state
        
    }
}

export const stageUpdateReducer = (state= {}, action) => {
    switch(action.type) {
        case STAGE_UPDATE_REQUEST:
            return {loading: true}
        
            case STAGE_UPDATE_SUCCESS:
                return {loading: false, stage:action.payload, success:true}

            case STAGE_UPDATE_FAIL:
                return {loading: false, error: action.payload}
            
            case STAGE_UPDATE_RESET:
                return {}

            default:
                return state
        
    }
}
