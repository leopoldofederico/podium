import { 
    STANDINGS_LIST_REQUEST, 
    STANDINGS_LIST_SUCCESS, 
    STANDINGS_LIST_FAIL,
} from '../constants/standingConstants';

export const standingsListReducer  = (state = {standings: []}, action) => {
    switch(action.type) {
        case STANDINGS_LIST_REQUEST:
            return {loading: true, standings: null};

        case STANDINGS_LIST_SUCCESS: {
            return {loading: false, standings: action.payload}; }

        case STANDINGS_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}