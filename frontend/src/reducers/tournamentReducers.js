import { 
    TOURNAMENT_LIST_REQUEST, 
    TOURNAMENT_LIST_SUCCESS, 
    TOURNAMENT_LIST_FAIL,
    AVAILABLETEAMS_LIST_REQUEST, 
    AVAILABLETEAMS_LIST_SUCCESS, 
    AVAILABLETEAMS_LIST_FAIL,
    TOURNAMENT_DETAILS_REQUEST,
    TOURNAMENT_DETAILS_SUCCESS, 
    TOURNAMENT_DETAILS_FAIL,
    TOURNAMENT_DELETE_REQUEST,
    TOURNAMENT_DELETE_SUCCESS, 
    TOURNAMENT_DELETE_FAIL,
    TOURNAMENT_DELETE_RESET,
    TOURNAMENT_UNDELETE_REQUEST,
    TOURNAMENT_UNDELETE_SUCCESS, 
    TOURNAMENT_UNDELETE_FAIL,
    TOURNAMENT_UNDELETE_RESET,
    TOURNAMENT_CREATE_REQUEST,
    TOURNAMENT_CREATE_SUCCESS, 
    TOURNAMENT_CREATE_FAIL,
    TOURNAMENT_CREATE_RESET,
    TOURNAMENT_UPDATE_REQUEST,
    TOURNAMENT_UPDATE_SUCCESS, 
    TOURNAMENT_UPDATE_FAIL,
    TOURNAMENT_UPDATE_RESET,
    TOURNAMENT_LOGO_REQUEST,
    TOURNAMENT_LOGO_SUCCESS,
    TOURNAMENT_LOGO_FAIL,
} from '../constants/tournamentConstants';

export const tournamentListReducer  = (state = {tournaments: []}, action) => {
    switch(action.type) {
        case TOURNAMENT_LIST_REQUEST:
            return {loading: true, tournaments: []};

        case TOURNAMENT_LIST_SUCCESS:
            return {loading: false, tournaments: action.payload};

        case TOURNAMENT_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}

export const availableTeamsListReducer  = (state = {tournaments: []}, action) => {
    switch(action.type) {
        case AVAILABLETEAMS_LIST_REQUEST:
            return {loading: true, teams: []};

        case AVAILABLETEAMS_LIST_SUCCESS:
            return {loading: false, teams: action.payload};

        case AVAILABLETEAMS_LIST_FAIL:
            return {loading: false, error: action.payload};
        
        default:
            return state;
    }
}

export const tournamentDetailsReducer = (state= {tournament:{ }}, action) => {
    switch(action.
        type) {
        case TOURNAMENT_DETAILS_REQUEST:
            return {loading: true, ...state}
        
            case TOURNAMENT_DETAILS_SUCCESS:
                return {loading: false, tournament: action.payload}

            case TOURNAMENT_DETAILS_FAIL:
                return {loading: false, error: action.payload}
            
            default:
                return state
        
    }
}

export const tournamentDeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case TOURNAMENT_DELETE_REQUEST:
            return {loading: true}
        
            case TOURNAMENT_DELETE_SUCCESS:
                return {loading: false, success: true, idDeleted: action.idDeleted}

            case TOURNAMENT_DELETE_FAIL:
                return {loading: false, error: action.payload}
            
            case TOURNAMENT_DELETE_RESET:
                return {}
            
            default:
                return state
        
    }
}

export const tournamentUndeleteReducer = (state= {}, action) => {
    switch(action.type) {
        case TOURNAMENT_UNDELETE_REQUEST:
            return {loading: true}
        
            case TOURNAMENT_UNDELETE_SUCCESS:
                return {loading: false, success:true}

            case TOURNAMENT_UNDELETE_FAIL:
                return {loading: false, error: action.payload}
            
            case TOURNAMENT_UNDELETE_RESET:
                return {}
            
            default:
                return state
        
    }
}

export const tournamentCreateReducer = (state= {}, action) => {
    switch(action.type) {
        case TOURNAMENT_CREATE_REQUEST:
            return {loading: true}
        
            case TOURNAMENT_CREATE_SUCCESS:
                return {loading: false, tournament:action.payload, success:true}

            case TOURNAMENT_CREATE_FAIL:
                return {loading: false, error: action.payload}
            
            case TOURNAMENT_CREATE_RESET:
                return {}

            default:
                return state
        
    }
}

export const tournamentUpdateReducer = (state= {}, action) => {
    switch(action.type) {
        case TOURNAMENT_UPDATE_REQUEST:
            return {loading: true}
        
            case TOURNAMENT_UPDATE_SUCCESS:
                return {loading: false, tournament:action.payload, success:true}

            case TOURNAMENT_UPDATE_FAIL:
                return {loading: false, error: action.payload}
            
            case TOURNAMENT_UPDATE_RESET:
                return {}

            default:
                return state
        
    }
}

export const tournamentLogoReducer = (state= {}, action) => {
    switch(action.type) {
        case TOURNAMENT_LOGO_REQUEST:
            return {loading: true}
        
            case TOURNAMENT_LOGO_SUCCESS:
                return {loading: false, tournament:action.payload, success:true}

            case TOURNAMENT_LOGO_FAIL:
                return {loading: false, error: action.payload}

            default:
                return state
        
    }
}
