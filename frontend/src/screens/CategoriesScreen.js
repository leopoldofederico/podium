import React, { useState, useEffect, useCallback } from 'react'
import { Table, Button, Row, Container, Modal, Form } from 'react-bootstrap'
import { useTable, useSortBy } from "react-table"
import { useDispatch, useSelector } from 'react-redux';
import { 
    listCategories, 
    updateCategory, 
    deleteCategory, 
    createCategory,
    undeleteCategory
} from '../actions/categoryActions';
import Message from '../components/Message'
import { BsFillTrashFill, BsPlusCircleFill } from "react-icons/bs";
import { Trans, useTranslation } from 'react-i18next';
import { Formik, Form as FormFormik } from 'formik';
import NameInput from '../components/NameInput'
import * as Yup from 'yup';

function CategoriesScreen() {
    const [data, setData] = useState([]);
    const [showCreate, setShowCreate] = useState(false);
    const [name, setName] = useState('');
    const [deleted, setDeleted] = useState(false);
    const [undeleted, setUndeleted] = useState(null);

    const dispatch = useDispatch();

    const { t } = useTranslation();

    const [deleteItem, setDeleteItem] = useState(null);

    const categoryList = useSelector(state => state.categoryList);
    const { error, loading, categories } = categoryList;
    
    const categoryUpdate = useSelector(state => state.categoryUpdate);
    const { error: errorUpdate, category: categoryUpdated, success } = categoryUpdate;

    const categoryDelete = useSelector(state => state.categoryDelete);
    const { error: errorDelete, category: categoryDeleted, success: successDelete } = categoryDelete;

    const categoryUndelete = useSelector(state => state.categoryUndelete);
    const { error: errorUndelete, category: categoryUndeleted, success: successUndelete } = categoryUndelete;

    const categoryCreate = useSelector(state => state.categoryCreate);
    const { error: errorCreate, category: categoryCreated, success: successCreate } = categoryCreate;
    
    const updateMyData = (index, id, value) => {
        dispatch(updateCategory({ 
            _id: data[index]._id, 
            name: value, 
         }))
        return index;
    }

    const EditableCell = ({
        value: initialValue,
        row: { index },
        column: { id },
        updateMyData, // This is a custom function that we supplied to our table instance
      }) => {
        // We need to keep and update the state of the cell normally
        const [value, setValue] = React.useState(initialValue)
    
        const onChange = e => {
        setValue(e.target.value)
        }
    
        // We'll only update the external data when the input is blurred
        const onBlur = () => {
            if (value != initialValue) {
                updateMyData(index, id, value);
            }
        }
    
        // If the initialValue is changed external, sync it up with our state
        React.useEffect(() => {
            setValue(initialValue)
        }, [initialValue])
    
        return <input className="m-1 p-0 border border-0" style={{ backgroundColor: "transparent"}} value={value} onChange={onChange} onBlur={onBlur} />
    }

    const columns = React.useMemo(
        () => [
            {
                Header: ' ',
                accesor: 'action',
                Cell: (props) => {
                    const row = props.row.values;
                    return (
                        <Button variant="link" onClick={ () => handleDelete(row) }>
                            <BsFillTrashFill />
                        </Button>
                    );
                  },
            },
        
            {
                Header: 'ID',
                accessor: '_id',
            },
            {
                Header: t('category.name'),
                accessor: 'name',
                Cell: EditableCell,
            }
        ],
        []
    )

    useEffect(() => {
        dispatch(listCategories());
    }, [dispatch]);
    
    useEffect(() => {
        setData(categories);
    }, [categories])

    useEffect(() => {
        if (categoryCreated) {
            let newCategories = [...categories];
            newCategories.push(categoryCreated);
            setData(newCategories);        
        }
    }, [categoryCreated])
    
    useEffect(() => {
        let newCategories = [...categories];
        let i = categories.findIndex(e => e._id == deleteItem )
        newCategories.splice(i, 1);
        setDeleted(true);
        setData(newCategories);    
    }, [deleteItem])

    useEffect(() => {
        if (undeleted) {
            let newCategories = [...categories];
            newCategories.push(undeleted);
            // setData(newCategories);
        }
    }, [undeleted])

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
        columns,
        data,
        // defaultColumn,
        updateMyData
    }, useSortBy)

    
    const handleDelete = (x) => {
        dispatch(deleteCategory(x._id));
        setDeleteItem(x._id);    
    }

    const handleCreate = () => {
        setShowCreate(true);
    }

    const handleClose = () => {
        setShowCreate(false);
    }

    const handleNewCategory = x => {
        dispatch(createCategory({ 
            name: x.name, 
        }));
        handleClose();
    }

    const handleUndelete = () => {
        dispatch(undeleteCategory(deleteItem));
        setDeleted(false);
        setUndeleted(deleteItem);
    }

    return (
        <div>
            { success && 
                <Message variant="success">
                    {t("category.updated", { category: categoryUpdated })}
                </Message> }
            { deleted && successDelete && 
                <Message variant="success">
                    {t("category.deleted", { category: deleteItem })}
                    <Button variant="Link" onClick={ handleUndelete }>
                        <Trans i18nKey="main.undo_question" />
                    </Button>
                </Message> 
            }
            { successCreate && 
                <Message variant="success">
                     {t("category.created", { category: categoryCreated })}
                </Message> }
            { error && <Message variant="danger">{error}</Message>}
            { errorUpdate && <Message variant="danger">{errorUpdate.name}</Message>}
            { errorDelete && <Message variant="danger">{errorDelete}</Message>}
            { errorCreate && <Message variant="danger">{errorCreate}</Message>}
            <Container>
                <Row>
                    <Button onClick={ handleCreate } className="my-1">
                        <Row className="mx-1 align-items-center">
                            <BsPlusCircleFill className="mr-1"/>
                            <Trans i18nKey="category.create">Create tournament</Trans>
                        </Row>
                    </Button>
                </Row>
            </Container>
            <Modal show={showCreate} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <Trans i18nKey="category.new">New category</Trans>
                    </Modal.Title>
                </Modal.Header>
                <Formik
                        enableReinitialize={true}
                        initialValues={{
                            name: name,
                        }}
                        validationSchema={Yup.object({
                            name: Yup.string().required(t('validate.required')),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            handleNewCategory(values);
                        }}
                    >
                    <FormFormik>
                        <Modal.Body>                            
                            <NameInput
                                label={t('category.name')}
                                name="name"
                                placeholder={t('category.name_placeholder')}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                <Trans i18nKey="home.close">Close</Trans>
                            </Button>
                            <Button variant="primary" type="submit">
                                <Trans i18nKey="home.save">Save</Trans>
                            </Button>
                        </Modal.Footer>
                    </FormFormik>
                </Formik>
                    

            </Modal>
            
            <Table striped bordered hover {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
            
                <tbody>
                    {rows.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td className="m-0 p-0" {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </td>
                                    )
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>
    )
}

export default CategoriesScreen
