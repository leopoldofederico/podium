import React, { useEffect, useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Trans, useTranslation } from 'react-i18next'
import { INCIDENCE_CREATE_RESET } from '../constants/incidenceConstants'
import Message from '../components/Message';
import { Formik, Form as FormFormik } from 'formik'
import { Container, Row, Button } from 'react-bootstrap'
import * as Yup from 'yup'
import { createIncidence } from '../actions/incidenceActions'
import DateTimeInput from '../components/DateTimeInput';
import SelectInput from '../components/SelectInput';
import TextAreaInput from '../components/TextAreaInput';
import NumericInput from '../components/NumericInput';

function IncidenceCreateScreen( { tournament, history, ...props } ) {
    const { t } = useTranslation();

    const dispatch = useDispatch();
    const incidenceCreate = useSelector(state => state.incidenceCreate);
    const { error, success } = incidenceCreate;

    const [selectedStage, setSelectedStage] = useState(null);
    const [teams, setTeams] = useState([]);

    const [team1, setSquad] = useState(null)

    const handleChangeStage = value => {
        // console.log(value)
        let newTeams = [];
        value.groups.forEach( group => {
            newTeams.push(...group.competitors)
        })
        // console.log(newTeams);
        setTeams(newTeams)
        
    }

    useEffect(() => {
        if (selectedStage) {
            let newTeams = [];
            selectedStage.groups.forEach( group => {
                newTeams.push(...group.competitors)
            })
            setTeams(newTeams)
        }
    }, [selectedStage])

    useEffect(() => {
        if (success){ 
            dispatch( { type: INCIDENCE_CREATE_RESET });
            props.handleClose()
        }
        
    }, [dispatch, history, success] )

    return (
        <Container>
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            <h1><Trans i18nKey="incidence.create">Create incidence</Trans></h1>
                <Formik
                    initialValues={{
                        stage: '',
                        squad: '',
                        points: '',
                        date: null,
                        note: '',
                    }}
                    validationSchema={Yup.object({
                        // stage: Yup.string().required(),
                        squad: Yup.string().required(),
                        points: Yup.number().required(),
                        date: Yup.date().required(),
                        note: Yup.string(),
                    })}
                    onSubmit={(values, { setSubmitting }) => {
                        dispatch(createIncidence({ 
                            stage_id: values.stage,
                            squad_id: values.squad,
                            points: values.points,
                            date: values.date,
                            note: values.note,
                        }))
                    }}
                    
                >
                    <FormFormik>
                        <SelectInput
                            label={t("stage.title")}
                            name="stage"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { tournament.stages }
                            onSelect = { handleChangeStage }
                        >
                        </SelectInput>
                        <SelectInput
                            label={t('incidence.squad')}
                            name="squad"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { teams }
                            onSelect = { value => setSquad(value) }
                            empty = {true}
                        />
                        <NumericInput 
                            label={t('incidence.points')}
                            name="points"
                            placeholder={t('incidence.points_placeholder')}
                        />
                        <DateTimeInput
                            name="date"
                            label={t('incidence.date')}
                        />
                        <TextAreaInput
                            label={t('incidence.note')}
                            name="note"
                            placeholder={t('incidence.note_placeholder')}
                            rows={3}
                        />
                        <Container>
                            <Row className="justify-content-md-center my-1">
                                <Button variant="secondary" onClick={ props.handleClose }>
                                    <Trans i18nKey="home.cancel"></Trans>
                                </Button>
                                <Button
                                    size="lg"
                                    variant="primary"
                                    type="submit"
                                    className="mx-1"
                                >
                                    <Trans i18nKey="home.save">Save</Trans>
                                </Button>
                            </Row>
                        </Container>
                    </FormFormik>
                </Formik>
        </Container>
    )
}

export default IncidenceCreateScreen
