import React, { useEffect, useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Trans, useTranslation } from 'react-i18next'
import { INCIDENCE_UPDATE_RESET } from '../constants/incidenceConstants'
import Message from '../components/Message';
import { Formik, Form as FormFormik } from 'formik'
import { Container, Row, Button, Tooltip, OverlayTrigger } from 'react-bootstrap'
import * as Yup from 'yup'
import { updateIncidence } from '../actions/incidenceActions'
import DateTimeInput from '../components/DateTimeInput';
import TextAreaInput from '../components/TextAreaInput';
import NumericInput from '../components/NumericInput';
import { listTournamentsDetails } from '../actions/tournamentActions';

function IncidenceEditScreen( { incidence, history, ...props } ) {
    const { t } = useTranslation();

    const dispatch = useDispatch();
    const incidenceUpdate = useSelector(state => state.incidenceUpdate);
    const { error, success } = incidenceUpdate;

    const [teams, setTeams] = useState([]);

    const [squad, setSquad] = useState(null)
    const [points, setPoints] = useState("")
    const [date, setDate] = useState("")
    const [note, setNote] = useState("")

    useEffect(() => {
        if (success){ 
            dispatch( { type: INCIDENCE_UPDATE_RESET });
            dispatch(listTournamentsDetails(incidence.tournament))
            props.handleClose()
        }
        
    }, [dispatch, history, success] )

    useEffect(() => {
        if (incidence) {
            // setSquad(incidence.squad)
            setPoints(incidence.points)
            setDate(new Date(incidence.date))
            console.log(incidence)
            setNote(incidence.note)
        }
    }, [incidence])

    return (
        <Container>
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            <h1><Trans i18nKey="incidence.edit">Edit incidence</Trans></h1>
                <Formik
                    initialValues={{
                        // stage: stage,
                        // squad: squad,
                        points: points,
                        date: date,
                        note: note,
                    }}
                    enableReinitialize={true}
                    validationSchema={Yup.object({
                        // stage: Yup.string().required(),
                        // squad: Yup.string().required(),
                        points: Yup.number().required(),
                        date: Yup.date().required(),
                        note: Yup.string(),
                    })}
                    onSubmit={(values, { setSubmitting }) => {
                        dispatch(updateIncidence({ 
                            _id: incidence._id,
                            // stage_id: values.stage,
                            // squad_id: values.squad,
                            points: values.points,
                            date: values.date,
                            note: values.note,
                        }))
                    }}
                    
                >
                    <FormFormik>
                        {/* <SelectInput
                            label={t("stage.title")}
                            name="stage"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { tournament.stages }
                            onSelect = { handleChangeStage }
                        >
                        </SelectInput> */}
                        {/* <SelectInput
                            label={t('incidence.squad')}
                            name="squad"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { teams }
                            onSelect = { value => setSquad(value) }
                            empty = {true}
                        /> */}
                        
                        <NumericInput 
                            label={t('incidence.points')}
                            name="points"
                            placeholder={t('incidence.points_placeholder')}
                        />
                        <DateTimeInput
                            name="date"
                            label={t('incidence.date')}
                        />
                        <TextAreaInput
                            label={t('incidence.note')}
                            name="note"
                            placeholder={t('incidence.note_placeholder')}
                            rows={3}
                        />
                        <Container>
                            <Row className="justify-content-md-center my-1">
                                <Button variant="secondary" onClick={ props.handleClose }>
                                    <Trans i18nKey="home.cancel"></Trans>
                                </Button>
                                <Button
                                    size="lg"
                                    variant="primary"
                                    type="submit"
                                    className="mx-1"
                                >
                                    <Trans i18nKey="home.save">Save</Trans>
                                </Button>
                            </Row>
                        </Container>
                    </FormFormik>
                </Formik>
        </Container>
    )
}

export default IncidenceEditScreen
