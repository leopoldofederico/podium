import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Col, Row, Container } from 'react-bootstrap'
import { BsPlusCircleFill } from 'react-icons/bs'
import { Trans, useTranslation } from 'react-i18next'
import { listIncidences } from '../actions/incidenceActions'
import Message from '../components/Message'
import IncidencesTable from '../components/IncidencesTable'
import IncidenceCreateScreen from './IncidenceCreateScreen'
import { listTournamentsDetails } from '../actions/tournamentActions'

function IncidencesTabScreen( { tournament, ...props } ) {
    const [showCreate, setShowCreate] = useState(false)
    const { t } = useTranslation()
    const [stage, setStage] = useState(null);
    const [group, setGroup] = useState(null);

    const [successMessage, setSuccessMessage] = useState(null)
    
    const incidenceCreate = useSelector(state => state.incidenceCreate);
    const { error, incidence, success } = incidenceCreate;


    const dispatch = useDispatch()
    
    const handleClose = () => {
        setShowCreate(false)
    }
    
    useEffect(() => {
        if (incidence) {
            setSuccessMessage( incidence._id ? 1: Number(incidence.detail.split(" ")[0]) )
            dispatch(listTournamentsDetails(tournament._id))
        }
    }, [incidence])

    return (
        <Container>
            <Row>
                { error && <Message variant="danger">{error}</Message> }
                { successMessage && <Message variant="success">
                    { t('incidence.created', {count: successMessage}) }
                </Message>}
            </Row>

            <Row>
                <Col md={3}>
                    <Row>
                        <Button type="primary" onClick={ () => setShowCreate(true) }>
                            <Row className="mx-1 align-items-center">
                                <BsPlusCircleFill className="mr-1"/>
                                <Trans i18nKey="incidence.create">Create incidence</Trans>
                            </Row>
                        </Button>
                    </Row>
                    <Row>
                        { showCreate && <IncidenceCreateScreen tournament={tournament} handleClose={ handleClose } />}
                    </Row>
                </Col>
                             
                <Col md={9}>
                    <Row>
                        <IncidencesTable tournament={tournament}/>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}

export default IncidencesTabScreen
