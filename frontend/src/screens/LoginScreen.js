import React, { useState, useEffect } from 'react'
import { Link, useNavigate, useLocation } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Trans, useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { login } from '../actions/userActions'
import FormContainer from '../components/FormContainer'


function LoginScreen() {
    let navigate = useNavigate();
    let location = useLocation();

    let from = location.state?.from?.pathname || "/";

    const { t } = useTranslation();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    
    // const redirect = location && location.search ? location.search.split('=')[1] : '/'

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(login(email, password))
    }

    const userLogin = useSelector(state => state.userLogin);
    
    const { error, loading, userInfo } = userLogin;

    useEffect(() => {
        if (userInfo) {
            navigate(from, { replace: true });
        }
    }, [userInfo, navigate])

    return (
        <FormContainer>
            <h1><Trans i18nKey="home.login"></Trans></h1>
            {error && <Message variant="danger">{error}</Message>}
            {loading && <Loader></Loader>}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='email'>
                    <Form.Label>
                        <Trans i18nKey="user.email">Email</Trans>
                    </Form.Label>

                    <Form.Control
                        type='email'
                        placeholder={t('user.email_placeholder')}
                        value={email}
                        onChange={(e)=>setEmail(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Form.Group controlId='password'>
                    <Form.Label>
                        <Trans i18nKey="user.password">Password</Trans>
                    </Form.Label>

                    <Form.Control
                        type='password'
                        placeholder={t('user.password_placeholder')}
                        value={password}
                        onChange={(e)=>setPassword(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Button type="submit" variant="primary">
                    <Trans i18nKey="login.login">Login</Trans>
                </Button>
            </Form>

            <Row className='py-3'>
                <Col>
                    <div>
                        <Trans i18nKey="login.new_here">New in here?</Trans>
                        <Link className="ml-1" to={navigate ? `/register?redirect=${navigate}` : '/register'}>
                            <Trans i18nKey="login.register">Register</Trans>
                        </Link>
                    </div>
                </Col>

            </Row>
        </FormContainer>
    )
}

export default LoginScreen
