import React, { useEffect, useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Trans, useTranslation } from 'react-i18next'
import { MATCH_CREATE_RESET } from '../constants/matchConstants'
import Message from '../components/Message';
import { Formik, Form as FormFormik } from 'formik'
import { Container, Row, Button, Tooltip, OverlayTrigger } from 'react-bootstrap'
import * as Yup from 'yup'
import { createMatch } from '../actions/matchActions'
import DateTimeInput from '../components/DateTimeInput';
import NameInput from '../components/NameInput';
import SelectInput from '../components/SelectInput';

function MatchCreateScreen( { tournament, history, ...props } ) {
    const { t } = useTranslation();

    const formRef = useRef()

    const dispatch = useDispatch();
    const matchCreate = useSelector(state => state.matchCreate);
    const { error, success } = matchCreate;

    const [selectedStage, setSelectedStage] = useState(null);
    const [teams, setTeams] = useState([]);

    const [team1, setTeam1] = useState(null)
    const [team2, setTeam2] = useState(null)

    const handleChangeStage = value => {
        // console.log(value)
        let newTeams = [];
        value.groups.forEach( group => {
            newTeams.push(...group.competitors)
        })
        // console.log(newTeams);
        setTeams(newTeams)
        
        if (team1 && !newTeams.find(x=> x._id==team1._id)) {
            formRef.current.setFieldValue('team1', '')
        }
        if (team2 && !newTeams.find(x=> x._id==team2._id)) {
            formRef.current.setFieldValue('team2', '')
        }
    }

    useEffect(() => {
        if (success){ 
            dispatch( { type: MATCH_CREATE_RESET });
            props.handleClose()
        }
        
    }, [dispatch, history, success] )

    useEffect(() => {
        if (selectedStage) {
            let newTeams = [];
            selectedStage.groups.forEach( group => {
                newTeams.push(...group.competitors)
            })
            setTeams(newTeams)
        }
    }, [selectedStage])

    return (
        <Container>
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            <h1><Trans i18nKey="match.create">Create match</Trans></h1>
                <Formik
                    innerRef={formRef}
                    initialValues={{
                        stage: '',
                        role1: 'home',
                        role2: 'away',
                        team1: '',
                        team2: '',
                        date: null,
                        note: '',
                        round: '',
                        series: '',
                    }}
                    validationSchema={Yup.object({
                        stage: Yup.string().required(),
                        role1: Yup.string().required(),
                        role2: Yup.string().required(),
                        team1: Yup.string().required(),
                        team2: Yup.string().required(),
                        date: Yup.date().nullable().default(null),
                        note: Yup.string(),
                        round: Yup.string(),
                        series: Yup.string(),
                    })}
                    onSubmit={(values, { setSubmitting }) => {
                        // console.log(values)
                        dispatch(createMatch({ 
                            stage_id: values.stage,
                            role1: values.role1,
                            role2: values.role2,
                            team1_id: values.team1,
                            team2_id: values.team2,
                            date: values.date,
                            note: values.note,
                            round: values.round,
                            series: values.series,
                        }))
                    }}
                    
                >
                    <FormFormik>
                        <SelectInput
                            label={t("stage.title")}
                            name="stage"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { tournament.stages }
                            onSelect = { handleChangeStage }
                        >
                        </SelectInput>
                        <SelectInput
                            label={t('match.home')}
                            name="team1"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { team2 ? teams.filter(x => x._id != team2._id) : teams }
                            onSelect = { value => setTeam1(value) }
                            empty = {true}
                        />
                        <SelectInput
                            label={t('match.away')}
                            name="team2"
                            getOptionLabel={e => e.name}
                            getOptionValue={e => e._id}
                            options = { team1 ? teams.filter(x => x._id != team1._id) : teams }
                            onSelect = { value => setTeam2(value) }
                        />
                        <DateTimeInput
                            name="date"
                            label={t('match.date')}
                        />
                        <NameInput
                            label={t('match.note')}
                            name="note"
                            placeholder={t('match.note_placeholder')}
                        />
                        <OverlayTrigger 
                            delay={{ show: 250, hide: 400 }} 
                            overlay={<Tooltip>{t('match.round_help')}</Tooltip>}
                        >
                            <NameInput
                                label={t('match.round')}
                                name="round"
                                placeholder={t('match.round_placeholder')}
                            />
                        </OverlayTrigger>
                        <OverlayTrigger 
                            delay={{ show: 250, hide: 400 }} 
                            overlay={<Tooltip>{t('match.series_help')}</Tooltip>}
                        >
                            <NameInput
                                label={t('match.series')}
                                name="series"
                                placeholder={t('match.series_placeholder')}
                            />
                        </OverlayTrigger>
                        <Container>
                            <Row className="justify-content-md-center my-1">
                                <Button variant="secondary" onClick={ props.handleClose }>
                                    <Trans i18nKey="home.cancel"></Trans>
                                </Button>
                                <Button
                                    size="lg"
                                    variant="primary"
                                    type="submit"
                                    className="mx-1"
                                >
                                    <Trans i18nKey="home.save">Save</Trans>
                                </Button>
                            </Row>
                        </Container>
                    </FormFormik>
                </Formik>
        </Container>
    )
}

export default MatchCreateScreen
