import React,{ useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Trans, useTranslation } from 'react-i18next'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { listMatchesDetails, updateMatch } from '../actions/matchActions';
import { MATCH_UPDATE_RESET } from '../constants/matchConstants';
import { Formik, Form as FormFormik, useField, useFormikContext } from 'formik';
import Message from '../components/Message'
import * as  Yup from 'yup'
import Loader from '../components/Loader'
import CheckInput from '../components/CheckInput'
import NameInput from '../components/NameInput'
import DateTimeInput from '../components/DateTimeInput'
import TextAreaInput from '../components/TextAreaInput'
import NumericInput from '../components/NumericInput'

const CheckPlayed = (props) => {
    const { values } = useFormikContext();
    const [field, meta, helpers] = useField(props);
    const { value } = meta;
    const { setValue } = helpers;

    useEffect(() => {
        if (values.played1 || values.played2 || values.score1 || values.score2) {
            // console.log(props.name + " " + value)
            if (value==undefined) {
                setValue(true)
            }
        }
    }, [values.played1, values.played2, values.score1, values.score2]);
  
    return (
        <CheckInput name={props.name} />
    )
}

const InputScore = (props) => {
    const { values } = useFormikContext();
    const [field, meta, helpers] = useField(props);
    const { value } = meta;
    const { setValue } = helpers;
    const [readOnly, setReadOnly] = useState(false);

    useEffect(() => {
        if (values.played1==null && values.played2==null) {
            setReadOnly(false)
            setValue("")
        } else if (values.played1!=null && values.played2!=null && props.stage) {
            setReadOnly(false)
            if (!values.played1 && !values.played2) {
                setValue(Math.trunc(props.stage.goals_lost_abandon))
                setReadOnly(true);
            } else if (!values.played1 || !values.played2) {
                let played = props.name=="score1" ? values.played1 : values.played2;
                
                if (played) {
                    setValue(Math.trunc(props.stage.goals_win_abandon))
                    setReadOnly(true);
                } else {
                    setValue(Math.trunc(props.stage.goals_lost_abandon))
                    setReadOnly(true);
                }
            }
        }
        
    }, [values.played1, values.played2]);
  
    return (
        <NameInput name={props.name} readOnly={readOnly}/>
    )
}

const TeamName = (props) => {
    const { values } = useFormikContext();
    const [field, meta, helpers] = useField(props);
    const { value } = meta;
    const [textDeco, setTextDeco] = useState("");

    useEffect(() => {
        if (values) {
            
            if (values.played1!=undefined && values.played2!=undefined) {
                let played = props.name=="team1" ? values.played1 : values.played2;
                
                if (played) {
                    setTextDeco("")
                } else {
                    setTextDeco("line-through")
                }
            } else {
                setTextDeco("")
            }
        }
    }, [values.played1, values.played2]);
  
    return (
        <big style={{ textDecoration: textDeco }}>
            {value && value.name}
        </big>
    )
}

const ConditionText = (props) => {
    const { t } = useTranslation();

    const { values } = useFormikContext();
    const [text, setText] = useState("");

    useEffect(() => {
        let played1 = values.played1;
        let played2 = values.played2;
        let score1 = values.score1;
        let score2 = values.score2;
        let team1 = values.team1.name;
        let team2 = values.team2.name;
        if (played1==undefined || played2==undefined) {
            setText(t('match.message_not_played'))
        } else if(!played1 && !played2) {
            setText(t('match.message_two_abandoned', { team1: team1, team2: team2 }))
        } else if(!played1 && played2) {
            setText(t('match.message_one_abandoned', { win: team2, lost: team1 }))
        } else if(played1 && !played2) {
            setText(t('match.message_one_abandoned', { win: team1, lost: team2 }))
        } else if(score1 == score2) {
            setText(t('match.message_draw', { team1: team1, team2: team2 }))
        } else if(score1 > score2) {
            setText(t('match.message_victory', { win: team1, lost: team2 }))
        } else if(score1 < score2) {
            setText(t('match.message_victory', { win: team2, lost: team1 }))
        }
    }, [values.played1, values.played2, values.score1, values.score2]);
  
    return (
        <Form.Text>{ text }</Form.Text>
    )
}

const PointsText = (props) => {
    const { t } = useTranslation();

    const { values, setFieldValue } = useFormikContext();
    const [text, setText] = useState("");

    useEffect(() => {
        if (props.stage) {
            let played1 = values.played1;
            let played2 = values.played2;
            let score1 = values.score1;
            let score2 = values.score2;
            let bonus1 = values.bonus1;
            let bonus2 = values.bonus2;
            let team1 = values.team1.name;
            let team2 = values.team2.name;
            let win = Math.trunc(props.stage.win_points);
            let lost = Math.trunc(props.stage.lost_points);
            let draw = Math.trunc(props.stage.draw_points);
            let abandon = Math.trunc(props.stage.abandon_points);
            if (played1==undefined || played2==undefined) {
                setText("")
                setFieldValue("points1", null)
                setFieldValue("points2", null)
            } else if(!played1 && !played2) {
                setText(abandon + "-" + abandon)
                setFieldValue("points1", abandon)
                setFieldValue("points2", abandon)
            } else if(!played1 && played2) {
                setText(abandon + "-" + win)
                setFieldValue("points1", abandon)
                setFieldValue("points2", win)
            } else if(played1 && !played2) {
                setText(win + "-" + abandon)
                setFieldValue("points1", win)
                setFieldValue("points2", abandon)
            } else if(score1 == score2) {
                setText(draw + "-" + draw)
                setFieldValue("points1", draw)
                setFieldValue("points2", draw)
            } else if(score1 > score2) {
                setText((win + bonus1) + "-" + (lost + bonus2))
                setFieldValue("points1", win + bonus1)
                setFieldValue("points2", lost + bonus2)
            } else if(score1 < score2) {
                setText((lost + bonus1) + "-" + (win + bonus2))
                setFieldValue("points1", lost + bonus1)
                setFieldValue("points2", win + bonus2)
            }
        }
    }, [values.played1, values.played2, values.score1, values.score2, values.bonus1, values.bonus2]);
  
    return (
        <Form.Text>{ text }</Form.Text>
    )
}

function MatchEditScreen( ) {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { id: matchId } = useParams();
    const matchDetails = useSelector(state => state.matchDetails);
    const { error, loading, match } = matchDetails;

    const matchUpdate = useSelector(state => state.matchUpdate);
    const { error: errorUpdate, loading: loadingUpdate, success:successUpdate } = matchUpdate;

    const [bonus, setBonus] = useState(false);
    const [draw, setDraw] = useState(false);
    const [role1, setRole1] = useState("home");
    const [role2, setRole2] = useState("away");
    const [team1, setTeam1] = useState("");
    const [team2, setTeam2] = useState("");
    const [played1, setPlayed1] = useState(false);
    const [played2, setPlayed2] = useState(false);
    const [score1, setScore1] = useState("");
    const [score2, setScore2] = useState("");
    const [bonus1, setBonus1] = useState(0);
    const [bonus2, setBonus2] = useState(0);
    const [points1, setPoints1] = useState("");
    const [points2, setPoints2] = useState("");
    const [date, setDate] = useState("");
    const [note, setNote] = useState("");
    const [fixtureNote, setFixtureNote] = useState("");
    const [series, setSeries] = useState("");
    const [round, setRound] = useState("");
    const [stage, setStage] = useState(null);

    const [condition, setCondition] = useState("");

    useEffect(() => {
        if (successUpdate){ 
            dispatch( { type: MATCH_UPDATE_RESET });
        } else {
            if (!error)  {
                if ( match._id !== Number(matchId)) {
                    dispatch(listMatchesDetails(matchId));
                } else {
                    setBonus(match.bonus)
                    setDraw(match.draw)
                    setRole1(match.role1)
                    setRole2(match.role2)
                    setTeam1(match.team1)
                    setTeam2(match.team2)
                    setPlayed1(match.team1_played==null ? undefined: match.team1_played)
                    setPlayed2(match.team2_played==null ? undefined: match.team2_played)
                    setScore1(match.team1_score==null ? undefined: match.team1_score)
                    setScore2(match.team2_score==null ? undefined: match.team2_score)
                    setBonus1(match.team1_bonus==null ? 0 : match.team1_bonus)
                    setBonus2(match.team2_bonus==null ? 0 : match.team2_bonus)
                    setPoints1(match.team1_points==null ? undefined: match.team1_points)
                    setPoints2(match.team2_points==null ? undefined: match.team2_points)
                    setDate(match.date==null ? undefined : new Date(match.date))
                    setNote(match.note==null ? undefined : match.note)
                    setRound(match.round==null ? undefined: match.round)
                    setSeries(match.series==null ? undefined: match.series)
                    setStage(match.stage)
                    setFixtureNote(match.fixture_note==null ? undefined : match.fixtureNote)
                }
            }
        }
        
    }, [dispatch, match, successUpdate] )
    // [dispatch, match, matchId, history, successUpdate]
    return (
        <Container>
            {stage && <Link to={`/tournament/${stage.tournament_id}?tab=matches`}>
                <Trans i18nKey="home.back">Go back</Trans>
            </Link>}
            <h1><Trans i18nKey="match.edit">Edit Match</Trans></h1>
            {loadingUpdate && <Loader />}
            {errorUpdate  && <Message variant="danger">{errorUpdate}</Message>}
            { successUpdate ? (
                navigate(-1)
            ) : ("") 
            }   
            { loading ? (
                <Loader />
            ) : (
                error ? (
                    <Message variant="danger">{ error }</Message>
                ) : (
                    <Formik
                        enableReinitialize={true}
                        initialValues={{
                            draw: draw,
                            role1: role1,
                            role2: role2,
                            team1: team1,
                            team2: team2,
                            played1: played1,
                            played2: played2,
                            score1: score1,
                            score2: score2,
                            bonus1: bonus1,
                            bonus2: bonus2,
                            points1: points1,
                            points2: points2,
                            date: date,
                            note: note,
                            series: series,
                            round: round,
                            fixture_note: fixtureNote,
                        }}
                        validationSchema={Yup.object({
                            role1: Yup.string().required(t('validate.required')),
                            role2: Yup.string().required(t('validate.required')),
                            // team1: Yup.string().required(t('validate.required')),
                            // team2: Yup.string().required(),
                            points1: Yup.number().nullable(),
                            points2: Yup.number().nullable(),
                            bonus1: Yup.number().nullable(),
                            bonus2: Yup.number().nullable(),
                            score1: Yup.number().test(
                                    'required', 
                                    'score is required when match was played', 
                                    function(item) {
                                        return item != undefined || this.parent.played1 == null
                                    }
                                ),
                            score2: Yup.number().test(
                                    'required',
                                    'score is required when match was played', 
                                    function(item) {
                                        return item != undefined || this.parent.played2 == null
                                    }
                                )
                                .test('notdraw', 
                                    'Draw is not possible on this tournament',
                                    function(item) {
                                        return this.parent.draw || item != this.parent.score1 || item != undefined || this.parent.played2 == null
                                    }   
                                ),
                            played1: Yup.boolean().nullable(),
                            played2: Yup.boolean().nullable(),
                            date: Yup.date().nullable(),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            // alert(values.team1._id)
                            dispatch(updateMatch({
                                _id: matchId,
                                // role1: values.role1,
                                // role2: values.role2,
                                // team1: values.team1._id,
                                // team2: values.team2._id,
                                team1_points: values.points1,
                                team2_points: values.points2,
                                team1_score: values.score1,
                                team2_score: values.score2,
                                team1_bonus: values.bonus1,
                                team2_bonus: values.bonus2,
                                team1_played: values.played1,
                                team2_played: values.played2,
                                date: values.date,
                                note: values.note,
                                fixture_note: values.fixture_note,
                                round: values.round,
                                series: values.series,
                            }))
                        }}
                    >
                        <FormFormik className="mt-3">

                            <Col md={8}>
                                <Row>
                                    <DateTimeInput name="date" label={t("match.date")} />
                                </Row>
                                <Row>
                                    <NameInput 
                                        name="round" 
                                        label={t("match.round")}
                                        placeholder={t("match.round_placeholder")}
                                        className="mx-1" 
                                    />
                                    <NameInput 
                                        name="series" 
                                        label={t("match.series")}
                                        placeholder={t("match.series_placeholder")} 
                                        className="mx-1" 
                                    />
                                </Row>                                
                                <Row>
                                    <Col style={{ textAlign: "left"}}>
                                        <h3>{role1.toUpperCase()}</h3>
                                    </Col>
                                    <Col style={{ textAlign: "right" }}>
                                        <h3>{role2.toUpperCase()}</h3>
                                    </Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col>
                                        <TeamName name="team1" />
                                    </Col>
                                    <Col md={1}>
                                        <CheckPlayed name="played1" />
                                    </Col>
                                    <Col md={2}>
                                        <InputScore size="lg" name="score1" stage={stage}></InputScore>
                                    </Col>
                                    <Col style={{ textAlign: "center" }} md="auto">
                                        Vs.
                                    </Col>
                                    <Col md={2}>
                                        <InputScore size="lg" name="score2" stage={stage}></InputScore>                                     
                                    </Col>
                                    <Col md={1} style={{ textAlign: "right" }}>
                                        <CheckPlayed name="played2" />
                                    </Col>
                                    <Col style={{ textAlign: "right" }}>
                                        <TeamName name="team2" />
                                    </Col>
                                </Row>
                                { bonus &&
                                    <div>
                                        <Row className="align-items-center">
                                            <NumericInput label={team1.name + " Bonus"} name="bonus1" />
                                        </Row>
                                        <Row className="align-items-center">
                                            <NumericInput label={team2.name + " Bonus"} name="bonus2" />
                                        </Row>
                                    </div>
                                }
                                <Row className="justify-content-md-center">
                                    <PointsText stage={stage} />
                                </Row>
                                <Row className="my-3">
                                    <ConditionText />
                                </Row>
                                <Row>
                                    <Col fluid="true">
                                        <NameInput
                                            name="fixture_note"
                                            row={3}
                                            label={t("match.fixture_note")}
                                            placeholder={t("match.fixture_note_placeholder")} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col fluid="true">
                                        <TextAreaInput
                                            name="note"
                                            row={3}
                                            label={t("match.note")}
                                            placeholder={t("match.note_placeholder")} />
                                    </Col>
                                </Row>
                                <Row className="justify-content-md-center">
                                    <Button type="reset" className="mx-1" variant="secondary">
                                        <Trans i18nKey="match.reset_score" />
                                    </Button>
                                    <Button type="submit" className="mx-1" variant="primary">
                                        <Trans i18nKey="home.save" />
                                    </Button>
                                </Row>
                            </Col>
                        </FormFormik>
                    </Formik>
                )
            )}
        </Container>
    )
}

export default MatchEditScreen
