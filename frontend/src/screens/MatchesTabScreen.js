import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Col, Row, Form, OverlayTrigger, Tooltip, Container } from 'react-bootstrap'
import { BsPlusCircleFill } from 'react-icons/bs'
import { Trans, useTranslation } from 'react-i18next'
import MatchCreateScreen from './MatchCreateScreen'
import Select from 'react-select'
import { Formik, Form as FormFormik } from 'formik'
import * as Yup from 'yup'
import SelectInput from '../components/SelectInput'
import NumericInput from '../components/NumericInput'
import { generateStageMatches, generateGroupMatches, listMatches } from '../actions/matchActions'
import Message from '../components/Message'
import MatchesTable from '../components/MatchesTable'

function MatchesTabScreen( { tournament, ...props } ) {
    const [showCreate, setShowCreate] = useState(false)
    const { t } = useTranslation()
    const [rounds, setRounds] = useState(1);
    const [stage, setStage] = useState(null);
    const [group, setGroup] = useState(null);
    const [teams, setTeams] = useState(0);
    const [successMessage, setSuccessMessage] = useState(null)
    
    const matchCreate = useSelector(state => state.matchCreate);
    const { error, match, success } = matchCreate;

    const dispatch = useDispatch()
    
    const handleClose = () => {
        setShowCreate(false)
    }

    const handleChangeRound = value => {
        setRounds(value)
    }

    const handleChangeStage = value => {
        setTeams(value.groups ? value.groups.map(x => 
            x.competitors.length).reduce((sum, val) => sum+val,0) : 0)
        setStage(value)
        setGroup(null)
    }
    
    const handleChangeGroup = value => {
        setTeams(value.competitors.length)
        setGroup(value)
    }

    const totalMatches = rounds => {
        return ((teams * (teams - 1)) / 2) * rounds
    }

    useEffect(() => {
        if (match) {
            setSuccessMessage( match._id ? 1: Number(match.detail.split(" ")[0]) )
            dispatch(listMatches(tournament._id))
        }
    }, [match, tournament])
    return (
        <Container>
            <Row>
                { error && <Message variant="danger">{error}</Message> }
                { successMessage && <Message variant="success">
                    { t('match.created', {count: successMessage}) }
                </Message>}
            </Row>

            <Row>
                <Col md={3}>
                    <Row>
                        <Button type="primary" onClick={ () => setShowCreate(true) }>
                            <Row className="mx-1 align-items-center">
                                <BsPlusCircleFill className="mr-1"/>
                                <Trans i18nKey="match.create">Create match</Trans>
                            </Row>
                        </Button>
                    </Row>
                    <Row>
                        { showCreate && <MatchCreateScreen tournament={tournament} handleClose={ handleClose } />}
                    </Row>
                
                    <Formik
                        initialValues={{
                            stage: {stage},
                            group: null,
                            rounds: 1.0,
                        }}
                        validationSchema={Yup.object({
                            stage: Yup.string().required(),
                            rounds: Yup.number().required().positive().integer(),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            if (group) {
                                dispatch(generateGroupMatches(values.group, values.rounds))
                            } else {
                                dispatch(generateStageMatches(values.stage, values.rounds))
                            }
                
                        }}
                    >
                        {({
                    setFieldValue,
                    setFieldTouched,
                    values,
                    errors,
                    touched,
                              }) => (
                        <FormFormik>
                            <Row className="my-1">
                                <Col>
                                    <SelectInput
                                        label={t('stage.title')}
                                        name="stage"
                                        getOptionLabel={e => e.name}
                                        getOptionValue={e => e._id}
                                        options = { tournament && tournament.stages }
                                        onSelect = { value => {
                                            handleChangeStage(value);
                                            setFieldValue('group', null)
                                        } }
                                    />
                                    { stage && !group && <Form.Text className="text-muted" >
                                        {teams + " " + t('team.title_plural').toLocaleLowerCase()}
                                    </Form.Text>}
                                </Col>
                            </Row>
                            <Row className="my-1">
                                <Col>
                                    <SelectInput
                                        label={t('group.title')}
                                        name="group"
                                        getOptionLabel={e => e.name}
                                        getOptionValue={e => e._id}
                                        options = { stage ?
                                            tournament.stages.find(x => x._id == stage._id).groups :
                                            []
                                        }
                                        onSelect = { handleChangeGroup }
                                    />
                                    { group && <Form.Text className="text-muted" >
                                        {teams + " " + t('team.title_plural').toLocaleLowerCase()}
                                    </Form.Text>}
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <OverlayTrigger
                                        delay={{ show: 250, hide: 400 }}
                                        overlay={<Tooltip><Trans i18nKey='match.rounds_help' /></Tooltip>}
                                    >
                                        <NumericInput
                                            label={t('match.rounds')}
                                            name="rounds"
                                            // onChange = { x => setRounds(x) }
                                        />
                                    </OverlayTrigger>
                                    {(stage || group) && values.rounds && <Form.Text className="text-muted" >
                                        {t('match.will_generate', {count: totalMatches(values.rounds)})}
                                    </Form.Text>}
                                </Col>
                            </Row>
                            <Row className="my-1">
                                <Col>
                                    <Row className="justify-content-md-center">
                                        <Button disabled={stage==null || stage._id==0} type="submit">
                                            <Trans i18nKey="match.generate_matches" />
                                        </Button>
                                    </Row>
                                </Col>
                            </Row>
                        </FormFormik> )}
                    </Formik>
                </Col>
                
                <Col md={9}>
                    <MatchesTable tournament={tournament}/>
                </Col>
            </Row>
        </Container>
    )
}

export default MatchesTabScreen
