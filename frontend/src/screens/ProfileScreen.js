import React, { useState, useEffect } from 'react'
import { Form, Button, Row, Col  } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Trans, useTranslation } from 'react-i18next'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { getUserDetails, updateUserProfile } from '../actions/userActions'
import { USER_UPDATE_PROFILE_RESET } from '../constants/userConstants'
import { useNavigate } from 'react-router'

function ProfileScreen() {

    const navigate = useNavigate();
    const { t } = useTranslation();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState('');

    const dispatch = useDispatch();

    const userDetails = useSelector(state => state.userDetails);
    const { error, loading, user } = userDetails;

    const userLogin = useSelector(state => state.userLogin);
    const { userInfo } = userLogin;

    const userUpdateProfile = useSelector(state => state.userUpdateProfile);
    const { success } = userUpdateProfile;

    useEffect(() => {
        if (!userInfo) {
            navigate('/login')
        } else {
            if(!user  || !user.name || success || userInfo._id !== user._id) {
                dispatch({type:USER_UPDATE_PROFILE_RESET});
                dispatch(getUserDetails('profile'));
            } else {
                setName(user.name);
                setEmail(user.email);
            }
        }
    }, [dispatch, userInfo, user, success, navigate])

    
    const submitHandler = (e) => {
        e.preventDefault();

        if (password !== confirmPassword) {
            setMessage(t('profile.passwords_dont_match'));
        } else {
            dispatch(updateUserProfile({
                'id': user._id,
                'name': name,
                'email': email,
                'password': password
            }));
            setMessage('');
        }
        
    }

    return (
        <Row>
            <Col md={3}>
                <h2><Trans i18nKey="profile.title">User profile</Trans></h2>
                {message && <Message variant="danger">{message}</Message>}
                {error && <Message variant="danger">{error}</Message>}
                {loading && <Loader></Loader>}  
                <Form onSubmit={submitHandler}>
                    <Form.Group controlId='name'>
                        <Form.Label>
                            <Trans i18nKey="user.name">Name</Trans>
                        </Form.Label>

                        <Form.Control
                            required
                            type='name'
                            placeholder={t('user.name_placeholder')}
                            value={name}
                            onChange={(e)=>setName(e.target.value) }
                        ></Form.Control>

                    </Form.Group>

                    <Form.Group controlId='email'>
                        <Form.Label>
                            <Trans i18nKey="user.email">Email</Trans>
                        </Form.Label>

                        <Form.Control
                            required
                            type='email'
                            placeholder={t('user.email_placeholder')}
                            value={email}
                            onChange={(e)=>setEmail(e.target.value) }
                        ></Form.Control>

                    </Form.Group>

                    <Form.Group controlId='password'>
                        <Form.Label>
                            <Trans i18nKey="user.password">Password</Trans>
                        </Form.Label>

                        <Form.Control
                            type='password'
                            placeholder={t('user.password_placeholder')}
                            value={password}
                            onChange={(e)=>setPassword(e.target.value) }
                        ></Form.Control>

                    </Form.Group>

                    <Form.Group controlId='confirmPassword'>
                        <Form.Label>
                            <Trans i18nKey="user.confirm_password">Confirm password</Trans>
                        </Form.Label>

                        <Form.Control
                            type='password'
                            placeholder={t('user.confirm_password_placeholder')}
                            value={confirmPassword}
                            onChange={(e)=>setConfirmPassword(e.target.value) }
                        ></Form.Control>

                    </Form.Group>

                    <Button type="submit" variant="primary">
                        <Trans i18nKey="profile.update">Update</Trans>
                    </Button>
                </Form>

            </Col>

        </Row>
    )
}

export default ProfileScreen
