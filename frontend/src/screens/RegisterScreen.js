import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { register } from '../actions/userActions'
import FormContainer from '../components/FormContainer'
import { Trans, useTranslation } from 'react-i18next'

function RegisterScreen({location, history}) {

    const { t } = useTranslation();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState('');

    const dispatch = useDispatch();
    
    const redirect = location && location.search ? location.search.split('=')[1] : '/'

    const userRegister = useSelector(state => state.userRegister);
    
    const { error, loading, userInfo } = userRegister;

    useEffect(() => {
        if (userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

    
    const submitHandler = (e) => {
        e.preventDefault();

        if (password !== confirmPassword) {
            setMessage('Las contraseñas no son iguales papanatas');
        } else {
            dispatch(register(name, email, password));
        }
        
    }

    return (
        <FormContainer>
            <h1>Registrarse</h1>
            {error && <Message variant="danger">{message}</Message>}
            {loading && <Loader></Loader>}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='name'>
                    <Form.Label><Trans i18nKey="user.name">Name</Trans></Form.Label>

                    <Form.Control
                        required
                        type='name'
                        placeholder={t('user.name_placeholder')}
                        value={name}
                        onChange={(e)=>setName(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Form.Group controlId='email'>
                    <Form.Label><Trans i18nKey="user.email">Email</Trans></Form.Label>

                    <Form.Control
                        required
                        type='email'
                        placeholder={t('user.email_placeholder')}
                        value={email}
                        onChange={(e)=>setEmail(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Form.Group controlId='password'>
                    <Form.Label>
                        <Trans i18nKey="user.password">Password</Trans>
                    </Form.Label>

                    <Form.Control
                        required
                        type='password'
                        placeholder={t('user.password_placeholder')}
                        value={password}
                        onChange={(e)=>setPassword(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Form.Group controlId='confirmPassword'>
                    <Form.Label>
                        <Trans i18nKey="user.confirm_password">Confirm password</Trans>
                    </Form.Label>

                    <Form.Control
                        required
                        type='password'
                        placeholder={t('user.confirm_password_placeholder')}
                        value={confirmPassword}
                        onChange={(e)=>setConfirmPassword(e.target.value) }
                    ></Form.Control>

                </Form.Group>

                <Button type="submit" variant="primary">
                    <Trans i18nKey="register.register">Register</Trans>
                </Button>
            </Form>
            
            <Row className='py-3'>
                <Col>
                    <Trans i18nKey="register.have_an_account">Have an account?</Trans>
                    <Link to={redirect ? `/login?redirect=${redirect}` : '/login'}>
                        <Trans i18nKey="register.login">Login</Trans>
                    </Link>
                </Col>

            </Row>
        </FormContainer>
    )
}

export default RegisterScreen
