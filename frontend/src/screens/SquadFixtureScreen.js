import React, { useEffect } from 'react'
import { Container, Row } from 'react-bootstrap';
import { Trans } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { listMatches } from '../actions/squadActions';
import Loader from '../components/Loader';
import MatchCard from '../components/MatchCard';
import Message from '../components/Message';

function SquadFixtureScreen() {
    const { id: squadId } = useParams();
    const { stage: stageId } = useParams();

    const dispatch = useDispatch();
    const matchList = useSelector(state => state.squadFixture);

    const { error, loading, matches } = matchList;

    useEffect(() => {
        dispatch(listMatches(squadId, stageId));
    }, [dispatch]);

    return (
        <div>
            <h1><Trans i18nKey="squad.fixture">Fixture</Trans></h1>
            { loading ? (
                <Loader />
            ) : (
                error ? 
                    (<Message variant="danger">{ error }</Message>) 
                : (
                    <Container>
                        { matches ? matches.map(match => (
                            <Row key={match._id}>
                                <MatchCard
                                    match={match}
                                    key={match._id}
                                    
                                />
                            </Row>
                        )) : "No matches"}
                    </Container>
                )
            )}
        </div>
    )
}

export default SquadFixtureScreen