import React, { useEffect, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import { listSquadsDetails } from '../actions/squadActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
import PersonCard from '../components/PersonCard';

function SquadScreen() {
    const { id } = useParams();

    const { t } = useTranslation();

    const dispatch = useDispatch();
    const squadDetails = useSelector(state => state.squadDetails);
    const { error, loading, squad } = squadDetails;

    useEffect(() => {
        dispatch(listSquadsDetails(id));
    }, [dispatch])


    return (
        <div>
            
            { loading ? (
                    <Loader />
                ) : ("")
            }
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            { squad && squad.team && (
                    <div>
                        <Row>
                            <Col md="auto">
                                <Image src={"/staticdjango" + squad.team.shield}
                                    width="150"
                                    rounded>
                                </Image>
                            </Col>
                            <Col>
                                <h1>{ squad.name }</h1>
                                <h3>{ squad.tournament.category.name}</h3>
                                <h2> { squad.tournament.name + " - " + squad.tournament.season} </h2>
                            </Col>
                        </Row>
                        <Row>
                            { squad.players ? squad.players.map(player => (
                                    <PersonCard
                                            person={player}
                                            key={player._id}
                                        />
                                    
                                )) : t('squad.noplayers')
                            }
                        </Row>


                    </div>
                )

            }
              
        </div>
    )
}

export default SquadScreen