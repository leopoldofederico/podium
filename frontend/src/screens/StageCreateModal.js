import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { Modal, Button } from 'react-bootstrap'
import { Formik, Form as FormFormik } from 'formik';
import { Trans, useTranslation } from 'react-i18next'
import NameInput from '../components/NameInput';
import NumericInput from '../components/NumericInput';
import * as Yup from 'yup'
import { createStage } from '../actions/stageActions';

function StageCreateModal( { tournament, ...props } ) {
    const { t }= useTranslation();
    const dispatch = useDispatch();

    console.log(tournament)
    const [name, setName] = useState("")

    const handleNewStage = x => {
        dispatch(createStage({ 
            name: x.name,
            tournament_id: tournament._id,
            win_points: x.winPoints,
            draw_points: x.drawPoints,
            lost_points: x.lostPoints,
            abandon_points: x.abandonPoints,
            goals_win_abandon: x.goalsWinAbandon,
            goals_lost_abandon: x.goalsLostAbandon,
        }));
        props.handleClose();
    }

    return (
        <Modal show={true} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>
                    <Trans i18nKey="stage.new">New stage</Trans>
                </Modal.Title>
            </Modal.Header>
            <Formik
                enableReinitialize={true}
                initialValues={{
                    name: name,
                    winPoints: tournament.win_points,
                    drawPoints: tournament.draw_points,
                    lostPoints: tournament.lost_points,
                    abandonPoints: tournament.abandon_points,
                    goalsWinAbandon: tournament.goals_win_abandon,
                    goalsLostAbandon: tournament.goals_lost_abandon,
                }}
                validationSchema={Yup.object({
                    name: Yup.string().required(t('validate.required')),
                    winPoints: Yup.number().required(),
                    drawPoints: Yup.number().required(),
                    lostPoints: Yup.number().required(),
                    abandonPoints: Yup.number().required(),
                    goalsWinAbandon: Yup.number().required().positive(),
                    goalsLostAbandon: Yup.number().required(),
                })}
                onSubmit={(values, { setSubmitting }) => {
                    handleNewStage(values);
                }}
            >
                <FormFormik>
                    <Modal.Body>
                        <NameInput
                            label={t('stage.name')}
                            name="name"
                            placeholder={t('stage.name_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.win_points')}
                            name="winPoints"
                            placeholder={t('tournament.win_points_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.draw_points')}
                            name="drawPoints"
                            placeholder={t('tournament.draw_points_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.lost_points')}
                            name="lostPoints"
                            placeholder={t('tournament.lost_points_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.abandon_points')}
                            name="abandonPoints"
                            placeholder={t('tournament.abandon_points_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.goals_win_abandon')}
                            name="goalsWinAbandon"
                            placeholder={t('tournament.goals_win_abandon_placeholder')}
                        />
                        <NumericInput
                            label={t('tournament.goals_lost_abandon')}
                            name="goalsLostAbandon"
                            placeholder={t('tournament.goals_lost_abandon_placeholder')}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            <Trans i18nKey="home.close">Close</Trans>
                        </Button>
                        <Button variant="primary" type="submit">
                            <Trans i18nKey="home.save">Save</Trans>
                        </Button>
                    </Modal.Footer>
                </FormFormik>
            </Formik>
        </Modal>
    )
}

export default StageCreateModal
