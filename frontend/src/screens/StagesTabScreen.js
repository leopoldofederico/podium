import React, { useState, useEffect } from 'react'
import { Button, Container, Row, Col, Modal, Form  } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Trans, useTranslation } from 'react-i18next';
import { BsPlusCircleFill } from "react-icons/bs";
import Stage from '../components/StageCard';
import { createStage } from '../actions/stageActions';
import { assignTeams } from '../actions/groupActions';
import { listTournamentsDetails } from '../actions/tournamentActions'
import Squad from '../components/DraggableSquad';
import Select from 'react-select';
import StageCreateModal from './StageCreateModal';

function StagesTabScreen( {tournament, stages, squads, ... props} ) {
    
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const [showCreate, setShowCreate] = useState(false)

    const [name, setName] = useState("")
    
    const [checked, setChecked] = useState([]);
    const [groups, setGroups ] = useState([]);
    const [selectedGroup, setSelectedGroup] = useState([])
    
    const stageCreate = useSelector(state => state.stageCreate);
    const { error: errorCreate, stage: stageCreated, success: successCreate } = stageCreate;

    const teamsAdded = useSelector(state => state.groupUpdate);
    const { error: errorTeamsAdded, loading: loadingTeamsAdded, success: successTeamAdded} = teamsAdded;

    const handleCreate = () => {
        setShowCreate(true);
    }

    const handleClose = () => {
        setShowCreate(false);
    }

    const handleNewStage = x => {
        dispatch(createStage({ 
            name: x.name,
            tournament_id: tournament._id
        }));
        handleClose();
    }

    const handleCheckAll = e => {
        let newChecked = checked.map(o => {return { ...o, value: e.target.checked }});

        setChecked(newChecked)
     } 

    const handleCheck = e => {
        const index = e.target.name.split("-")[1];
        let newChecked = checked.slice();
        
        newChecked[index].value = e.target.checked;
        setChecked(newChecked);
    }

    const handleAssignTeamsToGroup = () => {
        dispatch(assignTeams(checked.filter(x => x.value), selectedGroup.id));
    }

    useEffect(() => {
        if (stageCreated || successTeamAdded) {
            dispatch(listTournamentsDetails(tournament._id))
        }
    }, [stageCreated, successTeamAdded])

    useEffect(() => {
        if (squads) {
            setChecked(squads.map(o => { return {_id: o._id, value: false} }))
        }
    }, [squads])

    useEffect(() => {
        if (stages) {
            let newGroups = [];
            stages.forEach( stage => 
                newGroups.push(...stage.groups.map( 
                    g => {return {id: g._id, label: `${stage.name}-${g.name}`}}
                ))
            )
            setGroups(newGroups);
        }
    }, [stages])

    return (
        <Container>
            <Row>
                <Button onClick={ handleCreate }>
                    <Row className="mx-1 align-items-center">
                        <BsPlusCircleFill className="mr-1"/>
                        <Trans i18nKey="stage.create">Create stage</Trans>                        
                    </Row>
                </Button>
            </Row>
            
            {showCreate && <StageCreateModal tournament={tournament} handleClose={handleClose}/>}
            <Row>
                <Col md={3}>
                    <Row>
                        {/* <Col> */}
                            <Form>
                                <Form.Check
                                    type="checkbox"
                                    id='check-all'
                                    onChange ={ handleCheckAll }
                                    label = { t('main.selectOrUnselectAll') }
                                />
                            </Form>
                            
                        {/* </Col> */}
                    </Row>
                    { checked && checked.length > 0 && checked.map((squad, index) => (
                        <Row key={squad._id} className="align-items-center">
                            <Col md={1}>
                                <Form.Check
                                    type="checkbox"
                                    id={`check-${squad._id}`}
                                    name={`check-${index}`}
                                    onChange={ handleCheck }
                                    checked = { checked[index].value }
                                />

                            </Col>
                            <Col>          
                                {/* temporario en false por error de Draggable Squad                       */}
                                {squads && <Squad
                                    squad={ squads[index] }
                                    key={squad._id}
                                />}
                            </Col>
                        </Row>
                    ))}
                    <Row>
                        <Col>
                            <Select
                                className="my-1"
                                getOptionLabel={e => e.label}
                                getOptionValue={e => e._id}
                                options = { groups }
                                onChange = { value => setSelectedGroup(value) }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Button 
                            className ="my-1"
                            disabled={ selectedGroup.length == 0 }
                            onClick={ handleAssignTeamsToGroup }
                        >
                            <Trans i18nKey="squad.assignToGroup">Assign to group</Trans>
                        </Button>
                    </Row>
                </Col>
                <Col md={6}>
                    { stages ? stages.map(stage => (
                        // <Row key={stage._id}>
                            <Stage
                                stage={stage}
                                key={stage._id}
                                tournament={tournament._id}
                            />
                        // </Row>
                    )) : "No stages"}
                </Col>
            </Row>

        </Container>

    )
}

export default StagesTabScreen
