import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { listStandings } from '../actions/standingActions';
import { useTranslation, Trans } from 'react-i18next';
import { Col, Container, Image, Row, Table } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import { useTable, useFilters, useGlobalFilter, useAsyncDebounce, useSortBy } from 'react-table'
import MatchIcon from '../components/MatchIcon';

function StandingsScreen( ) {
    const { t } = useTranslation();
    
    const { id: groupId } = useParams();

    const dispatch = useDispatch();
    const standingsList = useSelector(state => state.standingsList);

    const [data, setData] = useState([])
    const [header, setHeader] = useState({name: "1"})
    const [tournament, setTournament] = useState(null)

    const { error, loading, standings } = standingsList;
    
    useEffect(() => {
        dispatch(listStandings(groupId));
    }, [dispatch]);

    useEffect(() => {
        if (standings && !Array.isArray(standings)) {
            setData(standings.standings)
            setTournament(standings.tournament)
            setHeader(standings)
        }
    }, [standings])
    
    useEffect(() => {
        if (tournament) {
            let hiddenColumns = []
            if (! tournament.draw) {
                hiddenColumns.push('draw')
            } 
            if (! tournament.bonus) {
                hiddenColumns.push('bonus')
            }
            setHiddenColumns(hiddenColumns)
        }
    }, [tournament])
    

    const columns = React.useMemo(
        () => [
            {
                Header: t('team.title'),
                accesor: 'team',
                Cell: (props) => {
                    return (
                        <Container>
                            <Image src={ "/staticdjango/" + props.row.original.team.shield } 
                                width="20"
                                rounded 
                                className="mr-1"
                            />
                            <Link to={`/squad/${props.row.original._id}/matches/${props.row.original.stage_id}`}>
                                <strong>{props.row.original.name}</strong>
                            </Link>
                        </Container>
                    );
                },
            },
        
            {
                Header: t('standings.played'),
                accessor: 'played',
            },
            {
                Header: t('standings.points'),
                accessor: 'points',
                sortType: 'number',
            },
            {
                Header: t('standings.score_for'),
                accessor: 'scoreFor',
            },
            {
                Header: t('standings.score_against'),
                accessor: 'scoreAgainst',
            },
            {
                Header: t('standings.difference'),
                accessor: 'difference',
                sortType: 'basic',
            },
            {
                Header: t('standings.win'),
                accessor: 'win',
            },
            {
                Header: t('standings.draw'),
                accessor: 'draw',
            },
            {
                Header: t('standings.lost'),
                accessor: 'lost',
            },
            {
                Header: t('standings.bonus'),
                accessor: 'bonus',
            },
            {
                Header: t('standings.last_matches'),
                accesor: 'last_matches',
                Cell: (props) => {
                    return (
                        <Container>
                            {/* {props.row.original.lastMatches.length} */}
                            {props.row.original.lastMatches &&
                            props.row.original.lastMatches.map(m => 
                                <MatchIcon match={m} key={m._id} />
                            )}
                        </Container>
                    )
                }
            }
        ],
        [header]
    )
    
    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, state,
        visibleColumns, preGlobalFilteredRows, setGlobalFilter, setHiddenColumns } = useTable({
            columns,
            data,
            initialState: {
                sortBy: [
                    {
                        id: 'points',
                        desc: true
                    },
                    {
                        id: 'difference',
                        desc: true,
                    }
                ],
                hiddenColumns: []
            }
            },
        useFilters, // useFilters!
        useGlobalFilter, // useGlobalFilter!
        useSortBy)

    return (
        <div>
            { tournament && 
                <Row>
                    { tournament.logo && (
                        <Col md="auto">
                            <Image src={"/staticdjango" + tournament.logo} 
                                width="150"
                                rounded>
                            </Image>
                        </Col>
                    )}
                    <Col>
                        <h1>{ tournament.name } - { tournament.season } </h1>
                        <h3>{ tournament.category.name}</h3>
                        <h2> { header.stage + " - " + header.name } </h2>
                    </Col>
                    
                </Row>
            }
            <Table striped bordered hover {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
            
                <tbody>
                    {rows.map((row, i) => {
                        prepareRow(row)
            
                        return (
                            <tr className="my-1" {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td className="m-1 p-1" {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </td>
                                    )
                                })}
                            </tr>
                        )
            
            
                    })}
                </tbody>
            </Table>
        </div>
    )
}

export default StandingsScreen
