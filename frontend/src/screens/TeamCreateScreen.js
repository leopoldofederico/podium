import React, { useEffect } from 'react'
import { Trans, useTranslation } from 'react-i18next';
import { useLocation, Navigate } from 'react-router-dom'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import FormContainer from '../components/FormContainer'
import NameInput from '../components/NameInput'
import InputPlace from '../components/InputPlace'
import FileInput from '../components/FileInput'
import * as Yup from 'yup';
import { Formik, Form as FormFormik } from 'formik';
import { Button, Container, Row } from 'react-bootstrap'
import { TEAM_CREATE_RESET } from '../constants/teamConstants'
import { createTeam } from '../actions/teamActions'


function TeamCreateScreen( ) {

    const { t } = useTranslation();

    const dispatch = useDispatch();
    const location = useLocation();

    const teamCreate = useSelector(state => state.teamCreate);
    const { error, success } = teamCreate;

    useEffect(() => {
        if (success){ 
            dispatch( { type: TEAM_CREATE_RESET });
        }
        
    }, [dispatch, location, success] )

    return (
        <div>
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            { success ? (
                <Navigate to="/teams" state={{ from: location }} replace />
            ) : ("") }
            <Link to='/teams'><Trans i18nKey="home.back">Go back</Trans></Link>
            <FormContainer>
                <h1><Trans i18nKey="tournament.create">Create Tournament</Trans></h1>
                    <Formik
                        enableReinitialize={true}
                        initialValues={{
                            name: '',
                            official_name: '',
                            shield: '',
                            place: '',           
                        }}
                        validationSchema={Yup.object({
                            name: Yup.string().required(t('validate.required')),
                            official_name: Yup.string().required(t('validate.required')),
                            place: Yup.string().required(t('validate.required')),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            dispatch(createTeam({ 
                                name: values.name, 
                                shield: values.shield, 
                                place_id: values.place,
                                official_name: values.official_name
                            }))
                        }}
                    >
                        <FormFormik>
                            <NameInput
                                label={t('team.name')}
                                name="name"
                                placeholder={t('team.name_placeholder')}
                            />
                            <NameInput
                                label={t('team.official_name')}
                                name="official_name"
                                placeholder={t('team.official_name_placeholder')}
                            />
                            <FileInput
                                label={t('team.shield')}
                                name="shield"
                                placeholder={t('team.shield_placeholder')}
                            />
                            <InputPlace
                                name="place"
                                // onChange={value => console.log(value) }
                            />
                            <Container>
                                <Row className="justify-content-md-center">
                                    <Button
                                        size="lg"
                                        variant="primary"
                                        type="submit"
                                    >
                                        <Trans i18nKey="home.save">Save</Trans>
                                    </Button>
                                </Row>
                            </Container>
                        </FormFormik>
                    </Formik>
            </FormContainer>
        </div>
    )
}

export default TeamCreateScreen
