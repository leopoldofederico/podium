import React, { useState, useEffect } from 'react'
import { Trans, useTranslation } from 'react-i18next';
import { Link, useParams, Navigate, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import FormContainer from '../components/FormContainer'
import NameInput from '../components/NameInput'
import InputPlace from '../components/InputPlace'
import FileInput from '../components/FileInput'
import * as Yup from 'yup';
import { Formik, Form as FormFormik } from 'formik';
import { Button, Container, Row } from 'react-bootstrap'
import { TEAM_UPDATE_RESET } from '../constants/teamConstants'
import { listTeamsDetails, updateTeam } from '../actions/teamActions'

function TeamEditScreen( ) {

    const { t } = useTranslation();
    
    const { id: teamId } = useParams();
    const location = useLocation();
    const [name, setName] = useState('');
    const [officialName, setOfficialName] = useState("");
    const [place, setPlace] = useState('1');
    const [shield, setShield] = useState('');

    const dispatch = useDispatch();
    const teamDetails = useSelector(state => state.teamDetails);
    const { error, loading, team } = teamDetails;

    const teamUpdate = useSelector(state => state.teamUpdate);
    const { error: errorUpdate, loading: loadingUpdate, success:successUpdate } = teamUpdate;

    useEffect(() => {
        
        if (successUpdate){ 
            dispatch( { type: TEAM_UPDATE_RESET });
        } else {
            
            if ( !team.name || team._id !== Number(teamId)) {
                dispatch(listTeamsDetails(teamId));
            } else {
                setName(team.name);
                setOfficialName(team.official_name);
                setPlace(team.place._id);
                setShield(team.shield);
            }
        }
        
    }, [dispatch, team, teamId, successUpdate] )

    return (
        <div>
            { loading && <Loader /> }
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            { errorUpdate ? (
                    <Message variant="danger">{errorUpdate}</Message>
                ) : ("")
            }
            { successUpdate ? (
                    <Navigate  to="/teams" state={{ from: location }} replace />
                ) : ("") 
            }
            <Link to='/teams'><Trans i18nKey="home.back">Go back</Trans></Link>
            <FormContainer>
                <h1><Trans i18nKey="team.edit">Edit Team</Trans></h1>
                    <Formik
                        enableReinitialize={true}
                        initialValues={{
                            name: name,
                            official_name: officialName,
                            shield: shield,
                            place: place,           
                        }}
                        validationSchema={Yup.object({
                            name: Yup.string().required(t('validate.required')),
                            official_name: Yup.string().required(t('validate.required')),
                            place: Yup.string().required(t('validate.required')),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            let updatedTeam = {
                                _id: team._id,
                                name: values.name,
                                shield: values.shield, 
                                place_id: values.place,
                                official_name: values.official_name
                            }
                            // console.log(updatedTeam)
                            dispatch(updateTeam(updatedTeam))
                        }}
                    >
                        <FormFormik>
                            <NameInput
                                label={t('team.name')}
                                name="name"
                                placeholder={t('team.name_placeholder')}
                            />
                            <NameInput
                                label={t('team.official_name')}
                                name="official_name"
                                placeholder={t('team.official_name_placeholder')}
                            />
                            <FileInput
                                label={t('team.shield')}
                                name="shield"
                                placeholder={t('team.shield_placeholder')}
                            />
                            
                            <InputPlace
                                name="place"
                                initial={ team.place }
                            />
                            <Container>
                                <Row className="justify-content-md-center">
                                    <Button
                                        size="lg"
                                        variant="primary"
                                        type="submit"
                                    >
                                        <Trans i18nKey="home.save">Save</Trans>
                                    </Button>
                                </Row>
                            </Container>
                        </FormFormik>
                    </Formik>
            </FormContainer>
        </div>
    )
}

export default TeamEditScreen