import React, {useEffect, useState} from 'react';
import { Trans } from 'react-i18next';
import { Row, Container, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { listOwnTeams } from '../actions/teamActions';
import Team from '../components/TeamCard';
import Message from '../components/Message';
import Loader from '../components/Loader';
import { BsPlusCircleFill } from "react-icons/bs";
import { LinkContainer } from 'react-router-bootstrap';

function TeamsScreen() {
    const dispatch = useDispatch();
    const teamList = useSelector(state => state.teamListOwn);
    const { error, loading, teams } = teamList;
    const [deleted, setDeleted] = useState(null);

    useEffect(() => {
        dispatch(listOwnTeams());
    }, [dispatch]);

    return (
        <div>
            <h1><Trans i18nKey="team.title">Teams</Trans></h1>
            { loading ? (
                <Loader />
            ) : (
                error ? 
                    (<Message variant="danger">{ error }</Message>) 
                : (
                    <div>
                        <LinkContainer to="/team/create">
                            <Container>
                                <Row>
                                    <Button type="primary">
                                        <Row className="mx-1 align-items-center">
                                            <BsPlusCircleFill className="mr-1"/>
                                            <Trans i18nKey="team.create">Create team</Trans>
                                        </Row>
                                    </Button>
                                </Row>
                            </Container>
                        </LinkContainer>
                        <Container fluid="md">
                            { teams ? teams.map(team => (
                                <Row key={team._id}>
                                    <Team 
                                        team={team} 
                                        key={team._id} 
                                    />
                                </Row>
                            )) : "No hay torneos"}
                        </Container>
                    </div>
                )  
            )}
            
        </div>
    )
}

export default TeamsScreen
