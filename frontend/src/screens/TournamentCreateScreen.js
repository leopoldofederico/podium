import React, { useEffect } from 'react'
import { Trans, useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import FormContainer from '../components/FormContainer'
import NameInput from '../components/NameInput'
import NumericInput from '../components/NumericInput';
import CheckInput from '../components/CheckInput'
import SelectCategoryInput from '../components/SelectCategoryInput'
import * as Yup from 'yup';
import { Formik, Form as FormFormik } from 'formik';
import { Button, Container, Row } from 'react-bootstrap'
import { TOURNAMENT_CREATE_RESET } from '../constants/tournamentConstants'
import { createTournament } from '../actions/tournamentActions'
import { useLocation, Navigate } from 'react-router-dom'
import FileInput from '../components/FileInput';

function TournamentCreateScreen( ) {

    const { t } = useTranslation();

    const location = useLocation();

    const dispatch = useDispatch();
    const tournamentCreate = useSelector(state => state.tournamentCreate);
    const { error, success } = tournamentCreate;

    useEffect(() => {
        if (success){ 
            dispatch( { type: TOURNAMENT_CREATE_RESET });
        }
        
    }, [dispatch, location, success] )

    return (
        <div>
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            { success ? (
                <Navigate to="/tournaments" state={{ from: location }} replace />
            ) : ("") }
            <Link to='/tournaments'><Trans i18nKey="home.back">Go back</Trans></Link>
            <FormContainer>
                <h1><Trans i18nKey="tournament.create">Create Tournament</Trans></h1>
                    <Formik
                        enableReinitialize={true}
                        initialValues={{
                            name: '',
                            season: '',
                            category: '',
                            isCurrent: true,
                            winPoints: 2,
                            drawPoints: 1,
                            lostPoints: 0,
                            abandonPoints: 0,
                            goalsWinAbandon: 20,
                            goalsLostAbandon: 0,
                            draw: false,
                            bonus: false,
                            // logo: '',
                        }}
                        validationSchema={Yup.object({
                            name: Yup.string().required(t('validate.required')),
                            season: Yup.string().required(t('validate.required')),
                            category: Yup.string().required(t('validate.required')),
                            winPoints: Yup.number().required(),
                            drawPoints: Yup.number().required(),
                            lostPoints: Yup.number().required(),
                            abandonPoints: Yup.number().required(),
                            goalsWinAbandon: Yup.number().required().positive(),
                            goalsLostAbandon: Yup.number().required(),
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            dispatch(createTournament({ 
                                name: values.name, 
                                season: values.season, 
                                category_id: values.category,
                                isCurrent: values.isCurrent,
                                win_points: values.winPoints,
                                draw_points: values.drawPoints,
                                lost_points: values.lostPoints,
                                abandon_points: values.abandonPoints,
                                goals_win_abandon: values.goalsWinAbandon,
                                goals_lost_abandon: values.goalsLostAbandon,
                                draw: values.draw,
                                bonus: values.bonus,
                                // logo: values.logo,
                            }))
                        }}
                    >
                        <FormFormik>
                            <NameInput
                                label={t('tournament.name')}
                                name="name"
                                placeholder={t('tournament.name_placeholder')}
                            />
                            <NameInput
                                label={t('tournament.season')}
                                name="season"
                                placeholder={t('tournament.season_placeholder')}
                            />
                            <CheckInput
                                label={t('tournament.isCurrent')}
                                name="isCurrent"
                            />
                            <SelectCategoryInput 
                                name="category"
                                // onChange={value => console.log(value) }
                            />
                            <NumericInput
                                label={t('tournament.win_points')}
                                name="winPoints"
                                placeholder={t('tournament.win_points_placeholder')}
                            />
                            <CheckInput
                                label={t('tournament.draw_in_standings')}
                                name="draw"
                            />
                            <NumericInput
                                label={t('tournament.draw_points')}
                                name="drawPoints"
                                placeholder={t('tournament.draw_points_placeholder')}
                            />
                            <NumericInput
                                label={t('tournament.lost_points')}
                                name="lostPoints"
                                placeholder={t('tournament.lost_points_placeholder')}
                            />
                            <NumericInput
                                label={t('tournament.abandon_points')}
                                name="abandonPoints"
                                placeholder={t('tournament.abandon_points_placeholder')}
                            />
                            <CheckInput
                                label={t('tournament.bonus')}
                                name="bonus"
                            />
                            <NumericInput
                                label={t('tournament.goals_win_abandon')}
                                name="goalsWinAbandon"
                                placeholder={t('tournament.goals_win_abandon_placeholder')}
                            />
                            <NumericInput
                                label={t('tournament.goals_lost_abandon')}
                                name="goalsLostAbandon"
                                placeholder={t('tournament.goals_lost_abandon_placeholder')}
                            />
                            {/* <FileInput
                                label={t('tournament.logo')}
                                name="logo"
                                placeholder={t('tournament.logo_placeholder')}
                            /> */}
                            <Container>
                                <Row className="justify-content-md-center">
                                    <Button
                                        size="lg"
                                        variant="primary"
                                        type="submit"
                                    >
                                        <Trans i18nKey="home.save">Save</Trans>
                                    </Button>
                                </Row>
                            </Container>
                        </FormFormik>
                    </Formik>
            </FormContainer>
        </div>
    )
}

export default TournamentCreateScreen