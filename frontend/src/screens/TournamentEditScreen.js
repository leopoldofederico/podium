import React, { useState, useEffect, useRef } from 'react'
import { Trans, useTranslation } from 'react-i18next';
import { Navigate, Link, useLocation, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import FormContainer from '../components/FormContainer'
import NameInput from '../components/NameInput'
import NumericInput from '../components/NumericInput'
import CheckInput from '../components/CheckInput'
import SelectCategoryInput from '../components/SelectCategoryInput'
import * as Yup from 'yup';
import { Formik, Form as FormFormik } from 'formik';
import { Button, Container, Row, Col, Tabs, Tab, Form, Image } from 'react-bootstrap'
import { TOURNAMENT_UPDATE_RESET } from '../constants/tournamentConstants'
import { listTournamentsDetails, updateLogo, updateTournament } from '../actions/tournamentActions'
import { createSquad } from '../actions/squadActions'
import Squad from '../components/SquadCard';
import SelectTeamForTournament from '../components/SelectTeamForTournament';
import StagesTabScreen from './StagesTabScreen';
import MatchesTabScreen from './MatchesTabScreen';
import IncidencesTabScreen from './IncidencesTabScreen';

function TournamentEditScreen( ) {

    const { t } = useTranslation();

    const { id: tournamentId } = useParams();

    // console.log("torneo es " + tournamentId);
    // console.log(useParams())
    const location = useLocation();
    const [name, setName] = useState('');
    const [isCurrent, setIsCurrent] = useState(1);
    const [category, setCategory] = useState('1');
    const [season, setSeason] = useState('');
    const [winPoints, setWinPoints] = useState(0);
    const [drawPoints, setDrawPoints] = useState(0);
    const [lostPoints, setLostPoints] = useState(0);
    const [draw, setDraw] = useState(true);
    const [bonus, setBonus] = useState(false);
    const [abandonPoints, setAbandonPoints] = useState(0);
    const [goalsWinAbandon, setGoalsWinAbandon] = useState(0);
    const [goalsLostAbandon, setGoalsLostAbandon] = useState(0);
    const [logo, setLogo] = useState('');

    const [uploadedFileName, setUploadedFileName] = useState(null)
    const [newImage, setNewImage] = useState(null)
    const inputRef = useRef(null);

    const handleDisplayFileDetails = (e) => {
        inputRef.current?.files && setUploadedFileName(inputRef.current.files[0].name);
        setLogo(inputRef.current.files[0])
        setNewImage(URL.createObjectURL(e.target.files[0]))

    };

    useEffect(() => {
        if (logo && typeof(logo)!= "string") {
            console.log(typeof(logo))
            console.log(logo)
            dispatch(updateLogo(
                { 
                    _id: tournamentId,
                    logo: logo
                }
            ))
        }
    }, [logo])

    const [selectedTeam, setSelectedTeam] = useState(null);

    const useQuery = () =>
        new URLSearchParams(useLocation().search);
    
    const [key, setKey] = useState("home");
    const defaultKey = useQuery().get("tab");
    
    const dispatch = useDispatch();
    const tournamentDetails = useSelector(state => state.tournamentDetails);
    const { error, loading, tournament } = tournamentDetails;

    const tournamentUpdate = useSelector(state => state.tournamentUpdate);
    const { error: errorUpdate, loading: loadingUpdate, success:successUpdate } = tournamentUpdate;

    const tournamentLogo = useSelector(state => state.tournamentLogo);
    const { error: errorUpdateLogo, loading: loadingUpdateLogo, success:successUpdateLogo } = 
        tournamentLogo;

    const squadCreate = useSelector(state => state.squadCreate);
    const { error: errorCreateSquad, 
        loading: loadingCreateSquad, 
        success: successCreateSquad} = squadCreate;

    const handleSelectedTeam = (value) => {
        setSelectedTeam(value)
        // alert("algo cambió " + value.name)
    }

    const handleTeamBlur = () => {

    }

    const registerSquad = () => {
        if (selectedTeam) {
            dispatch(createSquad({
                team_id: selectedTeam._id,
                name: selectedTeam.name,
                tournament_id: tournament._id
            }));
        }
    }
    
    useEffect(() => {
        if (successUpdate){ 
            dispatch( { type: TOURNAMENT_UPDATE_RESET });
        } else {
            if (!error)  {
                if ( tournament._id !== Number(tournamentId)) {
                    dispatch(listTournamentsDetails(tournamentId));
                } else {
                    setName(tournament.name);
                    setSeason(tournament.season);
                    setCategory(tournament.category._id);
                    setIsCurrent(tournament.isCurrent)
                    setWinPoints(tournament.win_points)
                    setDrawPoints(tournament.draw_points)
                    setLostPoints(tournament.lost_points)
                    setAbandonPoints(tournament.abandon_points)
                    setDraw(tournament.draw)
                    setBonus(tournament.bonus)
                    setGoalsWinAbandon(tournament.goals_win_abandon)
                    setGoalsLostAbandon(tournament.goals_lost_abandon)
                    setLogo(tournament.logo)
                }
            }
        }
        
    }, [dispatch, tournament, tournamentId, location, successUpdate] )

    //squad registered OK in tournament
    useEffect(() => {
        if (successCreateSquad) {
            dispatch(listTournamentsDetails(tournamentId))
        }
    }, [successCreateSquad])

    useEffect(() => {
        setKey(defaultKey ? defaultKey : "home")
    }, [])

    return (
        <div>
            {/* load tournament error */}
            { error ? (
                    <Message variant="danger">{error}</Message>
                ) : ("")
            }
            { successUpdate ? (
                <Navigate to="/tournaments" state={{ from: location }} replace />
            ) : ("") 
            }            
            <Container fluid>
                <Link to='/tournaments'><Trans i18nKey="home.back">Go back</Trans></Link>
                <h1><Trans i18nKey="tournament.edit">Edit Tournament</Trans></h1>
            
                <Tabs
                    activeKey={key}
                    id="uncontrolled-tab"
                    onSelect={k => setKey(k)}
                    className="mb-3">
                    <Tab eventKey="home" title={t('tournament.base_data')}>
                        <Row fluid="true">
                            <Col md={6} fluid="true">
                                <FormContainer>
                                    {loadingUpdate && <Loader />}
                                    {errorUpdate  && <Message variant="danger">{errorUpdate}</Message>}
                                    { loading ? (
                                        <Loader />
                                    ) : (
                                        error ? (
                                            <Message variant="danger">{ error }</Message>
                                        ) : (
                                            <Formik
                                                enableReinitialize={true}
                                                initialValues={{
                                                    name: name,
                                                    season: season,
                                                    category: category,
                                                    isCurrent: isCurrent,
                                                    winPoints: winPoints,
                                                    drawPoints: drawPoints,
                                                    lostPoints: lostPoints,
                                                    abandonPoints: abandonPoints,
                                                    draw: draw,
                                                    bonus: bonus,
                                                    goalsWinAbandon: goalsWinAbandon,
                                                    goalsLostAbandon: goalsLostAbandon,
                                                    // logo: logo,
                                                }}
                                                validationSchema={Yup.object({
                                                    name: Yup.string().required(t('validate.required')),
                                                    season: Yup.string().required(t('validate.required')),
                                                    category: Yup.string().required(t('validate.required')),
                                                    winPoints: Yup.number().required(),
                                                    drawPoints: Yup.number().required(),
                                                    lostPoints: Yup.number().required(),
                                                    abandonPoints: Yup.number().required(),
                                                    goalsWinAbandon: Yup.number().required().positive(),
                                                    goalsLostAbandon: Yup.number().required(),
                                                })}
                                                onSubmit={(values, { setSubmitting }) => {
                                                    dispatch(updateTournament({
                                                        _id: tournament._id,
                                                        name: values.name,
                                                        season: values.season,
                                                        category_id: values.category,
                                                        isCurrent: values.isCurrent,
                                                        win_points: values.winPoints,
                                                        draw_points: values.drawPoints,
                                                        lost_points: values.lostPoints,
                                                        abandon_points: values.abandonPoints,
                                                        draw: values.draw,
                                                        bonus: values.bonus,
                                                        goals_win_abandon: values.goalsWinAbandon,
                                                        goals_lost_abandon: values.goalsLostAbandon,
                                                        // logo: values.logo,
                                                    }))
                                                }}
                                            >
                                                <FormFormik>
                                                    <NameInput
                                                        label={t('tournament.name')}
                                                        name="name"
                                                        placeholder={t('tournament.name_placeholder')}
                                                    />
                                                    <NameInput
                                                        label={t('tournament.season')}
                                                        name="season"
                                                        placeholder={t('tournament.season_placeholder')}
                                                    />
                                                    <CheckInput
                                                        label={t('tournament.isCurrent')}
                                                        name="isCurrent"
                                                    />
                                                    <SelectCategoryInput
                                                        name="category"
                                                        initial={ tournament.category }
                                                        // onChange={value => console.log(value) }
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.win_points')}
                                                        name="winPoints"
                                                        placeholder={t('tournament.win_points_placeholder')}
                                                    />
                                                    <CheckInput
                                                        label={t('tournament.draw_in_standings')}
                                                        name="draw"
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.draw_points')}
                                                        name="drawPoints"
                                                        placeholder={t('tournament.draw_points_placeholder')}
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.lost_points')}
                                                        name="lostPoints"
                                                        placeholder={t('tournament.lost_points_placeholder')}
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.abandon_points')}
                                                        name="abandonPoints"
                                                        placeholder={t('tournament.abandon_points_placeholder')}
                                                    />
                                                    <CheckInput
                                                        label={t('tournament.bonus')}
                                                        name="bonus"
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.goals_win_abandon')}
                                                        name="goalsWinAbandon"
                                                        placeholder={t('tournament.goals_win_abandon_placeholder')}
                                                    />
                                                    <NumericInput
                                                        label={t('tournament.goals_lost_abandon')}
                                                        name="goalsLostAbandon"
                                                        placeholder={t('tournament.goals_lost_abandon_placeholder')}
                                                    />

                                                    <Container>
                                                        <Row className="justify-content-md-center">
                                                            <Button
                                                                size="lg"
                                                                variant="primary"
                                                                type="submit"
                                                            >
                                                                <Trans i18nKey="home.save">Save</Trans>
                                                            </Button>
                                                        </Row>
                                                    </Container>
                                                </FormFormik>
                                            </Formik>
                                        )
                                    )}
                                </FormContainer>

                                <Row className="align-items-center mt-2" >
                                    <Col md="auto">
                                        { (logo || newImage) && <Image 
                                            src={ newImage ? newImage : logo && ("/staticdjango" + logo) } 
                                            width="100" 
                                            rounded 
                                        />}
                                    </Col>
                                    <Col>
                                        <Form.File custom>
                                            <Form.File.Input
                                                ref={inputRef}
                                                onChange={handleDisplayFileDetails}
                                            />
                                            <Form.File.Label data-browse={t('main.select_file')}>
                                                { uploadedFileName || t('tournament.logo_placeholder') }
                                            </Form.File.Label>
                                        </Form.File>
                                    </Col>
                                </Row>
            
                            </Col>
                            <Col md={6}>
                                <Row>
                                    <Col md={2}><Form.Label><Trans i18nKey="team.title" /></Form.Label></Col>
                                    <Col>
                                        <SelectTeamForTournament
                                            tournament = { tournamentId }
                                            initialOption = {selectedTeam}
                                            onChange={ handleSelectedTeam }
                                            onBlur={ handleTeamBlur }
                                        />
                                    </Col>
                                </Row>
            
                                <Row className="my-1 justify-content-md-center">
                                    <Button onClick= { registerSquad }><Trans i18nKey="tournament.register">Register team</Trans></Button>
                                </Row>
                                <Container fluid="md">
                                    { !loading && !error && tournament.squads ? tournament.squads.map(squad => (
                                        <Row key={squad._id}>
                                            <Col>
                                                <Squad auto
                                                    squad={squad}
                                                    key={squad._id}
                                                    tournament={tournament._id}
                                                />
                                            </Col>
                                        </Row>
                                    )) : "No teams in this tournament"}
                                </Container>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="stages" title={t('stage.title_plural')}>
                        { !error && <StagesTabScreen tournament={tournament} stages={tournament.stages} squads={tournament.squads} />}
                    </Tab>
                    <Tab eventKey="matches" title={t('match.title_plural')}>
                        <MatchesTabScreen tournament={tournament}/>
                    </Tab>
                    <Tab eventKey="incidences" title={t('incidence.title_plural')}>
                        <IncidencesTabScreen tournament={tournament}/>
                    </Tab>
                </Tabs>
            </Container>
        </div>
    )
}

export default TournamentEditScreen