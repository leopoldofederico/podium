import React, {useEffect, useState} from 'react';
import { Trans } from 'react-i18next';
import { Row, Container, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { listTournaments } from '../actions/tournamentActions';
import Tournament from '../components/TournamentCard';
import Message from '../components/Message';
import Loader from '../components/Loader';
import { BsPlusCircleFill } from "react-icons/bs";
import { LinkContainer } from 'react-router-bootstrap';

function TournamentsScreen( {history} ) {
    const dispatch = useDispatch();
    const tournamentList = useSelector(state => state.tournamentList);

    const { error, loading, tournaments } = tournamentList;
    const [deleted, setDeleted] = useState(null);

    useEffect(() => {
        dispatch(listTournaments());
    }, [dispatch]);

    return (
        <div>
            <h1><Trans i18nKey="tournament.title">Tournaments</Trans></h1>
            { loading ? (
                <Loader />
            ) : (
                error ? 
                    (<Message variant="danger">{ error }</Message>) 
                : (
                    <div>
                        <LinkContainer to="/tournament/create">
                            <Container>
                                <Row>
                                    <Button type="primary">
                                        <Row className="mx-1 align-items-center">
                                            <BsPlusCircleFill className="mr-1"/>
                                            <Trans i18nKey="tournament.create">Create tournament</Trans>
                                        </Row>
                                    </Button>
                                </Row>
                            </Container>
                        </LinkContainer>
                        <Container fluid="md">
                            { tournaments ? tournaments.map(tournament => (
                                <Row key={tournament._id}>
                                    <Tournament 
                                        tournament={tournament} 
                                        key={tournament._id} 
                                    />
                                </Row>
                            )) : "No tournaments"}
                        </Container>
                    </div>
                )  
            )}
            
        </div>
    )
}

export default TournamentsScreen
