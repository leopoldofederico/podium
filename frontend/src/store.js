import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { tournamentListReducer,
    availableTeamsListReducer,
    tournamentDetailsReducer,
    tournamentUpdateReducer,
    tournamentCreateReducer,
    tournamentDeleteReducer,
    tournamentUndeleteReducer,
    tournamentLogoReducer,
} from './reducers/tournamentReducers';
import { categoryListReducer,
    categoryDetailsReducer,
    categoryUpdateReducer,
    categoryCreateReducer,
    categoryDeleteReducer,
    categoryUndeleteReducer,
} from './reducers/categoryReducers';
import { teamListReducer,
    teamListOwnReducer,
    teamDetailsReducer,
    teamUpdateReducer,
    teamCreateReducer,
    teamDeleteReducer,
    teamUndeleteReducer,
} from './reducers/teamReducers';
import { squadListReducer,
    squadDetailsReducer,
    squadUpdateReducer,
    squadCreateReducer,
    squadDeleteReducer,
    squadUndeleteReducer,
    squadFixtureReducer,
} from './reducers/squadReducers';
import { placeListReducer,
    placeDetailsReducer,
} from './reducers/placeReducers';
import { userLoginReducer, 
    userRegisterReducer, 
    userDetailsReducer, 
    userUpdateProfileReducer, 
    userListReducer, 
    userDeleteReducer,
    userUpdateReducer 
} from './reducers/userReducers';
import { stageListReducer,
    stageDetailsReducer,
    stageUpdateReducer,
    stageCreateReducer,
    stageDeleteReducer,
    stageUndeleteReducer,
} from './reducers/stageReducers';
import { groupListReducer,
    groupDetailsReducer,
    groupUpdateReducer,
    groupCreateReducer,
    groupDeleteReducer,
    groupUndeleteReducer,
} from './reducers/groupReducers';
import { matchListReducer,
    matchDetailsReducer,
    matchUpdateReducer,
    matchCreateReducer,
    matchDeleteReducer,
    matchUndeleteReducer,
} from './reducers/matchReducers';
import { incidenceListReducer,
    incidenceDetailsReducer,
    incidenceUpdateReducer,
    incidenceCreateReducer,
    incidenceDeleteReducer,
    incidenceUndeleteReducer,
} from './reducers/incidenceReducers';
import { standingsListReducer } from './reducers/standingReducers';


const reducer = combineReducers({
    tournamentList: tournamentListReducer,
    availableTeamsList: availableTeamsListReducer,
    tournamentDetails: tournamentDetailsReducer,
    tournamentUpdate: tournamentUpdateReducer,
    tournamentCreate: tournamentCreateReducer,
    tournamentDelete: tournamentDeleteReducer,
    tournamentUndelete: tournamentUndeleteReducer,
    tournamentLogo: tournamentLogoReducer,
    teamList: teamListReducer,
    teamListOwn: teamListOwnReducer,
    teamDetails: teamDetailsReducer,
    teamUpdate: teamUpdateReducer,
    teamCreate: teamCreateReducer,
    teamDelete: teamDeleteReducer,
    teamUndelete: teamUndeleteReducer,
    squadList: squadListReducer,
    squadFixture: squadFixtureReducer,
    squadDetails: squadDetailsReducer,
    squadUpdate: squadUpdateReducer,
    squadCreate: squadCreateReducer,
    squadDelete: squadDeleteReducer,
    squadUndelete: squadUndeleteReducer,    
    stageList: stageListReducer,
    stageDetails: stageDetailsReducer,
    stageUpdate: stageUpdateReducer,
    stageCreate: stageCreateReducer,
    stageDelete: stageDeleteReducer,
    stageUndelete: stageUndeleteReducer,    
    groupList: groupListReducer,
    groupDetails: groupDetailsReducer,
    groupUpdate: groupUpdateReducer,
    groupCreate: groupCreateReducer,
    groupDelete: groupDeleteReducer,
    groupUndelete: groupUndeleteReducer,    
    matchList: matchListReducer,
    matchDetails: matchDetailsReducer,
    matchUpdate: matchUpdateReducer,
    matchCreate: matchCreateReducer,
    matchDelete: matchDeleteReducer,
    matchUndelete: matchUndeleteReducer,    
    incidenceList: incidenceListReducer,
    incidenceDetails: incidenceDetailsReducer,
    incidenceUpdate: incidenceUpdateReducer,
    incidenceCreate: incidenceCreateReducer,
    incidenceDelete: incidenceDeleteReducer,
    incidenceUndelete: incidenceUndeleteReducer,
    placeList: placeListReducer,
    placeDetails: placeDetailsReducer,
    categoryList: categoryListReducer,
    categoryDetails: categoryDetailsReducer,
    categoryUpdate: categoryUpdateReducer,
    categoryCreate: categoryCreateReducer,
    categoryDelete: categoryDeleteReducer,
    categoryUndelete: categoryUndeleteReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer,
    userUpdate: userUpdateReducer,
    userList: userListReducer,
    userDelete: userDeleteReducer,
    standingsList: standingsListReducer,
});

const userInfoFromStorage = localStorage.getItem('userInfo') ?
    JSON.parse(localStorage.getItem('userInfo')) : null;

const initialState = {
    userLogin: {userInfo: userInfoFromStorage}
}


const middleware = [thunk]

const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middleware)))

export default store